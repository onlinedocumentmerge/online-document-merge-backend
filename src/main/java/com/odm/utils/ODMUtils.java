/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.odm.utils;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.odm.serviceImpl.DocumentServiceImpl;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

import org.json.JSONObject;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author 84918
 */
public class ODMUtils {

    //list columns in data source database/ keys in node
    private Set<String> columns = null;
    //map type data with keys
    private Map<String, String> map = null;
    private Map<String, String> mapDataTypeClient = null;
    //list String ('value1','value2',...)
    private List<String> values = null;
    //sql update row when update Data Source
    private String sqlUpdate = "";
    //sql insert new row when update Data Source
    private String sqlInsert = "";
    private String dataSourceStr = "";

    public ODMUtils() {
    }

    public String convertFileToString(File file) throws IOException {
        String content = "";
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(file);
            byte[] buffer = new byte[10];
            StringBuilder sb = new StringBuilder();
            while (fis.read(buffer) != -1) {
                sb.append(new String(buffer));
                buffer = new byte[10];
            }

            content = sb.toString();
        } finally {
            if (fis != null) {
                fis.close();
            }
        }

        return content;
    }

    public String getDataDir(Class c) {
        File dir = new File(System.getProperty("user.dir"));
        dir = new File(dir, "src");
        dir = new File(dir, "main");
        dir = new File(dir, "resources");

        for (String s : c.getName().split("\\.")) {
            dir = new File(dir, s);
            if (dir.isDirectory() == false) {
                dir.mkdir();
            }
        }
        System.out.println("Using data directory: " + dir.toString());
        return dir.toString() + File.separator;
    }

    //public void confirmTypeOfDataSource(MultipartFile dataSource) throws IOException {
    public void confirmTypeOfDataSource(String dataSource) throws IOException {
        try {
            values = new ArrayList<>();
            dataSourceStr = dataSource;
            //convert json string to json tree
            ObjectMapper mapper = new ObjectMapper();
            JsonNode jsonNode = mapper.readTree(dataSourceStr);
            String lastNode = dataSourceStr.substring(dataSourceStr.indexOf("{"), dataSourceStr.indexOf("}") + 1);
            //get key from 1 node
            JSONObject node1 = new JSONObject(lastNode);
            Set<String> keys = node1.keySet();
            columns = node1.keySet();

            JsonNode firstNode = jsonNode.get(0);
            values = new ArrayList<>();
            map = new HashMap<>();
            mapDataTypeClient = new HashMap<>();
            for (String key : keys) {
                map.put(key, firstNode.get(key).asText());
            }
            for (int i = 1; i < jsonNode.size(); i++) {
                String value = "(";
                for (String key : keys) {
                    String data = "";
                    if(jsonNode.get(i).get(key) != null) {
                        data = jsonNode.get(i).get(key).asText();
                    }
                    value += "'" + data + "',";
                    switch (getTypeOfVariable(data)) {
                        case CONST.TYPE_DATA_STRING:
                            map.put(key, "VARCHAR(255)");
                            mapDataTypeClient.put(key, "string");
                            break;
                        case CONST.TYPE_DATA_INTEGER:
                            if (map.get(key).equalsIgnoreCase("Decimal")) {

                            } else if (map.get(key).equalsIgnoreCase("Integer") || (map.get(key).equalsIgnoreCase("INT")) || map.get(key).equalsIgnoreCase("")) {
                                mapDataTypeClient.put(key, "integer");
                                map.put(key, "INT");
                            } else {
                                mapDataTypeClient.put(key, "string");
                                map.put(key, "VARCHAR(255)");
                            }
                            break;
                        case CONST.TYPE_DATA_FLOAT:
                            if (map.get(key).equalsIgnoreCase("Decimal") || map.get(key).equalsIgnoreCase("")
                                    || map.get(key).equalsIgnoreCase("Integer") || (map.get(key).equalsIgnoreCase("FLOAT"))) {
                                mapDataTypeClient.put(key, "decimal");
                                map.put(key, "FLOAT");
                            } else {
                                mapDataTypeClient.put(key, "string");
                                map.put(key, "VARCHAR(255)");
                            }
                            break;
                        case CONST.TYPE_DATA_DATATIME:
                            if (map.get(key).equalsIgnoreCase("DATETIME") || map.get(key).equalsIgnoreCase("")) {
                                map.put(key, "DATETIME");
                                mapDataTypeClient.put(key, "date");
                            } else {
                                mapDataTypeClient.put(key, "string");
                                map.put(key, "VARCHAR(255)");
                            }
                            break;
                        case CONST.TYPE_DATA_EMAIL:
                            if (map.get(key).equalsIgnoreCase("Email") || (map.get(key).equalsIgnoreCase("VARCHAR(255)")) || map.get(key).equalsIgnoreCase("")) {
                                map.put(key, "VARCHAR(255)");
                                mapDataTypeClient.put(key, "email");
                            } else {
                                mapDataTypeClient.put(key, "string");
                                map.put(key, "VARCHAR(255)");
                            }
                            break;
                    }
                }
                value = value.substring(0, value.length() - 1);
                value += ")";
                values.add(value);
            }
        } catch (IOException e) {
            throw new IOException();
        }
    }

    public int getTypeOfVariable(String variable) {
        int type = CONST.TYPE_DATA_STRING; // string
        try {
            Integer.parseInt(variable);
            type = CONST.TYPE_DATA_INTEGER; //integer
        } catch (Exception e) {
            try {
                Float.parseFloat(variable);
                type = CONST.TYPE_DATA_FLOAT; //float
            } catch (Exception a) {
            }
        }
        if (type == 1) {
            if (isThisDateValid(variable)) {
                type = CONST.TYPE_DATA_DATATIME;
            } else if(isEmail(variable)) {
                type = CONST.TYPE_DATA_EMAIL;
            }
        }
        return type;
    }

    public boolean isThisDateValid(String dateStr) {
        boolean checkDate = true;

        if (dateStr == null) {
            checkDate = false;
        }
        String[] split = dateStr.split("/");
        if (split.length == 3) {
            if (getTypeOfVariable(split[2]) == CONST.TYPE_DATA_INTEGER) {
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                sdf.setLenient(false);
                try {
                    Date date = sdf.parse(dateStr);
                } catch (ParseException e) {
                    checkDate = false;
                }
            } else {
                checkDate = false;
            }
        } else {
            checkDate = false;
        }
        return checkDate;
    }

    public boolean isEmail(String email) {
        String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\."+
                "[a-zA-Z0-9_+&*-]+)*@" +
                "(?:[a-zA-Z0-9-]+\\.)+[a-z" +
                "A-Z]{2,7}$";

        Pattern pat = Pattern.compile(emailRegex);
        if (email == null)
            return false;
        return pat.matcher(email).matches();
    }

    public File convertMultipartFileToFile(MultipartFile multipartFile) throws IOException {
        File file = null;
        try {
            String fileDir = getDataDir(DocumentServiceImpl.class);
            file = new File(fileDir);
            multipartFile.transferTo(file);
        } catch (IOException e) {
            System.out.println("CAN'T CONVERT FILE");
            throw new IOException();
        }
        return file;
    }

    public void confirmTypeOfStatus(String tableName) throws IOException {
        try {
            //convert json string to json tree
            ObjectMapper mapper = new ObjectMapper();
            JsonNode jsonNode = mapper.readTree(dataSourceStr);

            String setString = "(";
            for (String column : columns) {
                if (!column.equals("ODM")) {
                    setString += "\"" + column + "\",";
                }
            }
            setString = setString.substring(0, setString.length() - 1);
            setString += ")";

            sqlInsert = "SET IDENTITY_INSERT " + tableName + " OFF \n";
            for (int i = 1; i < jsonNode.size(); i++) {
                JsonNode odmId = jsonNode.get(i).get("ODM");
                if (odmId != null) {
                    //node is updated
                    String sqlUpdate1 = "UPDATE " + tableName + " SET ";
                    for (String column : columns) {
                        if (!column.equals("ODM")) {
                            sqlUpdate1 += "[" + column + "] = '" + jsonNode.get(i).get(column).asText() + "',";
                        }
                    }
                    sqlUpdate1 = sqlUpdate1.substring(0, sqlUpdate1.lastIndexOf(","));
                    sqlUpdate += sqlUpdate1 + " WHERE ODM = " + odmId.asInt() + " ;";
                } else { //node is insert
                    String value = "(";
                    for (String column : columns) {
                        if (!column.equals("ODM")) {
                            value += "'" + jsonNode.get(i).get(column).asText() + "',";
                        }
                    }
                    value = value.substring(0, value.length() - 1);
                    value += ")";
                    sqlInsert += "INSERT INTO " + tableName + setString + "VALUES " + value + ";";
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("FAIL_confirmTypeOfStatus_ODMUtils");
            throw new IOException();
        }
    }

    public Set<String> getColumns() {
        return columns;
    }

    public void setColumns(Set<String> columns) {
        this.columns = columns;
    }

    public Map<String, String> getMap() {
        return map;
    }

    public void setMap(Map<String, String> map) {
        this.map = map;
    }

    public List<String> getValues() {
        return values;
    }

    public void setValues(List<String> values) {
        this.values = values;
    }

    public String getSqlUpdate() {
        return sqlUpdate;
    }

    public String getSqlInsert() {
        return sqlInsert;
    }

    public Map<String, String> getMapDataTypeClient() {
        return mapDataTypeClient;
    }

    public void setMapDataTypeClient(Map<String, String> mapDataTypeClient) {
        this.mapDataTypeClient = mapDataTypeClient;
    }
}
