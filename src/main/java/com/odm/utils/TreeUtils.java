/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.odm.utils;

import com.odm.model.Node;
import odm.arithmetic.iarithmetic.IArithmeticService;

import java.math.BigDecimal;
import java.util.*;

/**
 *
 * @author 84918
 */
public class TreeUtils {
    private static int length = 0;
//    columns test
//    public static String[] columns = {"a", "b", "c", "d"};
//    public static IArithmeticService arithmetics[] = {s, s1};
//    input test
//    String input = "1 + SUM(SUM(a,b) + SUM(a,b),b,SUM(a,b)) + ABC(a,b)";

    public String[] columns;
    public IArithmeticService arithmetics[];
    public Node tree = null; //tree
    public List<Node> statements = new ArrayList<>(); //list statement cần lấy
    public IArithmeticService currentArithmetic; //arithmetic lúc kiểm tra
    public String input;
    public List<String> operator; //toán tử + - * /
    public List<String> operands; //toán hạng
    public Map<String, String> map = new HashMap<>(); //check statement tồn tại hay chưa

    public TreeUtils(String input, IArithmeticService[] arithmetics, String[] columns) {
        this.input = input; //biểu thức cần tạo tree
        this.arithmetics = arithmetics; //list arithmetic có sẵn (class vs jar file)
        this.columns = columns; //cột (header) của dataSource
    }

    public void initTree() {
        input = input.replaceAll(" ", "");
        tree = createTree(tree, input, 0); //Tạo cây từ đầu vào
        if (tree != null) {
            confirmStatementInTree(tree); //xác định statement của mỗi node
            //readTree(tree);
        }
    }

    public void readTree(Node node) { //hiện nhìn chơi
        List<Node> nodes = node.getChilds();
        try {
            System.out.println(node.getExpression() + " SQL:" + node.getSqlStatement() + " :" + node.getArithmetic().isSQL() + " - Position: " + node.getOpenPosition());
        } catch (Exception e) {
            System.out.println(node.getExpression() + ": child:  " + nodes);
        }
        if (nodes != null) {
            for (Node node1 : nodes) {
                readTree(node1);
            }
        }
    }

    public void getStatement(Node node) { //lấy list statement sau khi tạo cây, xác định statement của node
        List<Node> nodes = node.getChilds();
        if (node.getArithmetic().isSQL()) { //nếu node đã có thể lấy statement
            String s = node.getSqlStatement();
            if ((map.get(s) == null || !map.get(s).equals("ODM")) //thì k cần check node con
                    && !isANumber(node.getSqlStatement())) {
                map.put(node.getSqlStatement(), "ODM"); //đánh dấu
                statements.add(node);//add vào list
            }
        } else { //nếu node k thể tạo statement --> lấy ở node con nếu có
            if (nodes != null) {
                for (Node node1 : nodes) {
                    getStatement(node1);
                }
            }
        }
    }

    public static boolean confirmStatementInTree(Node node) {
        boolean checkStatement = true;
        List<Node> nodes = null;
        try {
            nodes = node.getChilds();
        } catch (Exception e) {
            nodes = null;
        }

        if (nodes != null) {
            for (Node node1 : nodes) {
                if (!confirmStatementInTree(node1)) {
                    checkStatement = false;
                    break;
                }
            }
            IArithmeticService dto = node.getArithmetic();
            if (!checkStatement) {
                IArithmeticService dto1 = new IArithmeticService() {
                    @Override
                    public Object calculate(List<Object> inputs) {
                        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                    }

                    @Override
                    public int getMinParameter() {
                        return dto.getMinParameter();
                    }

                    @Override
                    public int getMaxParameter() {
                        return dto.getMaxParameter();
                    }

                    @Override
                    public String getOperator() {
                        return dto.getOperator();
                    }

                    @Override
                    public boolean isSQL() {
                        return false;
                    }

                    @Override
                    public String sqlStatement(List<String> inputs) {
                        return "";
                    }

                    @Override
                    public String description() {
                        return dto.description();
                    }
                };
                node.setArithmetic(dto1);
            } else if (dto.isSQL()) {
                List<String> statementsChilds = new ArrayList<>();
                for (Node node1 : nodes) {
                    statementsChilds.add(node1.getSqlStatement());
                }
                String statementSQL = node.getArithmetic().sqlStatement(statementsChilds);
                if (node.getArithmetic().sqlStatement(statementsChilds).length() == 0) {
                    String expression = node.getExpression();
                    List<Node> childs = node.getChilds();
                    for (int i = 0; i < childs.size(); i++) {
                        String expressionChild = childs.get(i).getExpression();
                        String statementChild = childs.get(i).getSqlStatement();
                        expression = expression.replace(expressionChild, statementChild);
                    }
                    statementSQL = expression;
                    IArithmeticService dto1 = new IArithmeticService() {
                        @Override
                        public Object calculate(List<Object> inputs) {
                            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                        }

                        @Override
                        public int getMinParameter() {
                            return dto.getMinParameter();
                        }

                        @Override
                        public int getMaxParameter() {
                            return dto.getMaxParameter();
                        }

                        @Override
                        public String getOperator() {
                            return dto.getOperator();
                        }

                        @Override
                        public boolean isSQL() {
                            return true;
                        }

                        @Override
                        public String sqlStatement(List<String> inputs) {
                            return dto.sqlStatement(inputs);
                        }

                        @Override
                        public String description() {
                            return dto.description();
                        }
                    };
                    node.setArithmetic(dto1);
                }
                node.setSqlStatement(statementSQL);
            }
        }
        return node.getArithmetic().isSQL();
    }

    //xác định kiểu dữ liệu của thành phần (toán hạng)
    public int isValidationElement(String input) {
        //check is column or not --> return 1 if it's column
        for (String column : columns) { //nếu có trong list columns(header)
            if (column.equals(input)) {
                return 1;//it's is a column
            }
        }
        if (isANumber(input)) {
            return 3; //it's is a number
        }
        for (IArithmeticService arithmetic : arithmetics) {
            currentArithmetic = null;
            String operator = arithmetic.getOperator();
            //kiểm tra coi có trùng operator k
            if (input.indexOf(operator + "(") == 0) {
                int minParam = arithmetic.getMinParameter();
                int maxParam = arithmetic.getMaxParameter();
                //tách
                String cal = input.substring(input.indexOf("(") + 1, input.lastIndexOf(")"));
                List<String> elements = splitString(cal);
                // đếm số phần tử, kiểm tra trong khoảng cho phép k
                if (elements.size() >= minParam && elements.size() <= maxParam) {
                    currentArithmetic = new IArithmeticService() {
                        @Override
                        public Object calculate(List<Object> inputs) {
                            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                        }

                        @Override
                        public int getMinParameter() {
                            return arithmetic.getMinParameter();
                        }

                        @Override
                        public int getMaxParameter() {
                            return arithmetic.getMaxParameter();
                        }

                        @Override
                        public String getOperator() {
                            return arithmetic.getOperator();
                        }

                        @Override
                        public boolean isSQL() {
                            return arithmetic.isSQL();
                        }

                        @Override
                        public String sqlStatement(List<String> inputs) {
                            return arithmetic.sqlStatement(inputs);
                        }

                        @Override
                        public String description() {
                            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                        }
                    };
                    return 2;
                }
            }
        }
        //check arithmetic + validation arithmetic --> return 2 if it's true
        return 4; //return 4 another
    }

    //tạo cây
    public Node createTree(Node node, String s, int position) {
        if (!confirmOperands(s).equals("FAIL VALUE")) {
            if (node == null) { //root = null
                node = new Node();
            }
            node.setExpression(s);
            node.setOperands(operands);
            node.setOperator(operator);


            List<Node> childs = new ArrayList<>();
            if (operands.size() == 1) { //trường hợp nó đứng 1 mình
                //System.out.println("operand : " + operands.get(0));
                int checkValue = isValidationElement(operands.get(0));
                if (checkValue == 1 || checkValue == 3) { //column or number
                    Node dto = new Node();
                    dto.setExpression(operands.get(0));
                    String sqlStatement = operands.get(0);
                    IArithmeticService arithmetic = new IArithmeticService() {
                        @Override
                        public Object calculate(List<Object> inputs) {
                            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                        }

                        @Override
                        public int getMinParameter() {
                            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                        }

                        @Override
                        public int getMaxParameter() {
                            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                        }

                        @Override
                        public String getOperator() {
                            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                        }

                        @Override
                        public boolean isSQL() {
                            return true;
                        }

                        @Override
                        public String sqlStatement(List<String> inputs) {
                            return sqlStatement;
                        }

                        @Override
                        public String description() {
                            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                        }
                    };
                    dto.setArithmetic(arithmetic);
                    dto.setSqlStatement(sqlStatement);
                    node = dto;
                } else if (checkValue == 2) { //arithmetic
                    Node dto = new Node();
                    dto.setExpression(operands.get(0));
                    IArithmeticService ari = currentArithmetic;
                    IArithmeticService arithmetic = new IArithmeticService() {
                        @Override
                        public Object calculate(List<Object> inputs) {
                            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                        }

                        @Override
                        public int getMinParameter() {
                            return ari.getMinParameter();
                        }

                        @Override
                        public int getMaxParameter() {
                            return ari.getMaxParameter();
                        }

                        @Override
                        public String getOperator() {
                            return ari.getOperator();
                        }

                        @Override
                        public boolean isSQL() {
                            return ari.isSQL();
                        }

                        @Override
                        public String sqlStatement(List<String> inputs) {
                            return ari.sqlStatement(inputs);
                        }

                        @Override
                        public String description() {
                            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                        }
                    };
                    dto.setArithmetic(arithmetic);
                    String ss = operands.get(0).substring(operands.get(0).indexOf("(") + 1, operands.get(0).lastIndexOf(")"));
                    List<String> elements = splitString(ss);
                    List<Node> childsDto = new ArrayList<>();
                    List<String> inputs = new ArrayList<>();
                    for (String element : elements) {
                        Node childOfChild = new Node();
                        int childPosition = position + s.indexOf(element);
                        childOfChild = createTree(childOfChild, element, childPosition);
                        childsDto.add(childOfChild);
                        inputs.add(childOfChild.getSqlStatement());
                    }
                    dto.setChilds(childsDto);
                    dto.setSqlStatement(dto.getArithmetic().sqlStatement(inputs));
                    node = dto;
                } else {
                    System.out.println("FAIL      " + s);
                    tree = null;
                    return null;
                }
            } else { //trường hợp có nhiều toán hạng
                for (String operand : operands) {
                    //System.out.println("operand : " + operand);
                    int checkValue = isValidationElement(operand);
                    if (checkValue == 1 || checkValue == 3) { //column or number
                        Node dto = new Node();
                        dto.setExpression(operand);
                        String sqlStatement = operand;
                        IArithmeticService arithmetic = new IArithmeticService() {
                            @Override
                            public Object calculate(List<Object> inputs) {
                                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                            }

                            @Override
                            public int getMinParameter() {
                                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                            }

                            @Override
                            public int getMaxParameter() {
                                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                            }

                            @Override
                            public String getOperator() {
                                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                            }

                            @Override
                            public boolean isSQL() {
                                return true;
                            }

                            @Override
                            public String sqlStatement(List<String> inputs) {
                                return sqlStatement;
                            }

                            @Override
                            public String description() {
                                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                            }
                        };
                        dto.setArithmetic(arithmetic);
                        dto.setSqlStatement(sqlStatement);
                        childs.add(dto);
                    } else if (checkValue == 2) { //arithmetic
                        Node dto = new Node();
                        dto.setExpression(operand);
                        IArithmeticService ari = currentArithmetic;
                        IArithmeticService arithmetic = new IArithmeticService() {
                            @Override
                            public Object calculate(List<Object> inputs) {
                                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                            }

                            @Override
                            public int getMinParameter() {
                                return ari.getMinParameter();
                            }

                            @Override
                            public int getMaxParameter() {
                                return ari.getMaxParameter();
                            }

                            @Override
                            public String getOperator() {
                                return ari.getOperator();
                            }

                            @Override
                            public boolean isSQL() {
                                return ari.isSQL();
                            }

                            @Override
                            public String sqlStatement(List<String> inputs) {
                                return ari.sqlStatement(inputs);
                            }

                            @Override
                            public String description() {
                                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                            }
                        };
                        dto.setArithmetic(arithmetic);
                        String ss = operand.substring(operand.indexOf("(") + 1, operand.lastIndexOf(")"));
                        List<String> elements = splitString(ss);
                        List<Node> childsDto = new ArrayList<>();
                        List<String> inputs = new ArrayList<>();
                        for (String element : elements) {
                            System.out.println("element: " + element);
                            Node childOfChild = new Node();
                            int childPosition = position + s.indexOf(element);
                            childOfChild = createTree(childOfChild, element, childPosition);
                            childsDto.add(childOfChild);
                            inputs.add(childOfChild.getSqlStatement());
                        }
                        dto.setSqlStatement(dto.getArithmetic().sqlStatement(inputs));
                        dto.setChilds(childsDto);
                        childs.add(dto);
                    } else {
                        Node dto = new Node();
                        dto = createTree(node, operand, position);
                        if (dto != null) {
                            childs.add(dto);
                        } else {
                            tree = null;
                            return null;
                        }
                    }
                }
                node.setChilds(childs);
            }
            IArithmeticService arithmeticDefault = new IArithmeticService() {
                @Override
                public Object calculate(List<Object> inputs) {
                    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                }

                @Override
                public int getMinParameter() {
                    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                }

                @Override
                public int getMaxParameter() {
                    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                }

                @Override
                public String getOperator() {
                    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                }

                @Override
                public boolean isSQL() {
                    return true;
                }

                @Override
                public String sqlStatement(List<String> inputs) {
                    return "";
                }

                @Override
                public String description() {
                    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                }
            };
            if (node.getArithmetic() == null) {
                node.setArithmetic(arithmeticDefault);
            }
            node.setOpenPosition(position);
            System.out.println("Input: " + s + " - " + s.length() + " - " + position);
            node.setClosePosition(position + s.length());
        } else {
            System.out.println("FAIL HERE: " + s);
            return null;
        }
        return node;
    }

    //tạo postfix( hậu tố) từ input
    //xác định những thành phần, biểu thức validation
    public String confirmOperands(String s) {
        operator = new ArrayList<>();
        operands = new ArrayList<>();
        s = s.replaceAll(" ", "");
        int countColAndAri = 0;
        char[] postFix = createPostFixColumn(" " + s);
        if (postFix == null) {
            return "FAIL VALUE";
        }
//        System.out.println("====Begin PostFix=========");
//        for (char c : postFix) {
//            System.out.print(c);
//        }
//        System.out.println();
//        System.out.println("====End PostFix=========");
        boolean checkCalculate = true;
        Stack<BigDecimal> stack = new Stack<>();
        int j = 0;
        try {
            while (j < length) {
                while (postFix[j] == ' ' || postFix[j] == '\t') {
                    j++;
                }
                if ((postFix[j] - 0) == 0) {
                    break;
                }
                //trường hợp là số : số thực / số nguyên
                if (isANumber(postFix[j]) || (postFix[j] == '-' && isANumber(postFix[j + 1]))) {
                    int num1 = 0; //num before  floating point numbers
                    int num2 = 0; //num behind floating point numbers
                    boolean check = false; //check floating point numbers
                    boolean checkNegative = false;
                    if (postFix[j] == '-' && isANumber(postFix[j + 1])) {
                        checkNegative = true;
                        j++;
                    }
                    while (isANumber(postFix[j]) || postFix[j] == '.') {
                        //false -> before || true -> behind
                        //index of char '0' = 48 (in ASCII)
                        if (postFix[j] != '.') {
                            if (check) {
                                num2 = num2 * 10 + (postFix[j] - 48);
                            } else {
                                num1 = num1 * 10 + (postFix[j] - 48);
                            }
                        } else {
                            check = true;
                        }
                        j++;
                    }
                    BigDecimal num = null;
                    if (checkNegative) {
                        num = new BigDecimal("-" + num1 + "." + num2);
                    } else {
                        num = new BigDecimal(num1 + "." + num2);
                    }
                    stack.push(num);
//                    System.out.println("number in postfix: " + num);
                    operands.add(num + ""); //lưu toán hạng
                }//trường hợp là arithmetic/ expression / column (header) 
                else if (!isANumber(postFix[j]) && postFix[j] != ' ' && postFix[j] != '+' && postFix[j] != '-'
                        && postFix[j] != '*' && postFix[j] != '/') {
                    String col = "";
                    int inExpression = 0;
                    while (((!isANumber(postFix[j]) && postFix[j] != ' '
                            && postFix[j] != '+' && postFix[j] != '-'
                            && postFix[j] != '*' && postFix[j] != '/')
                            || (isANumber(postFix[j]))
                            || (postFix[j] == '(') || (postFix[j] == ')')
                            || (inExpression != 0 && (postFix[j] == '+' || postFix[j] != '-'
                            || postFix[j] == '/' || postFix[j] == '*')))
                            && j < length) {
                        if (postFix[j] == '(') {
                            inExpression++;
                        }
                        if (postFix[j] == ')') {
                            inExpression--;
                        }
                        col += postFix[j];
                        j++;
                    }
                    operands.add(col); //lưu toán hạng
                    countColAndAri++;

                    stack.push(new BigDecimal("1"));
                } else {
                    BigDecimal num1 = stack.pop();
                    BigDecimal num2 = stack.pop();
                    BigDecimal result = new BigDecimal("0");
                    operator.add(postFix[j] + ""); //lưu toán tử
                    switch (postFix[j]) {
                        case '+':
                            result = num2.add(num1);
                            break;
                        case '-':
                            result = num2.subtract(num1);
                            break;
                        case '*':
                            result = num2.multiply(num1);
                            break;
                        case '/':
                            result = num2.divide(num1);
                            break;
                        default:
                            System.out.println("FAIL operation");
                            return "FAIL VALUE";
                    }
                    stack.push(result);
                    j++;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            checkCalculate = false;
        }
        if (checkCalculate) {
            if (stack.size() != 1 && countColAndAri != 1) {
                return "FAIL VALUE";
            } else {
                return stack.pop().toString();
            }
        } else {
            return "FAIL VALUE";
        }
    }

    //tạo postfix (hậu tố)
    public char[] createPostFixColumn(String s) {
        s += " ";
        Stack<Character> stack = new Stack<>();
        char[] postFix = new char[1000];
        int j = 0;
        char x;
        int inExpression = 0;
        try {
            for (int i = 0; i < s.length(); i++) {

                char value = s.charAt(i);
                //trường hợp là ký tự --> có thể là arithmetic hoặc cột(header)
                if ((!isANumber(value) && value != '(' && value != ')' && value != ' '
                        && value != '+' && value != '-' && value != '/'
                        && value != '*') || (isANumber(value) && !isANumber(s.charAt(i - 1)))
                        || (value == '(' && !isANumber(s.charAt(i - 1)))
                        || (value == ')' && inExpression != 0)
                        || (inExpression != 0 && (value == '+' || value != '-' || value == '/'
                        || value == '*'))) {
                    if (value == '(') {
                        inExpression++;
                    }
                    if (value == ')') {
                        inExpression--;
                    }
                    postFix[j++] = value;
                }//trường hợp là 1 số: số thực , số nguyên
                else if (isANumber(value) || (value == '.' && isANumber(s.charAt(i + 1)) && isANumber(s.charAt(i - 1)))
                        || (value == '-' && isANumber(s.charAt(i + 1)) && !isANumber(s.charAt(i - 1)))) {
                    //float/integer number solution or negative solution '-'
                    postFix[j++] = value;
                } else if (value == '(') {
                    stack.push(value);
                } else if (value == ')') {
                    while ((x = stack.pop()) != '(') {
                        postFix[j++] = x;
                    }
                } else {
                    while (!stack.isEmpty() && (Precedence(value) <= Precedence(stack.peek()))) {
                        x = stack.pop();
                        postFix[j++] = x;
                    }
                    postFix[j++] = ' ';
                    stack.push(value);
                }
            }
            while (!stack.isEmpty()) {
                postFix[j++] = stack.pop();
            }
            length = j;
        } catch (Exception e) {
            return null;
        }
        return postFix;
    }

    public boolean isANumber(String s) {
        try {
            float f = Float.parseFloat(s);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    public boolean isANumber(char c) {
        return ((c - 48) >= 0 && (c - 48) <= 9);
    }

    public int Precedence(char x) {
        if (x == '(') {
            return 0;
        }
        if (x == '+' || x == '-') {
            return 1;
        }
        if (x == '*' || x == '/') {
            return 2;
        }
        return 3;
    }

    public List<String> splitString(String s) {
        int begin = 0;
        int count = 0;
        List<String> list = new ArrayList<>();
        for (int i = 0; i < s.length(); i++) {

            if (s.charAt(i) == '(') {
                count++;
            } else if (s.charAt(i) == ')') {
                count--;
            } else if (s.charAt(i) == ',' && count == 0) {
                String s1 = s.substring(begin, i);
                begin = i + 1;
                list.add(s1);
            }
        }
        list.add(s.substring(begin, s.length()));
        return list;
    }
}
