/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.odm.utils;

import com.odm.entity.SpecificLinkParameter;
import com.odm.repository.DataSourceDetailRepository;
import com.odm.repository.DataSourceRepository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author 84918
 */
public class DatabaseUtils {

    private String dbURL;
    private Connection conn = null;
    private PreparedStatement stm = null;
    private ResultSet rs = null;
    private int typeConnect;
    private List<String> values;

    @Autowired
    DataSourceDetailRepository dataSourceDetailRepository;

    @Autowired
    DataSourceRepository dataSourceRepository;

    public DatabaseUtils(int typeConnect, String dbURL) {
        this.dbURL = dbURL;
        this.typeConnect = typeConnect;
    }

    public Connection getConnection() throws SQLException, ClassNotFoundException {
        switch (typeConnect) {
            case CONST.TYPE_CONNECT_MYSQL:
                connectSQLServer("com.mysql.jdbc.Driver");
                break;
            case CONST.TYPE_CONNECT_ORACLE:
                connectSQLServer("oracle.jdbc.driver.OracleDriver");
                break;
            case CONST.TYPE_CONNECT_SQLSERVER:
                connectSQLServer("com.microsoft.sqlserver.jdbc.SQLServerDriver");
                break;
        }
        return conn;
    }

    public void connectSQLServer(String driverPath) throws SQLException, ClassNotFoundException {
        //urlString: jdbc:sqlserver://localhost:1433;databaseName=DatabaseNae;integratedSecurity=true
        Class.forName(driverPath);
        conn = DriverManager.getConnection(dbURL);
    }

    public void closeConnection() throws SQLException {
        if (rs != null) {
            rs.close();
        }
        if (stm != null) {
            stm.close();
        }
        if (conn != null) {
            conn.close();
        }
    }
    public String getDatabaseNameFromConnectionString () {
        String[] split = dbURL.split("/");
        String input = split[3];
        for(int i = 0; i < input.length(); i++) {
            if(input.charAt(i) == '?' || input.charAt(i) == ';') {
                return input.substring(0,i);
            }
        }
        return input;
    }

    public List<String> getTablesInDatabase() throws SQLException, ClassNotFoundException {
        List<String> listTable = new ArrayList<String>();
        try {
            conn = getConnection();
            String sql = "";
            if (typeConnect == CONST.TYPE_CONNECT_MYSQL) {
                String[] split = dbURL.split("/");
                //get databaseName from connection String
                String databaseName = getDatabaseNameFromConnectionString();
                System.out.println(databaseName);
                sql = "SELECT table_name FROM information_schema.TABLES WHERE table_schema = '" + databaseName + "' ;";

            } else if (typeConnect == CONST.TYPE_CONNECT_SQLSERVER) {
                sql = "SELECT name FROM sysobjects WHERE xtype='U'";
            }

            stm = conn.prepareStatement(sql);
            rs = stm.executeQuery();
            listTable = new ArrayList<>();
            while (rs.next()) {
                String name = "";
                if (typeConnect == CONST.TYPE_CONNECT_MYSQL) {
                    name = rs.getString("table_name");
                } else if (typeConnect == CONST.TYPE_CONNECT_SQLSERVER) {
                    name = rs.getString("name");
                }
                listTable.add(name);
            }
        } finally {
            closeConnection();
        }
        return listTable;
    }

    public List<String> getColumnFromTable(String tableName) throws SQLException {
        List<String> columns  = new ArrayList<>();
        try {
            conn = getConnection();
            String sql = "SELECT COLUMN_NAME \n"
                    + "FROM INFORMATION_SCHEMA.COLUMNS\n"
                    + "WHERE \n"
                    + "TABLE_NAME = ?";
            stm = conn.prepareStatement(sql);
            stm.setString(1, tableName);
            rs = stm.executeQuery();
            while (rs.next()) {
                String col = rs.getString("COLUMN_NAME");
                columns.add(col);
//                String dataType = rs.getString("DATA_TYPE");
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeConnection();
        }
        return columns;
    }

    public List<String> getDataFromTable(String statement) throws SQLException, ClassNotFoundException {

        List<String> listNode = new ArrayList<String>();
        try {
            conn = getConnection();
            //get data from table
            stm = conn.prepareStatement(statement);
            rs = stm.executeQuery();
            listNode = convertResultSetToJson(rs);
        } finally {
            closeConnection();
        }
        return listNode;
    }

    public void createTableDataSource(int userId, int metadataId, Map<String, String> map,
                                      Set<String> keys, List<String> values) throws SQLException, ClassNotFoundException {
        try {
            conn = getConnection();
            String setString = "(";
            //create table
            String sql = "CREATE TABLE TABLE_" + userId + "_" + metadataId + " (\n"
                    + " ODM INT NOT NULL IDENTITY(1,1) ,\n";
            for (String key : keys) {
                setString += "\"" + key + "\",";
                sql += "\"" + key + "\" " + map.get(key) + " NULL, \n";
            }
            sql += "  PRIMARY KEY (ODM)"
                    + ");";
            System.out.println(sql);
            stm = conn.prepareStatement(sql);
            stm.execute();

            //import data source
            setString = setString.substring(0, setString.length() - 1);
            setString += ")";

            String sqlQuery = "";
            for (String value : values) {
                String sqlInsert = "INSERT INTO TABLE_" + userId + "_" + metadataId + " " + setString
                        + "VALUES " + value + ";";
                sqlQuery += sqlInsert;
            }

            stm = conn.prepareStatement(sqlQuery);
            stm.execute();
        } finally {
            closeConnection();
        }
    }

    public String convertDataType(String dataTypeClient) {
        switch (dataTypeClient) {
            case "string": {
                return "varchar(255)";
            }
            case "integer": {
                return "int";
            }
            case "decimal": {
                return "float";
            }
            case "date": {
                return "datetime";
            }
            default:
                return "varchar(255)";
        }
    }

    public void changeDataTypeColumn(int typeConnect, String tableName, String typeData, String columnName) throws SQLException, ClassNotFoundException {
        try {
            String sql = "";
            switch (typeConnect) {
                case CONST.TYPE_CONNECT_MYSQL:
                    sql = "ALTER TABLE " + tableName + " MODIFY " + columnName + " " + convertDataType(typeData) + " ;";
                    break;
//                case CONST.TYPE_CONNECT_ORACLE:
//                    sql = "ALTER TABLE " + tableName + " MODIFY " + columnName + " " + typeData + " ;";
//                    break;
                case CONST.TYPE_CONNECT_SQLSERVER:
                    sql = "ALTER TABLE " + tableName + " ALTER COLUMN " + "[" + columnName + "]" + " " + convertDataType(typeData) + " ;";
                    break;
            }
            System.out.println(sql);
            conn = getConnection();
            stm = conn.prepareStatement(sql);
            stm.execute();
        } finally {
            closeConnection();
        }
    }

    public void deleteColumn(String tableName, String columnName) throws ClassNotFoundException, SQLException {
        String sql = "ALTER TABLE " + tableName + " \n"
                + "DROP COLUMN [" + columnName + "];";
        try {
            System.out.println(sql);
            conn = getConnection();
            stm = conn.prepareStatement(sql);
            stm.execute();
        } finally {
            closeConnection();
        }
    }

    public void changeColumnName(String tableName, String columnName, String newColumnName) throws ClassNotFoundException, SQLException {
        String sql = "USE Datasource\n" +
                "EXEC sp_rename '" + tableName + "." + columnName + "' ,'" + newColumnName + "', 'COLUMN';";
        try {
            System.out.println(sql);
            conn = getConnection();
            stm = conn.prepareStatement(sql);
            stm.execute();
        } finally {
            closeConnection();
        }
    }

    public void addColumn(String tableName, String typeData, String columnName) throws ClassNotFoundException, SQLException {
        String sql = "ALTER TABLE " + tableName + "\n"
                + "ADD [" + columnName + "] " + typeData + ";";
        try {
            System.out.println(sql);
            conn = getConnection();
            stm = conn.prepareStatement(sql);
            stm.execute();
        } finally {
            closeConnection();
        }
    }

    public void deleteRow(String[] rowIds, String tableName) throws ClassNotFoundException, SQLException {

        String sqlDelete = "";
        for (String rowId : rowIds) {
            sqlDelete += "DELETE FROM " + tableName + " WHERE ODM = " + rowId + ";";
        }
        try {
            conn = getConnection();
            stm = conn.prepareStatement(sqlDelete);
            stm.execute();
        } finally {
            closeConnection();
        }

    }

    public List<String> convertResultSetToJson(ResultSet rs) throws SQLException, JSONException {

        ResultSetMetaData rsmd = rs.getMetaData();
        List<String> listNode = new ArrayList<String>();

        while (rs.next()) {
            //get count keys
            int numColumns = rsmd.getColumnCount();
            String node = "{";
            for (int i = 1; i < numColumns + 1; i++) {
                String column_name = rsmd.getColumnName(i);
                String data = rs.getString(column_name);
                node += "\"" + column_name + "\":\"" + data + "\",";
            }
            if (node.length() > 1) {
                node = node.substring(0, node.lastIndexOf(","));
            }
            node += "}";
            listNode.add(node);
        }
        return listNode;
    }

    public boolean queryRowDataSource(String sql) throws SQLException, ClassNotFoundException {
        boolean check = true;
        try {
            conn = getConnection();
            stm = conn.prepareStatement(sql);
            check = stm.execute();

        } finally {
            closeConnection();
        }
        return check;
    }

    public void changeTypeColumn(String tableName, String dataType, String columnName) {

    }

    public List<String> getDataColumn(String tableName, String columnName) throws SQLException, ClassNotFoundException {

        List<String> datas = new ArrayList<String>();
        try {
            String sql = "SELECT " + columnName + " FROM " + tableName;
            conn = getConnection();
            stm = conn.prepareStatement(sql);
            rs = stm.executeQuery();
            while (rs.next()) {
                String data = rs.getString(columnName);
                datas.add(data);
            }
        } finally {
            closeConnection();
        }
        return datas;
    }

    public void comfirmTypeOfColumn(List<String> data) {

    }

    public List<String> checkStatement(String statement) throws SQLException, ClassNotFoundException {
        List<String> json = new ArrayList<String>();
        try {
            conn = getConnection();
            stm = conn.prepareStatement(statement);
            rs = stm.executeQuery();
            json = convertResultSetToJson(rs);
        } finally {
            closeConnection();
        }
        return json;
    }

    public void dropTable(String tableName) throws Exception {
        try {
            conn = getConnection();
            String sql = "DROP TABLE " + tableName;
            stm = conn.prepareStatement(sql);
            stm.execute();

        } finally {
            closeConnection();
        }
    }

    public List<String> getRowByLink(String statement) throws SQLException, ClassNotFoundException {
        List<String> json = new ArrayList<String>();
        try {
            conn = getConnection();
            stm = conn.prepareStatement(statement);
            rs = stm.executeQuery();
            json = convertResultSetToJson(rs);
        } finally {
            closeConnection();
        }
        return json;
    }

    public List<String> getRowByLinkA(String selectClause, List<SpecificLinkParameter> specificLinkParameters, List<String> filterParams) throws SQLException, ClassNotFoundException {
        List<String> json = new ArrayList<String>();
        try {
            String sql = selectClause;
            for (int i = 0; i < specificLinkParameters.size(); i++) {
                sql += " [" + specificLinkParameters.get(i).getName() + "] = ? AND ";
            }
            if(specificLinkParameters.size() > 0) {
                sql = sql.substring(0, sql.lastIndexOf("AND"));
            }
            conn = getConnection();
            stm = conn.prepareStatement(sql);
            for (int i = 1; i <= specificLinkParameters.size(); i++) {
//                try {
//                    int number = Integer.parseInt(filterParams.get(i - 1));
//                    stm.setInt(i, number);
//                } catch (Exception e) {
//                    stm.setString(i, filterParams.get(i - 1));
//                }
                stm.setString(i, filterParams.get(i - 1));
            }
            rs = stm.executeQuery();
            json = convertResultSetToJson(rs);
        } finally {
            closeConnection();
        }
        return json;
    }
}
