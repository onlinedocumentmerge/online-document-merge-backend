/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.odm.utils;

/**
 *
 * @author 84918
 */
public class CONST {
    public static final int CONNECTION_SQLSERVER = 1;
    public static final int CONNECTION_MYSQL = 2;
    public static final int TYPE_DATA_STRING = 1;
    public static final int TYPE_DATA_INTEGER = 2;
    public static final int TYPE_DATA_FLOAT = 3;
    public static final int TYPE_DATA_DATATIME = 4;
    public static final int TYPE_DATA_EMAIL = 5;
    public static final int TYPE_CONNECT_MYSQL = 1;
    public static final int TYPE_CONNECT_SQLSERVER = 2;
    public static final int TYPE_CONNECT_ORACLE = 3;
//    public static final String CONNECTION_STRING = "jdbc:sqlserver://cloudprc391.database.windows.net:1433;databaseName=DataSource;encrypt=true;trustServerCertificate=false;hostNameInCertificate=*.database.windows.net;loginTimeout=30;username=cloudprc391;password=Q!234567";
//    public static final String CONNECTION_USERNAME = "cloudprc391";
//    public static final String CONNECTION_PASSWORD = "Q!234567";
    public static final String CONNECTION_STRING = "jdbc:sqlserver://localhost:1433;databaseName=DataSource;user=sa;password=123;";
    public static final String CONNECTION_USERNAME = "sa";
    public static final String CONNECTION_PASSWORD = "123";
    public static final int TYPE_ALGORITHM_SUM = 1;
    public static final int TYPE_ALGORITHM_AVERAGE = 2;
    public static final int TYPE_ALGORITHM_MIN = 3;
    public static final int TYPE_ALGORITHM_MAX = 4;
    public static final int TYPE_ALGORITHM_COUNT = 5;
    public static final int TYPE_ALGORITHM_TODAY = 6;
    public static final int TYPE_ALGORITHM_DATE = 7;
    public static final int TYPE_ALGORITHM_NOW = 8;
    public static final int TYPE_ALGORITHM_IF = 9;
    public static final int TYPE_ALGORITHM_NOW_24H = 1;
    public static final int TYPE_ALGORITHM_NOW_12H = 2;
    public static final String DATETIME_REGEX_DEFAULT = "/";
    public static final String DATETIME_FORMAT_DEFAULT = "dd/MM/yyyy";
    public static final String CALCULATE_FAIL = "FAIL VALUE";
    public static final int PAYMENT_STATUS_PENDING = 1;
    public static final int PAYMENT_STATUS_COMPLETED = 2;
    public static final int PAYMENT_STATUS_FAILED = 3;
    public static final int PAYMENT_DEPOSIT = 0;
    public static final int PAYMENT_PAID = 1;
    public static final int ENABLE_JOB = -1;
    public static final int ACTIVE_JOB = 1;
    public static final int PENDING_JOB = 2;
    public static final int IN_QUEUE_JOB = 3;
    public static final int EXECUTE_JOB = 4;
    public static final int COMPLETED_JOB = 5;
}
