package com.odm.utils;

import com.odm.model.ArithmeticRequest;
import odm.arithmetic.iarithmetic.IArithmeticService;
import org.reflections.Reflections;
import org.reflections.scanners.SubTypesScanner;

import java.io.File;
import java.io.FileFilter;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.*;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

public class MathUtils2 {
    private ODMUtils utils = new ODMUtils();
    private int length = 0;
    boolean checkInput = true; // check input value
    public List<String> operands = new ArrayList<>();
    public String[] postfix = new String[1000];
    public List<IArithmeticService> arithmetics = new ArrayList<>();
    private String pathName = getArithmeticDir();

    public MathUtils2() {
    }

    public Object calculate(String input) {

        return calculateString(createPostFix(input));
    }

    public String getArithmeticDir() {
        File dir = new File(System.getProperty("user.dir"));
        dir = new File(dir, "src");
        dir = new File(dir, "main");
        dir = new File(dir, "java");
        dir = new File(dir, "com");
        dir = new File(dir, "odm");
        dir = new File(dir, "arithmetic");

        //System.out.println("Using data directory: " + dir.toString());
        return dir.toString() + File.separator;
    }

    public String runIArithmetic(String input) {
        String result = "";
        System.out.println("size arithmetic: " + arithmetics.size());
        for (IArithmeticService arithmetic : arithmetics) {
            String operator = arithmetic.getOperator();
            //if input have operator in header
            if (input.indexOf(operator + "(") == 0) {
//                    Method calculate = clas.getMethod("calculate", List.class);
                //subString operator
                String cal = input.substring(input.indexOf("(") + 1, input.lastIndexOf(")"));
                //list inputs after split
                List<String> inputs = splitString(cal);
                //list input send to method caculate in file jar
                List<Object> listInput = new ArrayList<Object>();
                //get min parameter number of this arithmetic
                Object min = arithmetic.getMinParameter();
                //get max parameter number of this arithmetic
                Object max = arithmetic.getMaxParameter();
                if (inputs.size() >= Integer.parseInt(min.toString())
                        && inputs.size() <= Integer.parseInt(max.toString())) {
                    for (int i = 0; i < inputs.size(); i++) {
                        String input1 = inputs.get(i);
                        //caculate input1 if input isn't a numberr
                        if (utils.getTypeOfVariable(input1) != CONST.TYPE_DATA_FLOAT
                                && utils.getTypeOfVariable(input1) != CONST.TYPE_DATA_INTEGER) {
                            inputs.set(i, calculate(input1).toString());
                        }
                        System.out.println("trong arithmetic: " + inputs.get(i));
                        //after caculate input1, if it isn't a number --> FAIL
                        if (utils.getTypeOfVariable(inputs.get(i)) != CONST.TYPE_DATA_FLOAT
                                && utils.getTypeOfVariable(inputs.get(i)) != CONST.TYPE_DATA_INTEGER) {
                            return "FAIL VALUE";
                        } else {
                            listInput.add(inputs.get(i));
                        }

                    }
                    Object ob1 = arithmetic.calculate(listInput);
                    result = ob1.toString();
                } else {
                    return "";
                }
            }
        }
        return result;
    }

    public List<String> splitString(String s) {
        int begin = 0;
        int count = 0;
        List<String> list = new ArrayList<>();
        for (int i = 0; i < s.length(); i++) {

            if (s.charAt(i) == '(') {
                count++;
            } else if (s.charAt(i) == ')') {
                count--;
            } else if (s.charAt(i) == ',' && count == 0) {
                String s1 = s.substring(begin, i);
                begin = i + 1;
                list.add(s1.trim());
            }
        }
        list.add(s.substring(begin, s.length()));
        return list;
    }

    public String[] createPostFix(String s) {
        s = s.replaceAll(" ", "");
        s += " ";
        Stack<Character> stack = new Stack<>();
        String[] postfix = new String[1000];
        int j = 0;
        int i = 0;
        char x;
        length = 0;
        try {
            int lengthStr = s.length();
            lengthStr--;
            while (j < lengthStr) {
                if ((s.charAt(j) - 0) == 0) {
                    break;
                }
                if (isANumber(s.charAt(j)) || (s.charAt(j) == '-' && isANumber(s.charAt(j + 1)))) {
                    int num1 = 0; //num before  floating point numbers
                    int num2 = 0; //num behind floating point numbers
                    boolean check = false; //check floating point numbers
                    boolean checkNegative = false;
                    if (s.charAt(j) == '-' && isANumber(s.charAt(j + 1))) {
                        checkNegative = true;
                        j++;
                    }
                    while ((isANumber(s.charAt(j)) || s.charAt(j) == '.') && j < lengthStr) {
                        //false -> before || true -> behind
                        //index of char '0' = 48 (in ASCII)
                        if (s.charAt(j) != '.') {
                            if (check) {
                                num2 = num2 * 10 + (s.charAt(j) - 48);
                            } else {
                                num1 = num1 * 10 + (s.charAt(j) - 48);
                            }
                        } else {
                            check = true;
                        }
                        j++;
                    }
                    BigDecimal num = null;
                    if (checkNegative) {
                        num = new BigDecimal("-" + num1 + "." + num2);
                    } else {
                        num = new BigDecimal(num1 + "." + num2);
                    }
                    postfix[i++] = num + "";
                    length++;
                    operands.add(num + ""); //
                } else if (!isANumber(s.charAt(j)) && s.charAt(j) != ' ' && s.charAt(j) != '+' && s.charAt(j) != '-'
                        && s.charAt(j) != '*' && s.charAt(j) != '/') {
                    String col = "";
                    int inExpression = 0;
                    while (((!isANumber(s.charAt(j)) && s.charAt(j) != ' '
                            && s.charAt(j) != '+' && s.charAt(j) != '-'
                            && s.charAt(j) != '*' && s.charAt(j) != '/')
                            || (isANumber(s.charAt(j)))
                            || (s.charAt(j) == '(') || (s.charAt(j) == ')')
                            || (inExpression != 0 && (s.charAt(j) == '+' || s.charAt(j) != '-'
                            || s.charAt(j) == '/' || s.charAt(j) == '*')))
                            && j < lengthStr) {
                        if (s.charAt(j) == '(') {
                            inExpression++;
                        }
                        if (s.charAt(j) == ')') {
                            inExpression--;
                        }
                        col += s.charAt(j);
                        j++;
                    }
                    length++;
                    operands.add(col); //lưu toán hạng
                    postfix[i++] = col;
                } else if (s.charAt(j) == '(') {
                    stack.push(s.charAt(j));
                    j++;
                } else if (s.charAt(j) == ')') {
                    while ((x = stack.pop()) != '(') {
                        postfix[i++] = x + "";
                        length++;
                    }
                    j++;
                } else {
                    while (!stack.isEmpty() && (Precedence(s.charAt(j)) <= Precedence(stack.peek()))) {
                        x = stack.pop();
                        postfix[i++] = x + "";
                        length++;
                    }
//                    postfix[i++] = " ";
                    stack.push(s.charAt(j));
                    j++;
                }

            }
            while (!stack.isEmpty()) {
                postfix[i++] = stack.pop() + "";
                length++;
            }
            for (int z = 0; z < i; z++) {
                System.out.print(postfix[z]);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return postfix;
    }

    public String calculateString(String[] postfix) {
        boolean checkCalculate = true;
        Stack<BigDecimal> stack = new Stack<>();
        try {
            System.out.println("Length: " + length);
            int size = length;
            for (int i = 0; i < size; i++) {
                String string = postfix[i];
                System.out.println("String trong postfix: " + string);
                if (isANumber(string)) {
                    BigDecimal tmp = new BigDecimal(string);
                    stack.push(tmp);
                } else if (!isANumber(string) && !string.equals("+")
                        && !string.equals("-") && !string.equals("*")
                        && !string.equals("/")) {
                    //check arithmetic
                    String result = runIArithmetic(string);
                    if (isANumber(result)) {
                        BigDecimal tmp = new BigDecimal(result);
                        stack.push(tmp);
                    } else {
                        return CONST.CALCULATE_FAIL;
                    }
                } else {
                    BigDecimal num1 = stack.pop();
                    BigDecimal num2 = stack.pop();
                    BigDecimal result = new BigDecimal("0");

                    switch (string) {
                        case "+":
                            result = num2.add(num1);
                            break;
                        case "-":
                            result = num2.subtract(num1);
                            break;
                        case "*":
                            result = num2.multiply(num1);
                            break;
                        case "/":
                            result = num2.divide(num1, 8, RoundingMode.HALF_DOWN);
                            break;
                        default:
                            System.out.println("FAIL operation :" + string);
                            return CONST.CALCULATE_FAIL;
                    }
                    stack.push(result);
                }
            }
        } catch (Exception e) {
            checkCalculate = false;
        }
        if (checkCalculate) {
            return stack.pop().toString();
        } else {
            return "FAIL VALUE";
        }
    }

    public boolean isANumber(char c) {
        return ((c - 48) >= 0 && (c - 48) <= 9);
    }

    public static int Precedence(char x) {
        if (x == '(') {
            return 0;
        }
        if (x == '+' || x == '-') {
            return 1;
        }
        if (x == '*' || x == '/') {
            return 2;
        }
        return 3;
    }

    public int loadJarFile() {
        File[] jarFiles = null;
        try {

            //load file jar from package
            ClassLoader loader = MathUtils.class.getClassLoader();
            URLClassLoader urlCL = (URLClassLoader) loader;
            File libFolder = new File(pathName);
            jarFiles = libFolder.listFiles(new FileFilter() {
                public boolean accept(File pathname) {
                    boolean result = false;
                    if (pathname.isFile()) {
                        result = pathname.getName().endsWith("jar");
                    }
                    return result;
                }
            });
            Method m = URLClassLoader.class.getDeclaredMethod("addURL", URL.class);
            m.setAccessible(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (jarFiles != null) {
            return jarFiles.length;
        } else {
            return 0;
        }
    }

    public List<ArithmeticRequest> loadArithmetic() {
        List<ArithmeticRequest> listArithmetic = new ArrayList<>();

        try {

            //load arithmetic in class
            Reflections reflections = new Reflections("com.odm.arithmetic", new SubTypesScanner(false));
            Set<Class<?>> classes = reflections.getSubTypesOf(Object.class);

            for (Class<?> clas : classes) {
                Constructor<?> ctor = clas.getConstructor();
                IArithmeticService instance = (IArithmeticService) ctor.newInstance();
                String operator = instance.getOperator();
                int min = instance.getMinParameter();
                int max = instance.getMaxParameter();
                ArithmeticRequest dto = new ArithmeticRequest(operator, min, max, instance.description());
                listArithmetic.add(dto);
            }
            //end load arithmetic in class

            //load arithmetic in jar file
            File libFolder = new File(pathName);
            File[] jarFiles = libFolder.listFiles(new FileFilter() {
                public boolean accept(File pathname) {
                    boolean result = false;
                    if (pathname.isFile()) {
                        result = pathname.getName().endsWith("jar");
                    }
                    return result;
                }
            });
            for (File jarFile : jarFiles) {
//              //package default
                Class classToLoad = getClassArithmeticInJar(jarFile);

                //get operator
                Method operatorMethod = classToLoad.getDeclaredMethod("getOperator");
                Method minParamMethod = classToLoad.getDeclaredMethod("getMinParameter");
                Method maxParamMethod = classToLoad.getDeclaredMethod("getMaxParameter");
                Method descriptionMethod = classToLoad.getDeclaredMethod("description");

                Object instance = classToLoad.newInstance();

                Object operator = operatorMethod.invoke(instance);
                Object min = minParamMethod.invoke(instance);
                Object max = maxParamMethod.invoke(instance);
                Object description = descriptionMethod.invoke(instance);
                int minParam = Integer.parseInt(min.toString());
                int maxParam = Integer.parseInt(max.toString());
                ArithmeticRequest dto = new ArithmeticRequest(operator.toString(), minParam, maxParam, description.toString());
                listArithmetic.add(dto);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return listArithmetic;
    }

    public IArithmeticService castClassToInterface(File input) {
        Class classToLoad = null;
        IArithmeticService result = null;
        try { //cast file to Interface
            classToLoad = getClassArithmeticInJar(input);
            IArithmeticService instance = (IArithmeticService) classToLoad.newInstance();
            result = instance;
        } catch (Exception e) { //cast fail
            try {
                classToLoad = getClassArithmeticInJar(input);
                Object instance = classToLoad.newInstance();
                //get operator method
                Method operatorMethod = classToLoad.getDeclaredMethod("getOperator");
                //get min parameter method
                Method getMinParameter = classToLoad.getDeclaredMethod("getMinParameter");
                //get max parameter method
                Method getMaxParameter = classToLoad.getDeclaredMethod("getMaxParameter");
                //get method calculate
                Method calculateMethod = classToLoad.getDeclaredMethod("calculate", List.class
                );
                result = new IArithmeticService() {
                    @Override
                    public Object calculate(List<Object> inputs) {
                        Object result = null;
                        try {
                            result = calculateMethod.invoke(instance, inputs);
                        } catch (Exception e) {
                        }
                        return result;
                    }

                    @Override
                    public int getMinParameter() {
                        int result = 0;
                        try {
                            result = Integer.parseInt(getMinParameter.invoke(instance).toString());
                        } catch (Exception e) {
                        }
                        return result;
                    }

                    @Override
                    public int getMaxParameter() {
                        int result = 0;
                        try {
                            result = Integer.parseInt(getMaxParameter.invoke(instance).toString());
                        } catch (Exception e) {
                        }
                        return result;
                    }

                    @Override
                    public String getOperator() {
                        String result = "";
                        try {
                            result = operatorMethod.invoke(instance).toString();
                        } catch (Exception e) {
                        }
                        return result;
                    }

                    @Override
                    public boolean isSQL() {
                        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                    }

                    @Override
                    public String sqlStatement(List<String> inputs) {
                        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                    }

                    @Override
                    public String description() {
                        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                    }
                };
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        return result;
    }

    public Class getClassArithmeticInJar(File file) {
        try {

            JarFile jarFile = new JarFile(file.toPath().toString());
            Enumeration<JarEntry> e = jarFile.entries();

            URL[] urls = {new URL("jar:file:" + file.toPath() + "!/")};
            URLClassLoader cl = URLClassLoader.newInstance(urls);

            //get element in jarfile
            while (e.hasMoreElements()) {
                JarEntry je = e.nextElement();
                if (je.isDirectory() || !je.getName().endsWith(".class")) {
                    continue;
                }
                // -6 because of .class
                String className = je.getName().substring(0, je.getName().length() - 6);
                className = className.replace('/', '.');
                Class c = cl.loadClass(className);
                //get interfaces of this class
                Class[] interFace = c.getInterfaces();

                for (Class class1 : interFace) {
                    String interfaceName = class1.getName();
                    interfaceName = interfaceName.substring(interfaceName.lastIndexOf(".") + 1, interfaceName.length());
                    //if interface of this class is IArithmeticService (interface default)
                    if (interfaceName.equals("IArithmeticService")) {
                        return c;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public boolean isANumber(String s) {
        try {
            float f = Float.parseFloat(s);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    public void loadAllArithmetic() {
        try {
            //load arithmetic in class
            Reflections reflections = new Reflections("com.odm.arithmetic", new SubTypesScanner(false));
            Set<Class<?>> classes = reflections.getSubTypesOf(Object.class);
            for (Class<?> clas : classes) {
                Constructor<?> ctor = clas.getConstructor();
                IArithmeticService instance = (IArithmeticService) ctor.newInstance();
                arithmetics.add(instance);
            }
            //end load arithmetic in class
            //load arithmetic in jar file
            File libFolder = new File(pathName);
            File[] jarFiles = libFolder.listFiles(new FileFilter() {
                public boolean accept(File pathname) {
                    boolean result = false;
                    if (pathname.isFile()) {
                        result = pathname.getName().endsWith("jar");
                    }
                    return result;
                }
            });
            for (File jarFile : jarFiles) {
//              //package default
                IArithmeticService instance = castClassToInterface(jarFile);
                arithmetics.add(instance);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<IArithmeticService> getArithmetics() {
        return arithmetics;
    }

    public void setArithmetics(List<IArithmeticService> arithmetics) {
        this.arithmetics = arithmetics;
    }
}
