/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.odm.utils;

import com.itextpdf.text.DocumentException;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import com.odm.serviceImpl.FirebaseService;

/**
 *
 * @author 84918
 */
public class ZipDocumentUtils extends SimpleFileVisitor<Path> {

    private static ZipOutputStream zos;

    private Path sourceDir;

    public ZipDocumentUtils(Path sourceDir) {
        this.sourceDir = sourceDir;
    }

    public ZipDocumentUtils() {
    }

    @Override
    public FileVisitResult visitFile(Path file,
                                     BasicFileAttributes attributes) {

        try {
            Path targetFile = sourceDir.relativize(file);

            zos.putNextEntry(new ZipEntry(targetFile.toString()));

            byte[] bytes = Files.readAllBytes(file);
            zos.write(bytes, 0, bytes.length);
            zos.closeEntry();

        } catch (IOException ex) {
            System.err.println(ex);
        }

        return FileVisitResult.CONTINUE;
    }

    //convert htmls to pdf files
    public String convertHtmlToFile(String name, List<String> htmls) throws IOException {
        ODMUtils utils = new ODMUtils();
        String path = utils.getDataDir(ZipDocumentUtils.class);
        OutputStream os = null;
        //create folder/ folder name = AccuntId
        String pathFolder = path + name;
        new File(pathFolder).mkdir();

//        try {
//            int i = 0;
//            for (String html : htmls) {
//                String outputFile = pathFolder + "\\ODM" + i + ".pdf";
//                i++;
//                os = new FileOutputStream(outputFile);
//                ITextRenderer renderer = new ITextRenderer();
//                renderer.setDocumentFromString(html);
//                renderer.layout();
//                renderer.createPDF(os);
//            }
//        } catch (DocumentException e) {
//            e.printStackTrace();
//        } finally {
//            if (os != null) {
//                os.close();
//            }
//        }
        return pathFolder;
    }

    //zip folder constain pdf files
    public void zipDocument(String name, List<String> htmls) throws IOException{

        //convert htmls to pdf files
        String dirPath = convertHtmlToFile(name, htmls);

        //path folder constain pdf file
        Path sourceDir = Paths.get(dirPath);

        //zip folder
        try {
            String zipFileName = dirPath.concat(".zip");
            zos = new ZipOutputStream(new FileOutputStream(zipFileName));

            Files.walkFileTree(sourceDir, new ZipDocumentUtils(sourceDir));

            zos.close();
        } catch (IOException ex) {
            System.err.println("I/O Error: " + ex);
        }
        //after zip file -> delete folder constain doc (file pdf)
        File directory = new File(dirPath + ".zip");
        if (!directory.exists()) {
            System.out.println("Directory does not exist.");
        } else {
            try {
                FirebaseService firebaseService = new FirebaseService();
                firebaseService.uploadFile("zipFile1", directory);
                delete(directory);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        //save zip file in firebase


//        Bổ sung ở đây

        // đường dẫn của file zip ở đây -->>>>>> dirPath + ".zip"

        //delete zip file
        //delete(new File(dirPath + ".zip"));
    }

    public static void delete(File file) throws IOException {

        if (file.isDirectory()) {
            //directory is empty, then delete it
            if (file.list().length == 0) {
                file.delete();
                System.out.println("Directory is deleted : "
                        + file.getAbsolutePath());
            } else {
                //list all the directory contents
                String files[] = file.list();
                for (String temp : files) {
                    //construct the file structure
                    File fileDelete = new File(file, temp);
                    //recursive delete
                    delete(fileDelete);
                }
                //check the directory again, if empty then delete it
                if (file.list().length == 0) {
                    file.delete();
                }
            }
        } else {
            //if file, then delete it
            file.delete();
        }
    }
}
