package com.odm.utils;

import java.util.Date;

import org.slf4j.Logger;
import org.springframework.stereotype.Component;

import com.odm.model.CustomUserDetails;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.UnsupportedJwtException;

@Component
public class JwtTokenProvider {
	Logger log;
    //private final String JWT_SECRET = "lodaaaaaa";
    private final String JWT_SECRET = "-----BEGIN PRIVATE KEY-----\nMIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQCkmUUkF3VrA9wy\nU9sFfjxtCaSTl4KV5pjOiDIloitL6kIQMguvDTa1mCO006W9FbTAIgJpTgZGMOs/\njMllN9kQXiB9xOihwZXE6PcdnIP0BVslxEavqNd7k+jbIIFrQjvM28Y5xbqnJigr\n2AcDb5lvrPgC5KwnAxxgDAe2kx16C3PX9U8Jx3AZD52veFcwQL5JLlYvFoAowyY3\nuNaLfWsLg290mU1zbMEOfrxLvWwAeuXri0FjiIW4WwY7dQnGq2VLbvrY4y469ilF\nWI9P8ckiAS6K7uFLxdE38bmvdujHGhOSGliqGQXlk0Tx1R2onm4qDbBVo285UDp8\nkh5RT9mrAgMBAAECggEALDVGTrVGQiHdSF8hWk5XaGnInCN7XUt8CyylW1o9Cry2\nVac7YB7KbOeZ331g7HIiA+CeiS39nWiSJpVnmhnv4OeHDoCOgrN9yE9R39Aj2Kx/\nmJso6REOHxDQ39nRKL+3VARLL0UeJrqm22/AInoc2dO5IP3OBwRc4KcagzzsE9UF\nG8lg4RKeHBv+q8g2nAmqi5LFgNGZ0qqBnL5IAt24+8OM44MAW/pGcyHQpXRhvUA1\nGMPOOlUkr/YbXyHqEnsOLhMOCNlLzstI4hVxbkiBuuvFpX7spnXJ7LZ0gvdAwL0p\nDZbqiPo11tl8BX8M9DRV8furANciLwMmiYSpTBcCMQKBgQDbVp+7fJrUO6kUaSCz\ncFmGK1iejODirxwI/wfLX6E8t/Q3qCzlwnIQlxuc63pCHkI/0aVAy/k4tY1XJvSZ\nUvJhf+CZB0sotsMSpMTYGkGgJXTeCAuz78l56l9M6I3MSlhFNVF3NL5kmwMTh4PW\nJUiWlc5QizxcQMKRXDb9ZHaetQKBgQDAHF03e43yptSmikczD4MP5kIj51WpDFY3\nrEmJ+gH/PbT21JlCj7Gdc6GMO4J/wXC5hH3Ly8f6TH/4tj06uLO9iDrA78R1NiRC\nJshQKASSd2vfoggsq5AhKeexJCs7QxtPypdBZBCtbzk1W2FFXxLLAjROeeLbJlFe\nXIzE8HNy3wKBgHlAMLsPTfQHKxfqabZoZEUET1AOpv8JOGFMwn1N1y9erbXa1T8K\nN84uCoi7r/E/EvZ+qXDhkVslGgZUtaDQtsbpTzqpSoYuSjZTObIRCbhDHJDlN7r3\n0pP/2UEFyeVmL9fKeseAM8Qf4Rp5rorCP5dnJw9leS+yaRy8MnXvhJeFAoGAERV0\npK50HRQA7R523m+OtCdB0neMApB6SlEj6QU2kEAoi5q0Vlr47IuP3iycmkfVgmnp\n1em1SujqqG9sWoB6W2x17RplWhcKemeAgMBOR8ge67A5Ed/f3VMgq3wP7CE3mIzn\njoUbhpDRgZEywTyHK05y4FeueNabfcy1J/5KBfkCgYBm68LqxtRy7+b0pveR2lOj\nHHGrfc+3FRb4srbyDOVhGobkOcEqvSQucYg/hl/G0GTzWv0UYLh3PNGj4IST2IOa\nBprB1FG/SlhUDw0T6i+08KXijLqRuyxIfd51uoCl5oEIav3H9zrXAL1DHYuSh0XO\nK0LnBkrcJmo+/XU0wOAVdg==\n-----END PRIVATE KEY-----\n";
    private final long JWT_EXPIRATION = 604800000L;

    public String generateToken(CustomUserDetails userDetails) {
    	if(userDetails.getAccount().getActive() == true) {
    		// Lấy thông tin user
            Date now = new Date();
            Date expiryDate = new Date(now.getTime() + JWT_EXPIRATION);
            // Tạo chuỗi json web token từ id của user.

            String token = Jwts.builder()
                    .setSubject(Integer.toString(userDetails.getAccount().getId()))
                    .setIssuedAt(now)
                    .setExpiration(expiryDate)
                    .signWith(SignatureAlgorithm.HS512, JWT_SECRET)
                    .compact();

            return token;
    	}else {
			return "Account is disabled";
		}
        
    }

    public int getUserIdFromJWT(String token) {
        Claims claims = Jwts.parser()
                            .setSigningKey(JWT_SECRET)
                            .parseClaimsJws(token)
                            .getBody();

        return Integer.parseInt(claims.getSubject());
    }

    public boolean validateToken(String authToken) {
        try {
            Jwts.parser().setSigningKey(JWT_SECRET).parseClaimsJws(authToken);
            return true;
        } catch (MalformedJwtException ex) {
            log.error("Invalid JWT token");
        } catch (ExpiredJwtException ex) {
            log.error("Expired JWT token");
        } catch (UnsupportedJwtException ex) {
            log.error("Unsupported JWT token");
        } catch (IllegalArgumentException ex) {
            log.error("JWT claims string is empty.");
        }
        return false;
    }
}