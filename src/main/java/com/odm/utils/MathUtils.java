package com.odm.utils;

import com.odm.model.ArithmeticModel;
import com.odm.model.ArithmeticRequest;
import odm.arithmetic.iarithmetic.IArithmeticService;
import org.reflections.Reflections;
import org.reflections.scanners.SubTypesScanner;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import java.io.File;
import java.io.FileFilter;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.*;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

public class MathUtils {
    private ODMUtils utils = new ODMUtils();
    private int length = 0;
    private String pathName = getDirPath();

    public MathUtils() {
    }

    private String getDirPath() {
        File dir = new File(System.getProperty("user.dir"));
        dir = new File(dir, "src");
        dir = new File(dir, "main");
        dir = new File(dir, "java");
        dir = new File(dir, "com");
        dir = new File(dir, "odm");
        dir = new File(dir, "arithmetic");
        return dir.getPath() + File.separator;
    }

    public Object calculate(String input) {
        Object result = null;
        try {
            //Begin calculate in class
            //get list class in package
            Reflections reflections = new Reflections("com.odm.arithmetic", new SubTypesScanner(false));
            Set<Class<?>> classes = reflections.getSubTypesOf(Object.class);

            for (Class<?> clas : classes) {
                Constructor<?> ctor = clas.getConstructor();
                IArithmeticService instance = (IArithmeticService) ctor.newInstance();
                //get operator
                result = runIArithmetic(instance, input);
                if (result.toString().equals(CONST.CALCULATE_FAIL)) {
                    return CONST.CALCULATE_FAIL;
                } else if (!result.toString().equals("")) {
                    return result;
                }
            }
            //End calculate in class
            //If calculate in class fail -> find in jar file
            //Begin calculate in jar file
            File libFolder = new File(pathName);
            File[] jarFiles = libFolder.listFiles(new FileFilter() {
                public boolean accept(File pathname) {
                    boolean result = false;
                    if (pathname.isFile()) {
                        result = pathname.getName().endsWith("jar");
                    }
                    return result;
                }
            });

            for (File jarFile : jarFiles) {
                IArithmeticService instance = castClassToInterface(jarFile);
                result = runIArithmetic(instance, input);
                if (result.toString().equals(CONST.CALCULATE_FAIL)) {
                    return CONST.CALCULATE_FAIL;
                } else if (!result.toString().equals("")) {
                    return result;
                }
            }
            //End calculate in jar file

            //if it's can not calculate in class and jar file   -> just calculate String
            if (result.toString().equals("")) {
                result = calculateString(input) + "";
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        //if result = "" -> input error
        return result;
    }

    public String runIArithmetic(IArithmeticService instance, String input) {

        String result = "";
        String operator = instance.getOperator();
        //if input have operator in header
        if (input.indexOf(operator + "(") == 0) {
//                    Method calculate = clas.getMethod("calculate", List.class);
            //subString operator
            String cal = input.substring(input.indexOf("(") + 1, input.lastIndexOf(")"));
            //list inputs after split
            List<String> inputs = splitString(cal);
            //list input send to method caculate in file jar
            List<Object> listInput = new ArrayList<Object>();
            //get min parameter number of this arithmetic
            Object min = instance.getMinParameter();
            //get max parameter number of this arithmetic
            Object max = instance.getMaxParameter();
            if (inputs.size() >= Integer.parseInt(min.toString())
                    && inputs.size() <= Integer.parseInt(max.toString())) {
                for (int i = 0; i < inputs.size(); i++) {
                    String input1 = inputs.get(i);
                    //caculate input1 if input isn't a numberr
                    if (utils.getTypeOfVariable(input1) != CONST.TYPE_DATA_FLOAT
                            && utils.getTypeOfVariable(input1) != CONST.TYPE_DATA_INTEGER) {
                        inputs.set(i, calculate(input1).toString());
                    }
                    //after caculate input1, if it isn't a number --> FAIL
                    if (utils.getTypeOfVariable(inputs.get(i)) != CONST.TYPE_DATA_FLOAT
                            && utils.getTypeOfVariable(inputs.get(i)) != CONST.TYPE_DATA_INTEGER) {
                        return "FAIL VALUE";
                    } else {
                        listInput.add(inputs.get(i));
                    }

                }
                Object ob1 = instance.calculate(listInput);
                result = ob1.toString();
            } else {
                return "";
            }

        }
        return result;
    }

    public List<String> splitString(String s) {
        int begin = 0;
        int count = 0;
        List<String> list = new ArrayList<>();
        for (int i = 0; i < s.length(); i++) {

            if (s.charAt(i) == '(') {
                count++;
            } else if (s.charAt(i) == ')') {
                count--;
            } else if (s.charAt(i) == ',' && count == 0) {
                String s1 = s.substring(begin, i);
                begin = i + 1;
                list.add(s1.trim());
            }
        }
        list.add(s.substring(begin, s.length()).trim());
        return list;
    }

    public char[] createPostFix(String s) {
        Stack<Character> stack = new Stack<>();
        char[] postFix = new char[100];
        int j = 0;
        char x;
        for (int i = 0; i < s.length(); i++) {

            char value = s.charAt(i);
            if (isANumber(value) || (value == '.' && isANumber(s.charAt(i + 1)) && isANumber(s.charAt(i - 1)))
                    || (value == '-' && isANumber(s.charAt(i + 1)) && !isANumber(s.charAt(i - 1)))) {
                //float/integer number solution or negative solution '-'
                postFix[j++] = value;
            } else if (value == '(') {
                stack.push(value);
            } else if (value == ')') {
                while ((x = stack.pop()) != '(') {
                    postFix[j++] = x;
                }
            } else {
                while (!stack.isEmpty() && (Precedence(value) <= Precedence(stack.peek()))) {
                    x = stack.pop();
                    postFix[j++] = x;
                }
                postFix[j++] = ' ';
                stack.push(value);
            }
        }
        while (!stack.isEmpty()) {
            postFix[j++] = stack.pop();
        }
        for (int i = 0; i <= j; i++) {
            System.out.print(postFix[i]);
        }
        System.out.println();
        length = j;
        return postFix;
    }

    public String calculateString(String s) {
        s = s.replaceAll(" ", "");
        System.out.println("input : " + s);
        char[] postFix = createPostFix(" " + s);
        boolean checkCalculate = true;
        Stack<BigDecimal> stack = new Stack<>();
        int j = 0;
        try {
            while (j < length) {
                while (postFix[j] == ' ' || postFix[j] == '\t') {
                    j++;
                }
                if (isANumber(postFix[j]) || (postFix[j] == '-' && isANumber(postFix[j + 1]))) {
                    int num1 = 0; //num before  floating point numbers
                    int num2 = 0; //num behind floating point numbers
                    boolean check = false; //check floating point numbers
                    boolean checkNegative = false;
                    if (postFix[j] == '-' && isANumber(postFix[j + 1])) {
                        checkNegative = true;
                        j++;
                    }
                    while (isANumber(postFix[j]) || postFix[j] == '.') {
                        //false -> before || true -> behind
                        //index of char '0' = 48 (in ASCII)
                        if (postFix[j] != '.') {
                            if (check) {
                                num2 = num2 * 10 + (postFix[j] - 48);
                            } else {
                                num1 = num1 * 10 + (postFix[j] - 48);
                            }
                        } else {
                            check = true;
                        }
                        j++;
                    }
                    BigDecimal num = null;
                    if (checkNegative) {
                        num = new BigDecimal("-" + num1 + "." + num2);
                    } else {
                        num = new BigDecimal(num1 + "." + num2);
                    }

                    stack.push(num);
                } else {
                    BigDecimal num1 = stack.pop();
                    BigDecimal num2 = stack.pop();
                    BigDecimal result = new BigDecimal("0");

                    switch (postFix[j]) {
                        case '+':
                            result = num2.add(num1);
                            break;
                        case '-':
                            result = num2.subtract(num1);
                            break;
                        case '*':
                            result = num2.multiply(num1);
                            break;
                        case '/':
                            result = num2.divide(num1, 8, RoundingMode.HALF_DOWN);
                            break;
                        default:
                            System.out.println("FAIL operation");
                            return "FAIL VALUE";
                    }
                    stack.push(result);
                    j++;
                }

            }
        } catch (Exception e) {
            checkCalculate = false;
        }
        if (checkCalculate) {
            return stack.pop().toString();
        } else {
            return "FAIL VALUE";
        }
    }

    public boolean isANumber(char c) {
        return ((c - 48) >= 0 && (c - 48) <= 9);
    }

    public static int Precedence(char x) {
        if (x == '(') {
            return 0;
        }
        if (x == '+' || x == '-') {
            return 1;
        }
        if (x == '*' || x == '/') {
            return 2;
        }
        return 3;
    }

    public List<ArithmeticRequest> loadArithmetic() throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        List<ArithmeticRequest> listArithmetic = new ArrayList<>();

            //load arithmetic in class
            Reflections reflections = new Reflections("com.odm.arithmetic", new SubTypesScanner(false));
            Set<Class<?>> classes = reflections.getSubTypesOf(Object.class);

            for (Class<?> clas : classes) {
                Constructor<?> ctor = clas.getConstructor();
                IArithmeticService instance = (IArithmeticService) ctor.newInstance();
                String operator = instance.getOperator();
                int min = instance.getMinParameter();
                int max = instance.getMaxParameter();
                ArithmeticRequest dto = new ArithmeticRequest(operator, min, max, instance.description());
                listArithmetic.add(dto);
            }
            //end load arithmetic in class

            //load arithmetic in jar file
            File libFolder = new File(pathName);
            File[] jarFiles = libFolder.listFiles(new FileFilter() {
                public boolean accept(File pathname) {
                    boolean result = false;
                    if (pathname.isFile()) {
                        result = pathname.getName().endsWith("jar");
                    }
                    return result;
                }
            });
            for (File jarFile : jarFiles) {
//              //package default
                Class classToLoad = getClassArithmeticInJar(jarFile);

                //get operator
                Method operatorMethod = classToLoad.getDeclaredMethod("getOperator");
                Method minParamMethod = classToLoad.getDeclaredMethod("getMinParameter");
                Method maxParamMethod = classToLoad.getDeclaredMethod("getMaxParameter");
                Method descriptionMethod = classToLoad.getDeclaredMethod("description");

                Object instance = classToLoad.newInstance();

                Object operator = operatorMethod.invoke(instance);
                Object min = minParamMethod.invoke(instance);
                Object max = maxParamMethod.invoke(instance);
                Object description = descriptionMethod.invoke(instance);
                int minParam = Integer.parseInt(min.toString());
                int maxParam = Integer.parseInt(max.toString());
                ArithmeticRequest dto = new ArithmeticRequest(operator.toString(), minParam, maxParam, description.toString());
                listArithmetic.add(dto);
            }
        return listArithmetic;
    }

    public IArithmeticService castClassToInterface(File input) {
        Class classToLoad = null;
        IArithmeticService result = null;
        try { //cast file to Interface
            classToLoad = getClassArithmeticInJar(input);
            IArithmeticService instance = (IArithmeticService) classToLoad.newInstance();
            result = instance;
        } catch (Exception e) { //cast fail
            try {
                classToLoad = getClassArithmeticInJar(input);
                Object instance = classToLoad.newInstance();
                //get operator method
                Method operatorMethod = classToLoad.getDeclaredMethod("getOperator");
                //get min parameter method
                Method getMinParameter = classToLoad.getDeclaredMethod("getMinParameter");
                //get max parameter method
                Method getMaxParameter = classToLoad.getDeclaredMethod("getMaxParameter");
                //get max parameter method
                Method getDescription = classToLoad.getDeclaredMethod("description");
                //get method calculate
                Method calculateMethod = classToLoad.getDeclaredMethod("calculate", List.class);
                result = new IArithmeticService() {
                    @Override
                    public Object calculate(List<Object> inputs) {
                        Object result = null;
                        try {
                            result = calculateMethod.invoke(instance, inputs);
                        } catch (Exception e) {
                        }
                        return result;
                    }

                    @Override
                    public int getMinParameter() {
                        int result = 0;
                        try {
                            result = Integer.parseInt(getMinParameter.invoke(instance).toString());
                        } catch (Exception e) {
                        }
                        return result;
                    }

                    @Override
                    public int getMaxParameter() {
                        int result = 0;
                        try {
                            result = Integer.parseInt(getMaxParameter.invoke(instance).toString());
                        } catch (Exception e) {
                        }
                        return result;
                    }

                    @Override
                    public String getOperator() {
                        String result = "";
                        try {
                            result = operatorMethod.invoke(instance).toString();
                        } catch (Exception e) {
                        }
                        return result;
                    }

                    @Override
                    public String description() {
                        String result = "";
                        try {
                            result = getMaxParameter.invoke(instance).toString();
                        } catch (Exception e) {
                        }
                        return result;
                    }

                    @Override
                    public boolean isSQL() {
                        return false;
                    }

                    @Override
                    public String sqlStatement(List<String> list) {
                        return null;
                    }
                };
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        return result;
    }

    public Class getClassArithmeticInJar(File file) {
        try {

            JarFile jarFile = new JarFile(file.toPath().toString());
            Enumeration<JarEntry> e = jarFile.entries();

            URL[] urls = {new URL("jar:file:" + file.toPath() + "!/")};
            URLClassLoader cl = URLClassLoader.newInstance(urls);

            //get element in jarfile
            while (e.hasMoreElements()) {
                JarEntry je = e.nextElement();
                if (je.isDirectory() || !je.getName().endsWith(".class")) {
                    continue;
                }
                // -6 because of .class
                String className = je.getName().substring(0, je.getName().length() - 6);
                className = className.replace('/', '.');
                Class c = cl.loadClass(className);
                //get interfaces of this class
                Class[] interFace = c.getInterfaces();

                for (Class class1 : interFace) {
                    String interfaceName = class1.getName();
                    interfaceName = interfaceName.substring(interfaceName.lastIndexOf(".") + 1, interfaceName.length());
                    //if interface of this class is IArithmeticService (interface default)
                    if (interfaceName.equals("IArithmeticService")) {
                        return c;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
