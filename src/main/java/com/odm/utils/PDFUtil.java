package com.odm.utils;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.tool.xml.XMLWorker;
import com.itextpdf.tool.xml.XMLWorkerHelper;
import com.itextpdf.tool.xml.css.CssFile;
import com.itextpdf.tool.xml.css.StyleAttrCSSResolver;
import com.itextpdf.tool.xml.html.Tags;
import com.itextpdf.tool.xml.parser.XMLParser;
import com.itextpdf.tool.xml.pipeline.css.CSSResolver;
import com.itextpdf.tool.xml.pipeline.css.CssResolverPipeline;
import com.itextpdf.tool.xml.pipeline.end.PdfWriterPipeline;
import com.itextpdf.tool.xml.pipeline.html.AbstractImageProvider;
import com.itextpdf.tool.xml.pipeline.html.HtmlPipeline;
import com.itextpdf.tool.xml.pipeline.html.HtmlPipelineContext;
import org.jsoup.Jsoup;

import java.io.*;

public class PDFUtil {
    public static void createPdf(String HTML, String file) throws IOException, DocumentException {
        // step 1
        Document document = new Document();
        // step 2
        PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(file));
        // step 3
        document.open();

        CSSResolver cssResolver = new StyleAttrCSSResolver();
        CssFile cssFile = XMLWorkerHelper.getCSS(new ByteArrayInputStream("table {border-collapse: collapse;} table, th, td { border: 1px solid black;}".getBytes()));
        cssResolver.addCss(cssFile);

        // HTML
        HtmlPipelineContext htmlContext = new HtmlPipelineContext(null);
        htmlContext.setTagFactory(Tags.getHtmlTagProcessorFactory());
//        htmlContext.setLinkProvider(new LinkProvider() {
//            public String getLinkRoot() {
//                return RELATIVE_PATH;
//            }
//        });

        // Pipelines
        PdfWriterPipeline pdf = new PdfWriterPipeline(document, writer);
        HtmlPipeline html = new HtmlPipeline(htmlContext, pdf);
        CssResolverPipeline css = new CssResolverPipeline(cssResolver, html);

        // XML Worker
        XMLWorker worker = new XMLWorker(css, true);
        XMLParser p = new XMLParser(worker);
        p.parse(new FileInputStream(HTML));

        // step 5
        document.close();


//        // step 4
//        XMLWorkerHelper.getInstance().parseXHtml(writer, document,
//                new FileInputStream(HTML));
//        // step 5
//        document.close();
    }

    public static void tidyUp(String path,String dir) throws IOException {
        File html = new File(path);
        org.jsoup.nodes.Document document = Jsoup.parse(html, "US-ASCII");
        document.outputSettings().syntax(org.jsoup.nodes.Document.OutputSettings.Syntax.xml);

        byte[] xhtml = document.html().getBytes();
//        File dir = new File("results/xml");
//        dir.mkdirs();
        FileOutputStream fos = new FileOutputStream(new File(dir, html.getName()));
        fos.write(xhtml);
        fos.close();
    }
}
