package com.odm.utils;

import java.util.*;

public class TrieUtil {
    public static String Replace(String template, HashMap<String, String> map, Trie<Character> trie)
    {
        StringBuilder builder = new StringBuilder();
        int lastIdx = -1;
        Node<Character> lastNode = trie.getRoot();

        int matchIdx = -1;
        HashSet<Node> badMatches = new HashSet<Node>();

        int templateLength = template.length();

        for (int i = 0; i < templateLength + 1; i++)
        {
            if (i < templateLength)
            {
                char chr = template.charAt(i);
                if (lastNode.hasNext(chr))
                {
                    // Partial match
                    Node matchNode = lastNode.getChildren().get(chr);
                    if (!badMatches.contains(matchNode))
                    {
                        lastNode = matchNode;
                        if (matchIdx == -1)
                        {
                            matchIdx = i;
                        }
                        continue;
                    }
                }
            }

            if (lastNode.isLeaf())
            {
                // Complete match
                String key = arrayToString(lastNode.getCurrentWord());
                builder.append(map.get(key));
                lastIdx = i - 1;
            }
            else
            {
                // No match
                if (matchIdx != -1)
                {
                    // Backtrack to the last match start and don't consider this match
                    i = matchIdx - 1;
                    matchIdx = -1;
                    badMatches.add(lastNode);
                    lastNode = trie.getRoot();
                    continue;
                }
                builder.append(i < templateLength ? template.substring(lastIdx + 1, i + 1) : template.substring(lastIdx + 1));
                lastIdx = i;
            }
            badMatches.clear();
            matchIdx = -1;
            lastNode = trie.getRoot();
        }
        return builder.toString();
    }

    private static String arrayToString(List<Character> a) {
        StringBuilder b = new StringBuilder();
        for (Character c : a) {
            b.append(c);
        }
        return b.toString();
    }

    public static class Trie<T>
    {
        private Node root;

        public Node getRoot() {
            return root;
        }

        public void setRoot(Node root) {
            this.root = root;
        }

        public Trie(List<String> elems) {
            root = new Node(new ArrayList<>());
            for (String elem : elems) {
                insert(elem);
            }
        }

        private void insert(String word)
        {
            Node lastNode = root;

            for (Character chr : word.toCharArray()) {
                Node node = (Node) lastNode.getChildren().get(chr);

                if (!lastNode.getChildren().containsKey(chr))
                {
                    List<Character> currentWord = new ArrayList<>(lastNode.getCurrentWord());
                    currentWord.add(chr);
                    node = new Node(currentWord);
                    lastNode.getChildren().put(chr, node);
                }
                lastNode = node;
            }
            lastNode.setLeaf(true);
        }

        public boolean search(String word) {
            Node lastNode = root;
            for (int i = 0; i < word.length(); i++) {
                char c = word.charAt(i);
                lastNode = (Node) lastNode.getChildren().get(c);
                if(lastNode == null) {
                    return false;
                }
            }
            if(lastNode == root) {
                return false;
            } else {
                return lastNode.isLeaf();
            }
        }
    }

    public static class Node<Character> {
        private List<Character> currentWord;
        private boolean isLeaf;
        private HashMap<Character, Node> children;
        public Node(List<Character> currentWord) {
            this.currentWord = currentWord;
            this.children = new HashMap<>();
        }

        public boolean hasNext(Character elem) {
            return children.containsKey(elem);
        }

        public List<Character> getCurrentWord() {
            return currentWord;
        }

        public void setCurrentWord(List<Character> currentWord) {
            this.currentWord = currentWord;
        }

        public boolean isLeaf() {
            return isLeaf;
        }

        public void setLeaf(boolean leaf) {
            isLeaf = leaf;
        }

        public HashMap<Character, Node> getChildren() {
            return children;
        }

        public void setChildren(HashMap<Character, Node> children) {
            this.children = children;
        }
    }
}
