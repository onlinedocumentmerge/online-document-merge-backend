/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.odm.utils;

import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author ADMIN
 */
public class CronExxpressionMaker {

    private final Date mDate;
    private final Calendar mCal;
    
    private final String mDaysOfWeek = "?";

private String mMins;
    private String mHours;
    private String mDaysOfMonth;
    private String mMonths;
    private String mYears;
    private String mSeconds;

    public CronExxpressionMaker(Date pDate) {
        this.mDate = pDate;
        mCal = Calendar.getInstance();
        this.generateCronExpression();
    }

    private void generateCronExpression() {
        mCal.setTime(mDate);

        String hours = String.valueOf(mCal.get(Calendar.HOUR_OF_DAY));
        this.mHours = hours;

        String mins = String.valueOf(mCal.get(Calendar.MINUTE));
        this.mMins = mins;

        String days = String.valueOf(mCal.get(Calendar.DAY_OF_MONTH));
        this.mDaysOfMonth = days;

        String months = new java.text.SimpleDateFormat("MM").format(mCal.getTime());
        this.mMonths = months;

        String years = String.valueOf(mCal.get(Calendar.YEAR));
        this.mYears = years;
        String seconds = String.valueOf(mCal.get(Calendar.SECOND));
        this.mSeconds = seconds;

    }

    public Date getDate() {
        return mDate;
    }

    public String getSeconds() {
        return mSeconds;
    }

    public String getMins() {
        return mMins;
    }

    public String getDaysOfWeek() {
        return mDaysOfWeek;
    }

    public String getHours() {
        return mHours;
    }

    public String getDaysOfMonth() {
        return mDaysOfMonth;
    }

    public String getMonths() {
        return mMonths;
    }

    public String getYears() {
        return mYears;
    }

//    public String CronMaker(){
//        LocalDateTime dateTime = LocalDateTime.now();
//        //CronExpression cronExpression = new CronExpression();
//        return toCron(
//                String.valueOf(dateTime.getSecond()),
//                String.valueOf(dateTime.getMinute()),
//                String.valueOf(dateTime.getHour()),
//                String.valueOf(dateTime.getDayOfMonth()),
//                String.valueOf(dateTime.getMonthValue()),
//                String.valueOf(dateTime.getYear()));        
//    }
//
//
//    public static String toCron(final String second,final String mins, final String hrs, final String dayOfMonth, final String month, final String year) {
//        return String.format("%s %s %s %s %s ? %s",second, mins, hrs, dayOfMonth, month, year);
//    }
}
