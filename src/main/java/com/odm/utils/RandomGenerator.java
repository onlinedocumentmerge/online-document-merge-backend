/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.odm.utils;

/**
 *
 * @author ADMIN
 */
public class RandomGenerator {
    public static String getAlphaNumericString(int length,boolean isUpper,boolean isLower,boolean isNumber)
    { 
        String upperCase = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        String lowerCase = "abcdefghijklmnopqrstuvxyz";
        String number = "0123456789";
        String AlphaNumericString = ""; 
        if(isUpper){
            AlphaNumericString+=upperCase;
        }
        if(isLower){
            AlphaNumericString+=lowerCase;
        }
        if(isNumber){
            AlphaNumericString+=number;
        }
        StringBuilder sb = new StringBuilder(length);
        for (int i = 0; i < length; i++) {
            int index 
                = (int)(AlphaNumericString.length() 
                        * Math.random()); 
            sb.append(AlphaNumericString 
                          .charAt(index)); 
        } 
        return sb.toString(); 
    } 
    
}
