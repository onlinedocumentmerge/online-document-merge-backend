/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.odm.serviceImpl;

import com.odm.entity.Datasource;
import com.odm.repository.JobRepository;
import com.odm.service.IDataSourceService;
import com.odm.repository.AccountRepository;
import com.odm.repository.DataSourceRepository;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author 84918
 */
@Service
public class DataSourceServiceImpl implements IDataSourceService {

    @Autowired
    JobRepository odMJobRepository;

    @Autowired
    AccountRepository accountRepository;
    
    @Autowired
    DataSourceRepository dataSourceRepository;

    @Override
    //public MetaData createDataSource(String connectionString, int accountId, Job job) {
    public Datasource create(String connectionString, int accountId, String statement, int sqlserverType) {
//        Job dto = odMJobRepository.findMetadataByName(job.getName());

        Date date = new Date();
        Datasource datasource = new Datasource();
        datasource.setAccountByCreatedBy(accountRepository.findById(accountId).get());
        datasource.setCreateDate(date);
        datasource.setConnection(connectionString);
        datasource.setStatement(statement);
        datasource.setSqlserverType(sqlserverType);
        return dataSourceRepository.save(datasource);
    }

    @Override
    public Datasource update(Datasource updateDatasource) {
        return dataSourceRepository.save(updateDatasource);
    }

    @Override
    public List<Datasource> getByAccountID(int accountId) {
        return dataSourceRepository.getByAccountID(accountId);
    }

    @Override
    public Datasource getByID (int id) {
        return dataSourceRepository.findById(id).get();
    }

    @Override
    public void deleteById(int id) {
        dataSourceRepository.deleteById(id);
    }
}
