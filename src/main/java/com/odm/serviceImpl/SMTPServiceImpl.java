package com.odm.serviceImpl;

import com.odm.entity.Job;
import com.odm.entity.SMTP;
import com.odm.repository.SMTPRepository;
import com.odm.service.ISMTPService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class SMTPServiceImpl implements ISMTPService {
    @Autowired
    SMTPRepository SMTPRepository;
    @Autowired
    JobServiceImpl jobService;

    @Override
    public void insert(SMTP SMTP) {
        SMTPRepository.save(SMTP);
    }

    @Override
    public void update(SMTP SMTP) {
        SMTPRepository.save(SMTP);
    }

    @Override
    @Transactional(Transactional.TxType.REQUIRED)
    public void delete(int id) {
        SMTP smtp = SMTPRepository.findById(id).get();
        List<Job> jobs = (List<Job>) smtp.getJobs();
        for (Job job : jobs) {
            job.setSmtp(null);
            jobService.update(job);
        }
        SMTPRepository.deleteById(id);
    }

    @Override
    public SMTP getByID(int id) {
        return SMTPRepository.findById(id).get();
    }
}
