package com.odm.serviceImpl;

import com.google.api.core.ApiFuture;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.firestore.DocumentReference;
import com.google.cloud.firestore.Firestore;
import com.google.cloud.firestore.WriteResult;
import com.google.cloud.storage.*;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.cloud.FirestoreClient;
import com.google.firebase.cloud.StorageClient;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingException;
import com.google.firebase.messaging.Message;
import com.odm.OnlineDocumentMergeApplication;
import com.odm.model.JobProcess;
import com.odm.service.IFirebaseService;
import org.springframework.stereotype.Service;

import java.io.*;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

@Service
public class FirebaseService implements IFirebaseService {
    private Firestore db;
    private Bucket bucket;

    public FirebaseService() throws IOException {
        initDB();
    }

    @Override
    public void addData(JobProcess jobProcess) throws ExecutionException, InterruptedException {
        DocumentReference docRef = db.collection("accounts").document(jobProcess.getAccountID() + "").collection("jobs").document(jobProcess.getId() + "");
// Add document data  with id "alovelace" using a hashmap
//        Map<String, Object> data = new HashMap<>();
//        data.put("first", "Ada");
//        data.put("last", "Lovelace");
//        data.put("born", 1815);
//asynchronously write data
        ApiFuture<WriteResult> result = docRef.set(jobProcess);
// ...
// result.get() blocks on response
        System.out.println("Update time : " + result.get().getUpdateTime());
    }

    @Override
    public void updateAmount(int accountID, int jobID, int amount) throws ExecutionException, InterruptedException {
        DocumentReference docRef = db.collection("accounts").document(accountID + "").collection("jobs").document(jobID + "");
        Map<String, Object> updates = new HashMap<>();
        updates.put("amount", amount);

// Async update document
        ApiFuture<WriteResult> writeResult = docRef.update(updates);
        System.out.println("Update time : " + writeResult.get().getUpdateTime());
    }

    @Override
    public void updateData(int accountID, int jobID, Map<String, Object> data) throws ExecutionException, InterruptedException {
        DocumentReference docRef = db.collection("accounts").document(accountID + "").collection("jobs").document(jobID + "");
        ApiFuture<WriteResult> writeResult = docRef.update(data);
        System.out.println("Update time : " + writeResult.get().getUpdateTime());
    }

    @Override
    public void updateJobStatus(int accountID, int jobID, int status) throws ExecutionException, InterruptedException {
        DocumentReference docRef = db.collection("accounts").document(accountID + "").collection("jobs").document(jobID + "");
        Map<String, Object> updates = new HashMap<>();
        updates.put("status", status);

        ApiFuture<WriteResult> writeResult = docRef.update(updates);
        System.out.println("Update time : " + writeResult.get().getUpdateTime());
    }

    @Override
    public void updateArithmetic(boolean status) throws ExecutionException, InterruptedException {
        DocumentReference docRef = db.collection("arithmetic").document("1");
        Map<String, Object> updates = new HashMap<>();
        updates.put("status", status);

        // Async update document
        ApiFuture<WriteResult> writeResult = docRef.update(updates);
        System.out.println("Update time : " + writeResult.get().getUpdateTime());
    }

    @Override
    public void uploadFile(String fileName, File file) throws IOException {
            Storage storage = bucket.getStorage();
            BlobInfo blobInfo = BlobInfo.newBuilder(BlobId.of(bucket.getName(), fileName)).setContentType("application/x-zip-compressed").build();
            Blob blob = storage.create(blobInfo, Files.readAllBytes(Paths.get(file.getPath())));
            URL url = blob.signUrl(365, TimeUnit.DAYS);
            System.out.println(url.toString());
    }

    public String uploadFilePath(String fileName, String filePath) throws IOException {
        File file = new File(filePath);
        System.out.println(file.getPath());
        Storage storage = bucket.getStorage();
        BlobInfo blobInfo = BlobInfo.newBuilder(BlobId.of(bucket.getName(), fileName)).setContentType("application/x-zip-compressed").build();
        Blob blob = storage.create(blobInfo, Files.readAllBytes(Paths.get(file.getPath())));
        URL url = blob.signUrl(365, TimeUnit.DAYS);
        return url.toString();
    }

    @Override
    public void updateDataCounting(int jobID, int accountID, int counting) throws ExecutionException, InterruptedException {
        DocumentReference docRef = db.collection("accounts").document(accountID + "").collection("jobs").document(jobID + "");
        Map<String, Object> updates = new HashMap<>();
        updates.put("counting", counting);

        ApiFuture<WriteResult> writeResult = docRef.update(updates);
        System.out.println("Update time : " + writeResult.get().getUpdateTime());
    }

    @Override
    public String uploadImage(String fileName, byte[] image) {
        Storage storage = bucket.getStorage();
        System.out.println(bucket.getName());
        BlobInfo blobInfo = BlobInfo.newBuilder(BlobId.of(bucket.getName(), fileName)).build();
        Blob blob = storage.create(blobInfo, image);
        URL url = blob.signUrl(365, TimeUnit.DAYS);
        return url.toString();
    }

    public void updateDownloadPDFLink(int jobID, int accountID, String link, String linkName) throws ExecutionException, InterruptedException {
        DocumentReference docRef = db.collection("accounts").document(accountID + "").collection("jobs").document(jobID + "");
        Map<String, Object> updates = new HashMap<>();
        updates.put(linkName, link);

        ApiFuture<WriteResult> writeResult = docRef.update(updates);
        System.out.println("Update time : " + writeResult.get().getUpdateTime());
    }
    public String pushNotification(String fileName, File file) throws FirebaseMessagingException {
        Message message = Message.builder()
                .putData("content", "Test Content")
                .setToken("token")
                .build();

        String response = FirebaseMessaging.getInstance().send(message);
        return response;
    }

    @Override
    public String getFirebaseToken(int id) throws FirebaseAuthException {
        String customToken = FirebaseAuth.getInstance().createCustomToken(String.valueOf(id));
        return customToken;
    }

    private void initDB() throws IOException {
        InputStream serviceAccount = OnlineDocumentMergeApplication.class.getResourceAsStream(("/firebasesdk.json"));
        GoogleCredentials credentials = GoogleCredentials.fromStream(serviceAccount);

        FirebaseOptions options = new FirebaseOptions.Builder()
                .setCredentials(credentials)
                .setServiceAccountId("firebase-adminsdk-6wed5@onlinedocumentmerge.iam.gserviceaccount.com")
                .setDatabaseUrl("https://onlinedocumentmerge.firebaseio.com")
                .setStorageBucket("onlinedocumentmerge.appspot.com")
                .build();
        if(FirebaseApp.getApps().isEmpty()) {
            FirebaseApp.initializeApp(options);
        }


        this.db = FirestoreClient.getFirestore();
        this.bucket = StorageClient.getInstance().bucket();

    }
}
