package com.odm.serviceImpl;

import com.odm.entity.SpecificLinkParameter;
import com.odm.repository.SpecificLinkParameterRepository;
import com.odm.service.ISpecificLinkParameterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SpecificLinkParameterServiceImpl implements ISpecificLinkParameterService {
    @Autowired
    SpecificLinkParameterRepository specificLinkParameterRepository;

    @Override
    public SpecificLinkParameter insert(SpecificLinkParameter specificLinkParameter) {
        return specificLinkParameterRepository.save(specificLinkParameter);
    }

    @Override
    public SpecificLinkParameter update(SpecificLinkParameter specificLinkParameter) {
        return specificLinkParameterRepository.save(specificLinkParameter);
    }

    @Override
    public void delete(SpecificLinkParameter specificLinkParameter) {
        specificLinkParameterRepository.delete(specificLinkParameter);
    }
}
