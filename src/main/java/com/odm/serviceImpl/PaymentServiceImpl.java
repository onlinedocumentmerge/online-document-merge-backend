package com.odm.serviceImpl;

import com.odm.entity.Account;
import com.odm.entity.Job;
import com.odm.entity.PaymentODM;
import com.odm.repository.AccountRepository;
import com.odm.repository.PaymentRepository;
import com.odm.service.IPaymentService;
import com.odm.utils.CONST;
import com.odm.utils.PayPalClient;
import com.paypal.core.PayPalHttpClient;
import com.paypal.orders.Order;
import com.paypal.orders.OrdersGetRequest;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
public class PaymentServiceImpl implements IPaymentService {
    @Autowired
    PaymentRepository paymentRepository;
    @Autowired
    AccountRepository accountRepository;
    @Autowired
    JobServiceImpl jobService;
    @Autowired
    PayPalClient payPalClient;

    @Transactional
    @Override
    public void deposit(int id, BigDecimal amount){
        Account account = accountRepository.findById(id).get();
        account.setBalance(account.getBalance().add(amount));
        PaymentODM paymentODM = new PaymentODM();
        paymentODM.setStatus(CONST.PAYMENT_STATUS_COMPLETED);
        paymentODM.setAmount(amount);
        paymentODM.setAccount(account);
        paymentODM.setCreatedDay(new Date());
        paymentODM.setType(CONST.PAYMENT_DEPOSIT);
        paymentRepository.save(paymentODM);
        accountRepository.save(account);
    }

    @Override
    public List<PaymentODM> getPaymentHistory(int id) {
        return paymentRepository.getPaymentHistory(id);
    }

    @Transactional
    @Override
    public void payForJob(Job job) throws Exception {
        Account account = job.getAccount();
        if(account.getBalance().compareTo(job.getPrice()) < 0) {
            throw new Exception("Not enough money");
        }
        account.setBalance(account.getBalance().subtract(job.getPrice()));
        accountRepository.save(account);
        PaymentODM paymentODM = new PaymentODM();
        paymentODM.setAmount(job.getPrice());
        paymentODM.setAccount(account);
        paymentODM.setType(CONST.PAYMENT_PAID);
        paymentODM.setCreatedDay(new Date());
        paymentODM.setStatus(CONST.PAYMENT_STATUS_COMPLETED);
        paymentODM.setDescription("Paid for job " + job.getName());
        paymentRepository.save(paymentODM);
        jobService.update(job);
    }

    @Override
    public void getOrder(String orderId) throws IOException {
        //OrdersGetRequest request = new OrdersGetRequest(orderId);
        //request.header("prefer","return=representation");
        //PayPalHttpClient client = payPalClient.client();
        //HttpResponse<Order> response = client.execute(request);
        //.out.println("Full response body:");
       // System.out.println(new JSONObject(new Json().serialize(response.result())).toString(4));
    }
}
