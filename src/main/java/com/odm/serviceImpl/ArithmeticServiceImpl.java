package com.odm.serviceImpl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.odm.entity.Arithmetic;
import com.odm.model.ArithmeticModel;
import com.odm.model.ArithmeticRequest;
import com.odm.model.JobModel;
import com.odm.repository.ArithmeticRepository;
import com.odm.service.IArithmeticService;
import com.odm.service.IFirebaseService;
import com.odm.utils.MathUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.jms.BytesMessage;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.lang.reflect.InvocationTargetException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class ArithmeticServiceImpl implements IArithmeticService {
    private static final String sendArithmetic = "arithmetic.queue";
    @Autowired
    private JmsTemplate jmsTemplate;
    @Autowired
    IFirebaseService firebaseService;
    @Autowired
    ArithmeticRepository arithmeticRepository;

    @Override
    public List<ArithmeticRequest> loadArithmetic() throws InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
//        MathUtils mathUtils = new MathUtils();
//        return mathUtils.loadArithmetic();
        List<Arithmetic> arithmetics = arithmeticRepository.getByActive();
        List<ArithmeticRequest> arithmeticRequests = new ArrayList<>();
        for(int i = 0; i < arithmetics.size(); i++) {
            Arithmetic arithmetic = arithmetics.get(i);
            ArithmeticRequest arithmeticRequest = new ArithmeticRequest();
            arithmeticRequest.setId(arithmetic.getId());
            arithmeticRequest.setOperator(arithmetic.getName());
            arithmeticRequest.setMinParam(arithmetic.getMinParameter());
            arithmeticRequest.setMaxParam(arithmetic.getMaxParameter());
            arithmeticRequest.setDescription(arithmetic.getDescription());
            arithmeticRequest.setActive(arithmetic.isActive());
            arithmeticRequests.add(arithmeticRequest);
        }
        return arithmeticRequests;
    }

    @Override
    public List<ArithmeticRequest> loadArithmeticWithoutStatus() throws InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
//        MathUtils mathUtils = new MathUtils();
//        return mathUtils.loadArithmetic();
        List<Arithmetic> arithmetics = arithmeticRepository.getAll();
        List<ArithmeticRequest> arithmeticRequests = new ArrayList<>();
        for(int i = 0; i < arithmetics.size(); i++) {
            Arithmetic arithmetic = arithmetics.get(i);
            ArithmeticRequest arithmeticRequest = new ArithmeticRequest();
            arithmeticRequest.setId(arithmetic.getId());
            arithmeticRequest.setOperator(arithmetic.getName());
            arithmeticRequest.setMinParam(arithmetic.getMinParameter());
            arithmeticRequest.setMaxParam(arithmetic.getMaxParameter());
            arithmeticRequest.setDescription(arithmetic.getDescription());
            arithmeticRequest.setActive(arithmetic.isActive());
            arithmeticRequests.add(arithmeticRequest);
        }
        return arithmeticRequests;
    }

    @Override
    public void updateStatus(int arithmeticID, boolean status) {
        Arithmetic arithmetic = arithmeticRepository.findById(arithmeticID).get();
        arithmetic.setActive(status);
        arithmeticRepository.save(arithmetic);
    }

    @Override
    public void saveArithmeticFile(MultipartFile file) throws Exception {
        // Normalize file name
        String fileName = StringUtils.cleanPath(file.getOriginalFilename());
        if(fileName.indexOf(".") < 0) {
            throw new Exception(("Invalid File"));
        }
        //Tao duong dan rac
        String fileNameWithoutExtension = fileName.substring(0, fileName.lastIndexOf("."));
            Path fileStorageLocation = Paths.get(getTempArithmeticFolderPath()).toAbsolutePath().normalize();
            // Check if the file's name contains invalid characters
            if(fileName.contains("..")) {
                throw new Exception("Sorry! Filename contains invalid path sequence " + fileName);
            }
            // Copy file to the target location (Replacing existing file with the same name)
            String newFileName = fileName;
            Path targetLocation = fileStorageLocation.resolve(newFileName);
            File tempFile = new File(targetLocation.toString());
            boolean copySuccess = true;
            while (copySuccess) {
                try {
                    Files.copy(file.getInputStream(), tempFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
                    copySuccess = false;
                } catch (Exception e) {
                    newFileName = fileName + "1";
                    targetLocation = fileStorageLocation.resolve(newFileName);
                    tempFile = new File(targetLocation.toString());
                }
            }

            MathUtils mathUtils = new MathUtils();
            odm.arithmetic.iarithmetic.IArithmeticService instance = mathUtils.castClassToInterface(tempFile);
            if(fileNameWithoutExtension.equals(instance.getOperator())) {
                fileStorageLocation = Paths.get(getArithmeticFolderPath()).toAbsolutePath().normalize();
                targetLocation = fileStorageLocation.resolve(fileName);
                File f = new File(targetLocation.toString());
                Files.copy(file.getInputStream(), f.toPath(), StandardCopyOption.REPLACE_EXISTING);

                Arithmetic arithmetic = arithmeticRepository.getByName(instance.getOperator());
                if(arithmetic == null) {
                    arithmetic = new Arithmetic();
                }
                arithmetic.setName(instance.getOperator());
                arithmetic.setMinParameter(instance.getMinParameter());
                arithmetic.setMaxParameter(instance.getMaxParameter());
                arithmetic.setActive(false);
                arithmetic.setDescription(instance.description());
                arithmeticRepository.save(arithmetic);
                firebaseService.updateArithmetic(false);
                sendArithmeticToWorker(f);
            } else {
                throw new Exception("File name is not same");
            }
    }

    private String getTempArithmeticFolderPath() {
        File dir = new File(System.getProperty("user.dir"));
        dir = new File(dir, "src");
        dir = new File(dir, "main");
        dir = new File(dir, "java");
        dir = new File(dir, "com");
        dir = new File(dir, "odm");
        dir = new File(dir, "arithmetic");
        dir = new File(dir, "temp");
        if (dir.isDirectory() == false) {
            dir.mkdir();
        }
        String path = dir.toString() + File.separator;
        return path;
    }

    private String getArithmeticFolderPath() {
        File dir = new File(System.getProperty("user.dir"));
        dir = new File(dir, "src");
        dir = new File(dir, "main");
        dir = new File(dir, "java");
        dir = new File(dir, "com");
        dir = new File(dir, "odm");
        dir = new File(dir, "arithmetic");
        String path = dir.toString() + File.separator;
        return path;
    }

    @Override
    public void sendArithmeticToWorker(File arithmeticFile) throws Exception{
        MessageCreator messageCreator = session -> {
            try {
                BytesMessage bytesMessage = session.createBytesMessage();
                bytesMessage.setStringProperty("fileName", arithmeticFile.getName());
                bytesMessage.writeBytes(readfileAsBytes(arithmeticFile));
                return bytesMessage;
            } catch (IOException ex) {
                //log.info(ex.getMessage());
                return null;
            }
        };
        if (messageCreator != null) {
            jmsTemplate.send(sendArithmetic, messageCreator);
        } else {
            throw new Exception("Server is Down, please contact administrator");
        }
    }

    @Override
    public byte[] readfileAsBytes(File file) throws IOException {
        try (RandomAccessFile accessFile = new RandomAccessFile(file, "r")) {
            byte[] bytes = new byte[(int) accessFile.length()];
            accessFile.readFully(bytes);
            return bytes;
        }
    }
}
