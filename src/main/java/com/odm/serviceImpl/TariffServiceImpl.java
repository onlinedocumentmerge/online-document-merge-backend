package com.odm.serviceImpl;

import com.odm.entity.Tariff;
import com.odm.repository.TariffRepository;
import com.odm.service.ITariffService;
import org.apache.tools.ant.taskdefs.Tar;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TariffServiceImpl implements ITariffService {
    @Autowired
    TariffRepository tariffRepository;

    @Override
    public List<Tariff> getAll() throws Exception {
        return tariffRepository.getAll();
    }

    @Override
    public Tariff getById(int id) throws Exception {
        return tariffRepository.findById(id).get();
    }

    @Override
    public void addNew(Tariff tariff) throws Exception {
        Tariff existPlan = tariffRepository.findByName(tariff.getName());
        if(existPlan != null){
            throw new Exception("Duplicated Plan.");
        }else{
            tariffRepository.save(tariff);
        }
    }
    @Override
    public void update(Tariff tariff) throws Exception {
        tariffRepository.save(tariff);
    }
    @Override
    public void delete(int planId) throws Exception {

        tariffRepository.deleteById(planId);
    }
    @Override
    public float getPriceByName(String name) throws Exception {
        return tariffRepository.findByName(name).getPrice();
    }
    @Override
    public int getQuantityByName(String name) throws Exception {
        return tariffRepository.findByName(name).getQuantity();
    }
}
