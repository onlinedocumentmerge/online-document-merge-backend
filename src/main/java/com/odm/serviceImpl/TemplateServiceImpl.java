package com.odm.serviceImpl;

import com.odm.service.ITemplateService;
import com.odm.entity.Account;
import com.odm.entity.Template;
import com.odm.repository.AccountRepository;
import com.odm.repository.TemplateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Date;
import java.util.List;

@Service
public class TemplateServiceImpl implements ITemplateService {

    @Autowired
    TemplateRepository templateRepository;
    @Autowired
    AccountRepository accountRepository;

    @Override
    public Template createTemplate(String templateName, String templateContent, int accountID) {
        Account account = accountRepository.findById(accountID).get();
        Template template = new Template();
        template.setAccount(account);
        template.setName(templateName);
        template.setTemplateContent(templateContent);
        template.setCreateDate(new Date());
        return templateRepository.save(template);
    }

    @Override
    public List<Template> getByAccountID(int accountID) {
        return templateRepository.getByAccountID(accountID);
    }

    @Override
    public Template update(Template template) {
        return templateRepository.save(template);
    }

    @Override
    public Template findById(int templateID) {
        return templateRepository.findById(templateID).get();
    }

    public void deleteById(int templateID) {
        templateRepository.deleteById(templateID);
    }

    private static byte[] readFileToByteArray(File file) {
        FileInputStream fis = null;
        // Creating a byte array using the length of the file
        // file.length returns long which is cast to int
        byte[] bArray = new byte[(int) file.length()];
        try {
            fis = new FileInputStream(file);
            fis.read(bArray);
            fis.close();

        } catch (IOException ioExp) {
            ioExp.printStackTrace();
        }
        return bArray;
    }
}
