package com.odm.serviceImpl;

import com.odm.entity.MailHeader;
import com.odm.repository.MailFieldRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MailFieldServiceImpl {
    @Autowired
    MailFieldRepository mailFieldRepository;
    public MailHeader insert(MailHeader mailHeader) {
        return mailFieldRepository.save(mailHeader);
    }
    public MailHeader update(MailHeader mailHeader) {
        return mailFieldRepository.save(mailHeader);
    }
    public void delete(MailHeader mailHeader) {
        mailFieldRepository.delete(mailHeader);
    }
}
