package com.odm.serviceImpl;

import com.odm.entity.Account;
import com.odm.entity.Job;
import com.odm.entity.PaymentODM;
import com.odm.repository.AccountRepository;
import com.odm.repository.JobRepository;
import com.odm.repository.PaymentRepository;
import com.odm.service.IStatisticService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class StatisticServiceImpl implements IStatisticService {
    @Autowired
    AccountRepository accountRepository;
    @Autowired
    JobRepository jobRepository;
    @Autowired
    PaymentRepository paymentRepository;

    @Override
    public List<List> getStatistic(Date from, Date to) {
        List<Job> jobs = jobRepository.statistic(from, to);
        List<Account> accounts = accountRepository.statistic(from, to);
        List<PaymentODM> paymentODMS = paymentRepository.statistic(from, to);
        List<List> result = new ArrayList<>();
        result.add(jobs);
        result.add(accounts);
        result.add(paymentODMS);
        return result;
    }
}
