/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.odm.serviceImpl;

import com.odm.model.Field;
import com.odm.model.PrepairTemplate;
import com.odm.repository.AccountRepository;
import com.odm.repository.DataSourceRepository;
import com.odm.repository.TemplateRepository;
import com.odm.service.IDocumentService;
import java.util.*;
import com.odm.utils.*;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author 84918
 */
@Service
public class DocumentServiceImpl implements IDocumentService {
    @Autowired
    TemplateRepository templateRepository;
    @Autowired
    AccountRepository accountRepository;
    @Autowired
    DataSourceRepository dataSourceRepository;
    @Autowired
    FirebaseService firebaseService;
    private DatabaseUtils databaseUtils = new DatabaseUtils(CONST.TYPE_CONNECT_SQLSERVER,
            CONST.CONNECTION_STRING);


    public static final String OPENTAG = "OPENTAG";
    public static final String CLOSETAG = "CLOSETAG";
    public static final String CONTENT = "content";


    @Override
    public PrepairTemplate doPrepareTemplate(String template, HashMap<String, String> fieldToken) {
        List<String> words = new ArrayList<>();
        for (String key : fieldToken.keySet()) {
            words.add(key);
        }
        TrieUtil.Trie<Character> trie = new TrieUtil.Trie<Character>(words);
        TrieUtil.Node lastNode = trie.getRoot();
        char open = '«';
        char close = '»';
        String stage = CONTENT;
        StringBuilder result = new StringBuilder();
        StringBuilder expression = new StringBuilder();
        StringBuilder normal = new StringBuilder();
        boolean isOpen = false;
        boolean isExpressionOpen = false;
        int normalIndex = -1;
        int expressionIndex = -1;
        int expressionLevel = 0;
        PrepairTemplate prepairTemplate = new PrepairTemplate();
        for (int i = 0; i < template.length(); i++) {
            char c = template.charAt(i);
            switch (stage) {
                case CONTENT:
                    if(c == open) {
                        stage = OPENTAG;
                        isOpen = true;
                        normalIndex = result.length();
                        if(isExpressionOpen) {
                            expressionLevel++;
                        }
                    } else if(c == close) {
                        if(isExpressionOpen) {
                            expressionLevel--;
                            Field field = prepairTemplate.getPositionField().get(expressionIndex);
                            if(field != null && expressionLevel == -1) {
                                field.setName(expression.toString());
                                isExpressionOpen = false;
                                expression.setLength(0);
                                expressionIndex = -1;
                                expressionLevel = 0;
                            }
                        }
                    } else {
                        if(isExpressionOpen) {
                            expression.append(c);
                        } else {
                            result.append(c);
                        }
                    }
                    break;
                case OPENTAG:
                    if(c == open) {
                        if(isOpen) {
                            if(expressionLevel == 0) {
                                expressionIndex = result.length();
                            }
                            expression.append(normal);
                            normal.setLength(0);
                            normalIndex = -1;
                            isExpressionOpen = true;
                            expressionLevel++;
                        }
                    } else if (c == close) {
                        stage = CLOSETAG;
                    } else {
                        normal.append(c);
                    }
                    break;
                case CLOSETAG:
                    if(trie.search(normal.toString())) {
                        if(isExpressionOpen) {
                            Field field = prepairTemplate.getPositionField().get(expressionIndex);
                            if (field == null) {
                                field = new Field();
                                field.setExpression(true);
                                prepairTemplate.getPositionField().put(expressionIndex, field);
                            }

                            LinkedHashMap<Integer, String> expressionField = prepairTemplate.getPositionExpression().get(expressionIndex);
                            if(expressionField == null) {
                                expressionField = new LinkedHashMap<>();
                            }
                            expressionField.put(expression.length(), normal.toString());
                            normal.setLength(0);
                            normalIndex = -1;
                            prepairTemplate.getPositionExpression().put(expressionIndex, expressionField);
                            isOpen = false;
                            expressionLevel--;
                            if(c == open) {
                                stage = OPENTAG;
                            } else if(c == close) {
                                if(field != null && expressionLevel == -1) {
                                    field.setName(expression.toString());
                                    expression.setLength(0);
                                    expressionIndex = -1;
                                    isExpressionOpen = false;
                                    expressionLevel = 0;
                                }
                                stage = CONTENT;
                            } else {
                                stage = CONTENT;
                                expression.append(c);
                            }
                        } else if(isOpen) {
                            Field field = prepairTemplate.getPositionField().get(expressionIndex);
                            if (field == null) {
                                field = new Field();
                                field.setName(normal.toString());
                                field.setExpression(false);
                            }
                            prepairTemplate.getPositionField().put(normalIndex, field);
                            normal.setLength(0);
                            normalIndex = -1;
                            if(c == open) {
                                stage = OPENTAG;
                            } else {
                                stage = CONTENT;
                                result.append(c);
                            }
                        }
                    }
                    break;
            }
        }
        prepairTemplate.setContent(result);
        return prepairTemplate;
    }

    @Override
    public String insertPosition(PrepairTemplate prepairTemplate, String row) {
        LinkedHashMap<Integer, Field> positionField = prepairTemplate.getPositionField();
        List<Integer> keys = new ArrayList<>(positionField.keySet());
        Collections.reverse(keys);
        MathUtils mathUtils = new MathUtils();
        StringBuilder result = new StringBuilder(prepairTemplate.getContent());
        JSONObject node = new JSONObject(row);
        for (Integer key : keys) {
            Field field = positionField.get(key);
            if (field.isExpression()) {
                LinkedHashMap<Integer, String> expressionParams = prepairTemplate.getPositionExpression().get(key);
                StringBuilder expression = new StringBuilder(field.getName());
                List<Integer> paramPosition = new ArrayList<>(expressionParams.keySet());
                Collections.reverse(paramPosition);
                for (Integer pos : paramPosition) {
                    expression.insert(pos, node.get(expressionParams.get(pos)));
                }
                String expressionResult = mathUtils.calculate(expression.toString().trim()).toString();
                result.insert(key, expressionResult);
            } else {
                result.insert(key, node.get(field.getName()));
            }
        }
        return result.toString();
    }
}
