package com.odm.serviceImpl;

import com.odm.entity.PaymentODM;
import com.odm.repository.PaymentRepository;
import com.odm.service.IAccountService;
import com.odm.utils.CONST;
import com.odm.utils.EmailSender;
import com.odm.entity.Account;
import com.odm.repository.AccountRepository;

import java.math.BigDecimal;
import java.util.List;

import com.odm.utils.RandomGenerator;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class AccountServiceImpl implements IAccountService {

    @Autowired
    AccountRepository accountRepository;
    @Autowired
    PaymentRepository paymentRepository;
    @Autowired
    EmailSender emailSender;
    @Autowired
    PasswordEncoder passwordEncoder;

    @Override
    public void insert(Account account) throws Exception {
        Account existedAccount = accountRepository.findByEmail(account.getEmail());
        if(existedAccount != null){
            throw new Exception("Duplicated Email.");
        } else {
            accountRepository.save(account);
            sendActiveCode(account.getEmail(), account.getFullname(), account.getActivationCode());
        }
    }

    @Override
    public Account getAccountById(int id) {
        return accountRepository.findById(id).get();
    }

    @Override
    public List<Account> GetAllAccount() {
        return (List<Account>) accountRepository.findAll();
    }

    @Override
    public void UpdateUserStatus(int accountID, boolean isActive) throws Exception {
        try {
            Account account = accountRepository.findById(accountID).get();
            account.setActive(isActive);
            accountRepository.save(account);
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }

    @Override
    public void ActivateAccount(String activationCode) throws Exception {
        Account account = accountRepository.findByActivationCode(activationCode);
        if(account == null) {
            throw new Exception("Wrong Active Code");
        } else {
            account.setConfirm(true);
            account.setActivationCode(null);
            accountRepository.save(account);
        }
    }

    @Override
    public void UpdateAccount(int id, Account accountReq){
        Account account = accountRepository.findById(id).get();
//            account.setEmail(accountReq.getEmail());
//            account.setPassword(accountReq.getPassword());
        if (!accountReq.getFullname().isEmpty()) {
            account.setFullname(accountReq.getFullname());
        }
        if (!accountReq.getPhone().isEmpty()) {
            account.setPhone(accountReq.getPhone());
        }
        accountRepository.save(account);
    }

    @Override
    public void forgotPassword(Account account, String email) throws Exception {
        String newPass = RandomGenerator.getAlphaNumericString(15, true, true, true);
        account.setPassword(passwordEncoder.encode(newPass));
        accountRepository.save(account);
        emailSender.SendEmail(null, null,email ,
                "Dear"+ account.getFullname()
                        +"\n Your password has been reset"
                        +"\n Your new Password is: " + newPass
                        +"\n Please Login with the new password and change your password at the Profile page",
                "Reset Your Password"
        );
    }

    @Override
    public void changePassword(int id, String oldPassword, String newPassword) throws Exception {
        Account account = accountRepository.findById(id).get();
        if(passwordEncoder.matches(oldPassword, account.getPassword())){
            account.setPassword(passwordEncoder.encode(newPassword));
            accountRepository.save(account);
        } else {
            throw new Exception("Old Password Is Wrong");
        }



//        if(account.getPassword().equals(oldPassword)){
//            account.setPassword(newPassword);
//            accountRepository.save(account);
//        } else {
//            throw new Exception("old Password is wrong!!!");
//        }
    }

    @Override
    public void sendActiveCode(String email, String fullName, String activeCode) throws Exception {
        emailSender.SendEmail(null, null, email, "Dear " + fullName +
                ", Your account "
                + "have been created, your active code is: \n"
                + activeCode, "Online Document Merge Active Code");
    }

    @Override
    public Account getAccountByEmail(String email) {
        return accountRepository.findByEmail(email);
    }
}
