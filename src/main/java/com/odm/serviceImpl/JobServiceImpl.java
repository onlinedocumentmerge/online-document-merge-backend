/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.odm.serviceImpl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.joda.JodaModule;
import com.odm.model.JobModel;
import com.odm.model.PrepairTemplate;
import com.odm.service.IJobService;
import com.odm.entity.Account;
import com.odm.entity.Job;
import com.odm.repository.AccountRepository;
import com.odm.repository.JobRepository;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.odm.utils.CronExxpressionMaker;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.stereotype.Service;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.ObjectMessage;
import javax.jms.Session;

/**
 *
 * @author ADMIN
 */
@Slf4j
@Service
public class JobServiceImpl implements IJobService {
    //Constant
    private static final String sendJob = "job.queue";
    private static final String PAUSE_JOB_QUEUE = "pause.job.queue";
    private static final String RESUME_JOB_QUEUE = "resume.job.queue";
    private static final String receiveJob = "Workerjob.queue";
    @Autowired
    private JmsTemplate jmsTemplate;
    @Autowired
    JobRepository jobRepository;
    @Autowired
    AccountRepository accountRepository;
    @Autowired
    TariffServiceImpl tariffService;

    @Override
    public Job createJob(Job job, int accountId) {
            //create job
            Account dto = accountRepository.findById(accountId).get();
//            job.setInQueue(false);
//            job.setIsDone(false);
            job.setAccount(dto);
            //job.setCreatedDate(new Date());

            //job.setJobGroup(account.getEmail());
            //check duplicate
//            String jobName =randomGenerator.getAlphaNumericString(6, false, true, true);
//            if(odmJobRepository.findByNameAndGroup(jobName, account.getEmail())!=null){
//                jobName = randomGenerator.getAlphaNumericString(6, false, true, true);
//            }
//            job.setName(jobName);
//            job.setStart(false);
//            job.setInQueue(false);
//            job.setDone(false);
//            job.setCronJob(false);
//            job.setRepeatTime(5L);
//            job.setAccount(account);
            //log.info("Jobgroup"+odmjob.getJobGroup());
            //sendJob(odmjob);
        return jobRepository.save(job);
    }

    @Override
    public List<Job> findByAcountID(int accountID) {
        return jobRepository.findByAccountID(accountID);
    }

    @Override
    public Job findBySpecificLink(String doamin, String url) {
        return jobRepository.findBySpecificLink(doamin, url);
    }

    @Override
    public Job findById(int id) {
        return jobRepository.findById(id).get();
    }

    @Override
    public void sendJob(JobModel jobModel) throws Exception{
        MessageCreator messageCreator = session -> {
            ObjectMapper mapper = new ObjectMapper();
            mapper.registerModule(new JodaModule());
            ObjectMessage objectMessage = session.createObjectMessage();
            try {
                String json = mapper.writeValueAsString(jobModel);
                //log.info(json);
                objectMessage.setObject(json);
                return objectMessage;
            } catch (JsonProcessingException ex) {
                //log.info(ex.getMessage());
                return null;
            }
        };
        if (messageCreator != null) {
            jmsTemplate.send(sendJob, messageCreator);
        } else {
            throw new Exception("Server is Down, please contact administrator");
        }
    }

    @Override
    public void pauseJob(JobModel jobModel) throws Exception {
        MessageCreator messageCreator = session -> {
            ObjectMapper mapper = new ObjectMapper();
            mapper.registerModule(new JodaModule());
            ObjectMessage objectMessage = session.createObjectMessage();
            try {
                String json = mapper.writeValueAsString(jobModel);
                //log.info(json);
                objectMessage.setObject(json);
                return objectMessage;
            } catch (JsonProcessingException ex) {
                //log.info(ex.getMessage());
                return null;
            }
        };
        if (messageCreator != null) {
            jmsTemplate.send(PAUSE_JOB_QUEUE, messageCreator);
        } else {
            throw new Exception("Server is Down, please contact administrator");
        }
    }

    @Override
    public void resumeJob(JobModel jobModel) throws Exception {
        MessageCreator messageCreator = session -> {
            ObjectMapper mapper = new ObjectMapper();
            mapper.registerModule(new JodaModule());
            ObjectMessage objectMessage = session.createObjectMessage();
            try {
                String json = mapper.writeValueAsString(jobModel);
                //log.info(json);
                objectMessage.setObject(json);
                return objectMessage;
            } catch (JsonProcessingException ex) {
                //log.info(ex.getMessage());
                return null;
            }
        };
        if (messageCreator != null) {
            jmsTemplate.send(RESUME_JOB_QUEUE, messageCreator);
        } else {
            throw new Exception("Server is Down, please contact administrator");
        }
    }

    @Override
    public boolean isExistedCustomURL(String domain, String url) {
        return findBySpecificLink(domain, url) != null;
    }

    @Override
    public void update(Job job) {
        jobRepository.save(job);
    }

    @Override
    public float computeJobPrice(int rows, int emailHeaders, boolean isSendingEmail, boolean isDownloadPDF, boolean isSpecificLink) throws Exception{
        float price = 0;

        float rowBasePrice = tariffService.getPriceByName("Document");
        int rowBaseUnitPrice = tariffService.getQuantityByName("Document");
        float rowMergePrice = (rowBasePrice / rowBaseUnitPrice) * rows;
        price += rowMergePrice;

        if(isSendingEmail) {
            float emailPrice = tariffService.getPriceByName("Email");
            int emailQuantity = tariffService.getQuantityByName("Email");
            float emailTotalPrice = (emailPrice / emailQuantity) * rows * emailHeaders;
            price += emailTotalPrice;
        }
        if(isDownloadPDF) {
            float pdfPrice = tariffService.getPriceByName("PDF");
            int pdfQuantity = tariffService.getQuantityByName("PDF");
            float pdfTotalPrice = (pdfPrice / pdfQuantity) * rows;
            price += pdfTotalPrice;
        }
        if(isSpecificLink) {
            price += 1;
//            float linkPrice = tariffService.getPriceByName("Specific Link");
//            int linkQuantity = tariffService.getQuantityByName("Specific Link");
//            float emailPrice = (linkPrice / linkQuantity) * linkDay;
        }
        return price;
    }

    public String generateCronExpression(Date date, boolean isEveryMonth) {
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String dt = dateFormat.format(date);
            Date cronDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(dt);
            CronExxpressionMaker calHelper = new CronExxpressionMaker(cronDate);
            String month = calHelper.getMonths();
            String year = calHelper.getYears();
            if(isEveryMonth){
                month = "*";
                year = "*";
            }
            String cron = calHelper.getSeconds() + " "
                    + calHelper.getMins() + " "
                    + calHelper.getHours() + " "
                    + calHelper.getDaysOfMonth() + " "
                    + month + " "
                    + calHelper.getDaysOfWeek() + " "
                    + year;
            return cron;
        } catch (Exception e) {
            return e.getMessage();
        }

    }
//    @JmsListener(destination = receiveJob)
//    public void receiveJobMessage(String jsonString) {
//        try {
//            Gson g = new Gson();
//            Job jobs = g.fromJson(jsonString, Job.class);
//
//            Job oldJobs = jobRepository.findByNameAndGroup(jobs.getName(),jobs.getJobGroup());
//            oldJobs.setInQueue(jobs.isInQueue());
//            oldJobs.setStart(jobs.isStart());
//            oldJobs.setJobClass(jobs.getJobClass());
//            jobRepository.save(oldJobs);
//            //log.info("New Job: " + jobs.getName() + " have been added to queue!");
//        } catch (Exception e) {
//            //log.error(e.getMessage());
//        }
//    }

}
