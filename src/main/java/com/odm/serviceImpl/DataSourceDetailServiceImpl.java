/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.odm.serviceImpl;

import com.odm.entity.Datasource;
import com.odm.entity.DatasourceDetail;
import com.odm.repository.DataSourceRepository;
import com.odm.service.IDataSourceDetailsService;
import com.odm.repository.DataSourceDetailRepository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.odm.utils.DatabaseUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author 84918
 */
@Service
public class DataSourceDetailServiceImpl implements IDataSourceDetailsService {

    @Autowired
    DataSourceDetailRepository detailRepository;
    @Autowired
    DataSourceRepository dataSourceRepository;

    @Override
    public void deleteById(int id) {
        detailRepository.deleteById(id);
    }

    @Override
    public void create(Map<String, String> map, Set<String> keys, Datasource dataSource) {
        for (String key : keys) {
            String name = key;
            String typeData = map.get(key);
            DatasourceDetail dto = new DatasourceDetail();
            dto.setDatasourceByDatasourceId(dataSource);
            dto.setColumnName(name);
            dto.setDataType(typeData);
            detailRepository.save(dto);
        }
    }

    @Override
    public List<DatasourceDetail> getDBMSHeaders(Datasource datasource) throws Exception {
        List<DatasourceDetail> datasourceDetails = new ArrayList<>();
        DatabaseUtils databaseUtils = new DatabaseUtils(datasource.getSqlserverType(), datasource.getConnection());
        Connection connection = databaseUtils.getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(datasource.getStatement());
        ResultSet resultSet = preparedStatement.executeQuery();
        ResultSetMetaData rsmd = resultSet.getMetaData();

        if (resultSet.next()) {
            //get count keys
            int numColumns = rsmd.getColumnCount();
            for (int i = 1; i <= numColumns; i++) {
                String column_name = rsmd.getColumnName(i);
                DatasourceDetail datasourceDetail = new DatasourceDetail();
                datasourceDetail.setColumnName(column_name);
                datasourceDetails.add(datasourceDetail);
            }
        }
        if (resultSet != null) {
            resultSet.close();
        }
        if (preparedStatement != null) {
            preparedStatement.close();
        }
        if (connection != null) {
            connection.close();
        }
        return datasourceDetails;
    }

    @Override
    public DatasourceDetail updateNameById(DatasourceDetail datasourceDetail) {
        return detailRepository.save(datasourceDetail);
    }
    @Override
    public void update(List<DatasourceDetail> columns, Set<String> keys, DatabaseUtils DBUtils, Map<String, String> types, Datasource datasource) throws ClassNotFoundException, SQLException {
        //keys: list columns from dataSource input
        //types: type data of column input
        //columns: list columns vs type data of dataSource in database
        for (String key : keys) {
            for (DatasourceDetail column : columns) {
                if (column.getColumnName().equals(key)) {
                    System.out.println( types.get(key) +"  " + column.getDataType());
                    if (!column.getDataType().equals(types.get(key))) {
                        //if input is String -> type is String
                        if (types.get(key).equals("string")) {
                            column.setDataType("string");
                        }//if input is float and database is int -> type is float
                        else if (types.get(key).equals("decimal") && column.getDataType().equals("integer")) {
                            column.setDataType("decimal");
                        }//if input not datatime and database is datatime -> String
                        else if (!types.get(key).equals("date") && column.getDataType().equals("date")) {
                            column.setDataType("string");
                        }//if input is datatime and database not datatime -> String
                        else if (types.get(key).equals("date") && !column.getDataType().equals("date")) {
                            column.setDataType("string");
                        }
                    }
                }
            }
        }
        for (String key : keys) {
            if (!key.equals("ODM")) {
                boolean check = true; // key don't have in database
                for (DatasourceDetail column : columns) {
                    if (key.equals(column.getColumnName())) {
                        check = false;
                    }
                }
                if (check) { //add new column into database
                    DatasourceDetail dto = new DatasourceDetail();
                    dto.setDatasourceByDatasourceId(datasource);
                    dto.setColumnName(key);
                    dto.setDataType(types.get(key));
                    columns.add(dto);
                    System.out.println("add new column");
                    DBUtils.addColumn(datasource.getTableName(), DBUtils.convertDataType(dto.getDataType()), dto.getColumnName());
                }
            }

        }

        List<Integer> columnsDrop = new ArrayList<Integer>();
        //check input columns vs columns in database
        for (DatasourceDetail column : columns) {
            //if columns input don't have column -> column was deleted
            if (!keys.contains(column.getColumnName())) {
                //remove column
//                detailRepository.deleteById(column.getId());
                DBUtils.deleteColumn(datasource.getName(), column.getColumnName());
                columnsDrop.add(column.getDatasourceDetailId());
            }
        }
        for (Integer integer : columnsDrop) {
            columns.removeIf(n -> (n.getDatasourceDetailId() == integer));
        }

        datasource.setDatasourceDetailById(columns);
        for (DatasourceDetail column : columns) {
            detailRepository.save(column);
        }
        dataSourceRepository.save(datasource);

        for (DatasourceDetail column : columns) {
            System.out.println("id :   " + column.getDatasourceDetailId() + "     " + column.getColumnName() + "  " + column.getDataType());
        }

    }
}
