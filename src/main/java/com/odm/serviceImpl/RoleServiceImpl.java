/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.odm.serviceImpl;

import com.odm.service.IRoleService;
import com.odm.entity.Role;
import com.odm.repository.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author ADMIN
 */
@Service
public class RoleServiceImpl implements IRoleService{
    @Autowired
    RoleRepository roleRepository;

    @Override
    public boolean insert(Role role) {
        boolean result = false;
        try {
            roleRepository.save(role);
            result = true;
        } catch (Exception e) {
            System.out.println("ERROR at RoleService-Insert: " + e.getMessage());
        }
        return result;
    }

    @Override
    public Role getRole(int id) {
        
        return roleRepository.findById(id).get();
    }
    
}
