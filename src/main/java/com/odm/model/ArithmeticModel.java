/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.odm.model;

/**
 *
 * @author 84918
 */
public class ArithmeticModel {

    private String operator;
    private int minParam;
    private int maxParam;

    public ArithmeticModel(String operator, int minParam, int maxParam) {
        this.operator = operator;
        this.minParam = minParam;
        this.maxParam = maxParam;
    }

    public ArithmeticModel() {
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public int getMinParam() {
        return minParam;
    }

    public void setMinParam(int minParam) {
        this.minParam = minParam;
    }

    public int getMaxParam() {
        return maxParam;
    }

    public void setMaxParam(int maxParam) {
        this.maxParam = maxParam;
    }

}
