/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.odm.model;

/**
 *
 * @author 84918
 */
public class ArithmeticRequest {
    private int id;
    private String operator;
    private int minParam;
    private int maxParam;
    private String description;
    private boolean isActive;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ArithmeticRequest(String operator, int minParam, int maxParam, String description) {
        this.operator = operator;
        this.minParam = minParam;
        this.maxParam = maxParam;
        this.description = description;
    }

    public ArithmeticRequest(String operator, int minParam, int maxParam) {
        this.operator = operator;
        this.minParam = minParam;
        this.maxParam = maxParam;
    }

    public ArithmeticRequest() {
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public int getMinParam() {
        return minParam;
    }

    public void setMinParam(int minParam) {
        this.minParam = minParam;
    }

    public int getMaxParam() {
        return maxParam;
    }

    public void setMaxParam(int maxParam) {
        this.maxParam = maxParam;
    }

}
