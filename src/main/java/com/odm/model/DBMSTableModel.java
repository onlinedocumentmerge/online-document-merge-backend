package com.odm.model;

import java.io.Serializable;
import java.util.List;

public class DBMSTableModel implements Serializable {
    private String name;
    private List<String> columns;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getColumns() {
        return columns;
    }

    public void setColumns(List<String> columns) {
        this.columns = columns;
    }
}
