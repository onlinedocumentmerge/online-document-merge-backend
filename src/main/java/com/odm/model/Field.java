package com.odm.model;

public class Field {
    private String name;
    private boolean isExpression;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isExpression() {
        return isExpression;
    }

    public void setExpression(boolean expression) {
        this.isExpression = expression;
    }
}
