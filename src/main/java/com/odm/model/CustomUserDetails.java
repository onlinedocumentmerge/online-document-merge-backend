package com.odm.model;

import java.util.Collection;
import java.util.Collections;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.odm.entity.Account;


public class CustomUserDetails implements UserDetails {
  private  Account account;
    
    

    public CustomUserDetails(Account account) {
		this.account = account;
	}

	@Override
    public Collection<? extends GrantedAuthority> getAuthorities() {		 
        return Collections.singleton(new SimpleGrantedAuthority(account.getRole().getName()));
        
    }

    @Override
    public String getPassword() {
        return account.getPassword();
    }

    @Override
    public String getUsername() {
        return account.getEmail();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }
}