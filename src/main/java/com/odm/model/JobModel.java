package com.odm.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.odm.entity.MailHeader;
import com.odm.entity.SMTP;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class JobModel implements Serializable {
    private int id;
    private List<String> rows;
    private PrepairTemplate prepairTemplate;
    @JsonFormat
            (shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd hh:mm:ss.SSSSSS", timezone = "CET")
    private Date executeTime;
    private int amount;
    private int[] outputOptions;
    private boolean isNotify;
    private boolean sendingEmail;
    private boolean downloadPDF;
    private boolean specificLink;
    private String emailHeader;
    private String notifyEmail;
    private int fromRow;
    private int toRow;
    private int accountID;
    private String cronExpression;
    private SMTP smtp;
    private List<MailHeader> mailFields;

    public List<MailHeader> getMailFields() {
        return mailFields;
    }

    public void setMailFields(List<MailHeader> mailFields) {
        this.mailFields = mailFields;
    }

    public SMTP getSmtp() {
        return smtp;
    }

    public JobModel() {
    }

    public JobModel(JobModel that) {
        this.id = that.id;
        this.rows = that.rows;
        this.prepairTemplate = that.prepairTemplate;
        this.executeTime = that.executeTime;
        this.amount = that.amount;
        this.outputOptions = that.outputOptions;
        this.isNotify = that.isNotify;
        this.sendingEmail = that.sendingEmail;
        this.downloadPDF = that.downloadPDF;
        this.specificLink = that.specificLink;
        this.emailHeader = that.emailHeader;
        this.notifyEmail = that.notifyEmail;
        this.fromRow = that.fromRow;
        this.toRow = that.toRow;
        this.accountID = that.accountID;
        this.cronExpression = that.cronExpression;
        this.smtp = that.smtp;
    }

    public void setSmtp(SMTP smtp) {
        this.smtp = smtp;
    }
    public String getCronExpression() {
        return cronExpression;
    }

    public void setCronExpression(String cronExpression) {
        this.cronExpression = cronExpression;
    }

    public int getAccountID() {
        return accountID;
    }

    public void setAccountID(int accountID) {
        this.accountID = accountID;
    }

    public int getFromRow() {
        return fromRow;
    }

    public void setFromRow(int fromRow) {
        this.fromRow = fromRow;
    }

    public int getToRow() {
        return toRow;
    }

    public void setToRow(int toRow) {
        this.toRow = toRow;
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<String> getRows() {
        return rows;
    }

    public void setRows(List<String> rows) {
        this.rows = rows;
    }

    public PrepairTemplate getPrepairTemplate() {
        return prepairTemplate;
    }

    public void setPrepairTemplate(PrepairTemplate prepairTemplate) {
        this.prepairTemplate = prepairTemplate;
    }

    public Date getExecuteTime() {
        return executeTime;
    }

    public void setExecuteTime(Date executeTime) {
        this.executeTime = executeTime;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public int[] getOutputOptions() {
        return outputOptions;
    }

    public void setOutputOptions(int[] outputOptions) {
        this.outputOptions = outputOptions;
    }

    public boolean isNotify() {
        return isNotify;
    }

    public void setNotify(boolean notify) {
        isNotify = notify;
    }

    public boolean isSendingEmail() {
        return sendingEmail;
    }

    public void setSendingEmail(boolean sendingEmail) {
        this.sendingEmail = sendingEmail;
    }

    public boolean isDownloadPDF() {
        return downloadPDF;
    }

    public void setDownloadPDF(boolean downloadPDF) {
        this.downloadPDF = downloadPDF;
    }

    public boolean isSpecificLink() {
        return specificLink;
    }

    public void setSpecificLink(boolean specificLink) {
        this.specificLink = specificLink;
    }

    public String getEmailHeader() {
        return emailHeader;
    }

    public void setEmailHeader(String emailHeader) {
        this.emailHeader = emailHeader;
    }

    public String getNotifyEmail() {
        return notifyEmail;
    }

    public void setNotifyEmail(String notifyEmail) {
        this.notifyEmail = notifyEmail;
    }
}
