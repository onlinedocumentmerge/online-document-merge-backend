package com.odm.model;

import java.util.LinkedHashMap;

public class PrepairTemplate {
    private StringBuilder content;
    private LinkedHashMap<Integer, Field> positionField;
    private LinkedHashMap<Integer, LinkedHashMap<Integer, String>> positionExpression;

    public PrepairTemplate() {
        this.positionField = new LinkedHashMap<>();
        this.positionExpression = new LinkedHashMap<>();
    }

    public StringBuilder getContent() {
        return content;
    }

    public void setContent(StringBuilder content) {
        this.content = content;
    }

    public LinkedHashMap<Integer, Field> getPositionField() {
        return positionField;
    }

    public void setPositionField(LinkedHashMap<Integer, Field> positionField) {
        this.positionField = positionField;
    }

    public LinkedHashMap<Integer, LinkedHashMap<Integer, String>> getPositionExpression() {
        return positionExpression;
    }

    public void setPositionExpression(LinkedHashMap<Integer, LinkedHashMap<Integer, String>> positionExpression) {
        this.positionExpression = positionExpression;
    }
}
