/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.odm.model;


import com.odm.service.IArithmeticServiceTest;
import odm.arithmetic.iarithmetic.IArithmeticService;

import java.util.List;

/**
 *
 * @author 84918
 */
public class Node {

    String expression;
    List<Node> childs;
    IArithmeticService arithmetic;
    List<String> operator; //toán tử + - * /
    List<String> operands; //toán hạng
    String sqlStatement;
    int openPosition, closePosition;

    public Node() {
    }

    public int getOpenPosition() {
        return openPosition;
    }

    public void setOpenPosition(int openPosition) {
        this.openPosition = openPosition;
    }

    public int getClosePosition() {
        return closePosition;
    }

    public void setClosePosition(int closePosition) {
        this.closePosition = closePosition;
    }

    public String getSqlStatement() {
        return sqlStatement;
    }

    public void setSqlStatement(String sqlStatement) {
        this.sqlStatement = sqlStatement;
    }

    public String getExpression() {
        return expression;
    }

    public void setExpression(String expression) {
        this.expression = expression;
    }

    public List<String> getOperator() {
        return operator;
    }

    public void setOperator(List<String> operator) {
        this.operator = operator;
    }

    public List<String> getOperands() {
        return operands;
    }

    public void setOperands(List<String> operands) {
        this.operands = operands;
    }

    public List<Node> getChilds() {
        return childs;
    }

    public void setChilds(List<Node> childs) {
        this.childs = childs;
    }

    public IArithmeticService getArithmetic() {
        return arithmetic;
    }

    public void setArithmetic(IArithmeticService arithmetic) {
        this.arithmetic = arithmetic;
    }

}
