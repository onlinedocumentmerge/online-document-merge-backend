package com.odm.model;

public class JSONSubArray {
    private String subArrayName;
    private int num;

    public String getSubArrayName() {
        return subArrayName;
    }

    public void setSubArrayName(String subArrayName) {
        this.subArrayName = subArrayName;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }
}
