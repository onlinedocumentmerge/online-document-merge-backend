/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.odm.service;

import com.odm.entity.Job;
import com.odm.model.JobModel;

import java.util.List;

/**
 *
 * @author ADMIN
 */
public interface IJobService {

    Job createJob(Job job, int accountId);
    List<Job> findByAcountID(int accountID);
    Job findById(int id);
    Job findBySpecificLink(String doamin, String url);
    void sendJob(JobModel jobModel) throws Exception;
    void pauseJob(JobModel jobModel) throws Exception;
    void resumeJob(JobModel jobModel) throws Exception;
    boolean isExistedCustomURL(String domain, String url);
    void update(Job job);
    float computeJobPrice(int rows, int emailHeaders, boolean isSendingEmail, boolean isDownloadPDF, boolean isSpecificLink) throws Exception;
}
