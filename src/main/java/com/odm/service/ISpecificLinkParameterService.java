package com.odm.service;

import com.odm.entity.SpecificLinkParameter;

public interface ISpecificLinkParameterService {
    SpecificLinkParameter insert(SpecificLinkParameter specificLinkParameter);
    SpecificLinkParameter update(SpecificLinkParameter specificLinkParameter);
    void delete(SpecificLinkParameter specificLinkParameter);
}
