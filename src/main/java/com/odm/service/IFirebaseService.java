package com.odm.service;

import com.google.firebase.auth.FirebaseAuthException;
import com.odm.model.JobProcess;

import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ExecutionException;

public interface IFirebaseService {
    String getFirebaseToken(int id) throws FirebaseAuthException;
    void uploadFile(String fileName, File file) throws IOException;
    void updateData(int accountID, int jobID, Map<String, Object> data) throws ExecutionException, InterruptedException;
    void addData(JobProcess jobProcess) throws ExecutionException, InterruptedException;
    void updateAmount(int accountID, int jobID, int amount) throws ExecutionException, InterruptedException;
    void updateDataCounting(int jobID, int accountID, int counting) throws ExecutionException, InterruptedException;
    String uploadImage(String fileName, byte[] image);
    void updateArithmetic(boolean status) throws ExecutionException, InterruptedException;
    void updateJobStatus(int accountID, int jobID, int status) throws ExecutionException, InterruptedException;
}
