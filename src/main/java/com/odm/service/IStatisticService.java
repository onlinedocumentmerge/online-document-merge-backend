package com.odm.service;

import java.util.Date;
import java.util.List;

public interface IStatisticService {
    List<List> getStatistic(Date from, Date to);
}
