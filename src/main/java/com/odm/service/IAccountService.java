/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.odm.service;

import com.odm.entity.Account;

import java.util.List;

/**
 *
 * @author ADMIN
 */
public interface IAccountService {
    void insert(Account account) throws Exception;
    Account getAccountById(int id);
    List<Account> GetAllAccount();
    void UpdateUserStatus(int accountID, boolean isActive) throws Exception;
    void ActivateAccount(String activationCode) throws Exception;
    void UpdateAccount(int id,Account accountReq) throws Exception;
    void forgotPassword(Account account, String email) throws Exception;
    void changePassword(int id, String oldPassword,String newPassword) throws Exception;
    void sendActiveCode(String email, String fullName, String activeCode) throws Exception;
    Account getAccountByEmail(String email);
}
