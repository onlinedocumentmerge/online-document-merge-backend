/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.odm.service;

import com.odm.entity.Datasource;
import com.odm.entity.DatasourceDetail;
import com.odm.utils.DatabaseUtils;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author 84918
 */
public interface IDataSourceDetailsService {
    void create(Map<String, String> map, Set<String> keys, Datasource dataSource);
    List<DatasourceDetail> getDBMSHeaders(Datasource datasource) throws Exception;
    DatasourceDetail updateNameById(DatasourceDetail datasourceDetail);
    void deleteById(int id);
    void update(List<DatasourceDetail> columns, Set<String> keys, DatabaseUtils DBUtils, Map<String, String> types, Datasource dataSource) throws ClassNotFoundException, SQLException;
}
