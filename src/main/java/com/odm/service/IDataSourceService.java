/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.odm.service;

import com.odm.entity.Datasource;

import java.util.List;

/**
 *
 * @author 84918
 */
public interface IDataSourceService {
    //MetaData createMetadata(String connectionString, int accountId, Job job);
    Datasource create(String connectionString, int accountId, String statement, int typeConnect);
    Datasource update(Datasource updateDatasource);
    List<Datasource> getByAccountID(int accountId);
    Datasource getByID(int id);
    void deleteById(int id);
}
