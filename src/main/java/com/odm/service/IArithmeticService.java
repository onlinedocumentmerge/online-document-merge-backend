package com.odm.service;

import com.odm.model.ArithmeticModel;
import com.odm.model.ArithmeticRequest;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

public interface IArithmeticService {
    List<ArithmeticRequest> loadArithmetic() throws InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException;
    List<ArithmeticRequest> loadArithmeticWithoutStatus() throws InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException;

    void saveArithmeticFile(MultipartFile file) throws Exception;

    void sendArithmeticToWorker(File arithmeticFile) throws Exception;

    byte[] readfileAsBytes(File file) throws IOException;

    void updateStatus(int arithmeticID, boolean status);
}
