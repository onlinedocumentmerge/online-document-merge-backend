package com.odm.service;

public interface IArithmeticServiceTest {

    public Object calculate(String string);


    public int getMinParameter();


    public int getMaxParameter();


    public String getOperator();


    public boolean isSQL();


    public String sqlStatement();
}
