package com.odm.service;

import com.odm.entity.Job;
import com.odm.entity.PaymentODM;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;

public interface IPaymentService {
    void deposit(int id, BigDecimal amount);
    void payForJob(Job job) throws Exception;
    List<PaymentODM> getPaymentHistory(int id);
    void getOrder(String orderId) throws IOException;
}
