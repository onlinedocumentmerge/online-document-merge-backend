package com.odm.service;

import com.odm.entity.Template;

import java.util.List;
import java.util.concurrent.ExecutionException;

public interface ITemplateService{
    Template createTemplate(String templateName, String templateContent, int accountID);
    List<Template> getByAccountID(int accountID);
    Template update(Template template);
    Template findById(int templateID);
    void deleteById(int templateID);
}
