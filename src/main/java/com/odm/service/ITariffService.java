package com.odm.service;

import com.odm.entity.Tariff;

import java.util.List;

public interface ITariffService {
    List<Tariff> getAll() throws Exception;
    Tariff getById(int id) throws Exception;
    void addNew(Tariff tariff) throws Exception;
    void update(Tariff tariff) throws Exception;
    void delete(int planId) throws Exception;
    float getPriceByName(String name) throws Exception;
    int getQuantityByName(String name) throws Exception;
}
