/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.odm.service;

import com.odm.model.PrepairTemplate;
import org.springframework.web.multipart.MultipartFile;

import java.util.HashMap;

/**
 *
 * @author 84918
 */
public interface IDocumentService {
    PrepairTemplate doPrepareTemplate(String template, HashMap<String, String> fieldToken);
    String insertPosition(PrepairTemplate prepairTemplate, String row);
}
