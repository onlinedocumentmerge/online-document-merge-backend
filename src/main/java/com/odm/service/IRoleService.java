/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.odm.service;

import com.odm.entity.Role;

/**
 *
 * @author ADMIN
 */
public interface IRoleService {
    boolean insert(Role role);
    Role getRole(int id);
}
