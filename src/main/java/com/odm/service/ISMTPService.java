package com.odm.service;

import com.odm.entity.SMTP;

public interface ISMTPService {
    void insert(SMTP SMTP);
    void update(SMTP SMTP);
    void delete(int id);
    SMTP getByID(int id);
}
