package com.odm.controller;

import com.odm.model.ArithmeticModel;
import com.odm.model.ArithmeticRequest;
import com.odm.service.IArithmeticService;
import com.odm.serviceImpl.ArithmeticServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

@RestController
@Slf4j
@CrossOrigin
@RequestMapping("/api/arithmetics")
public class ArithmeticController {
    @Autowired
    IArithmeticService arithmeticService;
    @GetMapping("")
    public ResponseEntity getAll() {
        try {
            List<ArithmeticRequest> arithmeticModels = arithmeticService.loadArithmetic();
            return new ResponseEntity(arithmeticModels, HttpStatus.OK);
        } catch (InstantiationException e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.OK);
        } catch (InvocationTargetException e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.OK);
        } catch (NoSuchMethodException e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.OK);
        } catch (IllegalAccessException e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.OK);
        }
    }

    @GetMapping("/all")
    public ResponseEntity getAllAdmin() {
        try {
            List<ArithmeticRequest> arithmeticModels = arithmeticService.loadArithmeticWithoutStatus();
            return new ResponseEntity(arithmeticModels, HttpStatus.OK);
        } catch (InstantiationException e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.OK);
        } catch (InvocationTargetException e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.OK);
        } catch (NoSuchMethodException e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.OK);
        } catch (IllegalAccessException e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.OK);
        }
    }

    @PutMapping("")
    public ResponseEntity updateStatus(@RequestParam int id, @RequestParam int status) {
        try {
            arithmeticService.updateStatus(id, status == 1);
            return new ResponseEntity(HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.OK);
        }
    }

    @PostMapping("")
    public ResponseEntity create(@RequestBody MultipartFile file) {
        try {
            arithmeticService.saveArithmeticFile(file);
            return new ResponseEntity(HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.OK);
        }
    }
}
