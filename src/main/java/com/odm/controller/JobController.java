package com.odm.controller;

import com.odm.entity.*;
import com.odm.model.JobModel;
import com.odm.model.JobProcess;
import com.odm.model.PrepairTemplate;
import com.odm.service.*;
import com.odm.serviceImpl.*;
import com.odm.utils.CONST;
import com.odm.utils.DatabaseUtils;
import com.odm.utils.JwtTokenProvider;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.support.CronSequenceGenerator;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.*;

@RestController
@CrossOrigin
@RequestMapping("/api/jobs")
@Slf4j
public class JobController {

    @Autowired
    JwtTokenProvider tokenProvider;
    @Autowired
    IDocumentService documentService;
    @Autowired
    IJobService jobService;
    @Autowired
    ITemplateService templateService;
    @Autowired
    IDataSourceService dataSourceService;
    @Autowired
    IDataSourceDetailsService dataSourceDetailService;
    @Autowired
    IFirebaseService firebaseService;
    @Autowired
    IPaymentService paymentService;
    @Autowired
    ISMTPService smtpService;
    @Autowired
    ISpecificLinkParameterService specificLinkParameterService;
    @Autowired
    MailFieldServiceImpl mailFieldService;
    @PostMapping("")
    public ResponseEntity createJob(@RequestBody Map<String, String> payload, @RequestHeader(name = "Authorization") String token) {
        try {
            String jobName = payload.get("jobName");
            String templateID = payload.get("templateID");
            String datasourceID = payload.get("datasourceID");
            String amountString = payload.get("amount");
            int amount = 0;
            if(!amountString.isEmpty()) {
                amount = Integer.parseInt(amountString);
            }
            String outputOptions = payload.get("outputOptions");
            String specificDomain = payload.get("speacificDomain");
            String specificLink = payload.get("speacificLink");
            String mailNotification = payload.get("notifyEmail");
            String smtpString = payload.get("smtp");
            boolean isSetTime = Boolean.parseBoolean(payload.get("isSetTime"));
            SMTP smtp = null;
            if(!smtpString.isEmpty()) {
                int smtpID = Integer.parseInt(smtpString);
                smtp = smtpService.getByID(smtpID);
            }

            Datasource dataSource = dataSourceService.getByID(Integer.parseInt(datasourceID));

            if(amount == 0) {
                DatabaseUtils databaseUtils = new DatabaseUtils(dataSource.getSqlserverType(), dataSource.getConnection());
                List<String> rows = databaseUtils.getDataFromTable(dataSource.getStatement());
                amount = rows.size();
            }

            Job job = new Job();
            job.setName(jobName);
            job.setDatasource(dataSource);
            job.setTemplate(templateService.findById(Integer.parseInt(templateID)));
            job.setMailNotification(mailNotification);
            job.setCreatedDate(new Date());
            job.setStatus(CONST.ACTIVE_JOB);
            job.setSmtp(smtp);
            if(isSetTime) {
                String cronExpression = payload.get("cronExpression");
                job.setCronExpression(cronExpression);
            }
            //get key from 1 node
            JSONObject outputOptionNode = new JSONObject(outputOptions);
            int emailHeaders = 0;
            List<MailHeader> mailHeaders = new ArrayList<>();
            List<SpecificLinkParameter> specificLinkParameters = new ArrayList<>();
            boolean isSendingEmail = false, isDownloadPDF = false, isSpecificLink = false;
            for (String optionKey : outputOptionNode.keySet()) {
                String option = (String) outputOptionNode.get(optionKey);
                if (option.equals("1")) {
                    String emailHeader = payload.get("emailHeader");
                    JSONObject emailHeaderNode = new JSONObject(emailHeader);
                    emailHeaders = emailHeaderNode.keySet().size();
                    for (String email : emailHeaderNode.keySet()) {
                        String data = emailHeaderNode.get(email).toString();
                        MailHeader mailHeader = new MailHeader();
                        mailHeader.setName(data);
                        mailHeaders.add(mailHeader);
                    }
                    job.setMailHeaders(mailHeaders);
                    isSendingEmail = true;
                }
                if (option.equals("2")) {
                    job.setDownloadPDF(true);
                    isDownloadPDF = true;
                }
                if (option.equals("3")) {
                    isSpecificLink = true;
                    String customURL = payload.get("customURL").replaceAll(" ", "%20");
                    if(jobService.isExistedCustomURL(specificDomain, customURL)) {
                        return new ResponseEntity("SpecificLink is existed!", HttpStatus.BAD_REQUEST);
                    }
                    job.setSpecificDomain(specificDomain);
                    job.setRootParameter(customURL);
                    JSONObject specificLinkNode = new JSONObject(specificLink);
                    if (specificLinkNode.keySet().size() > 0) {
                        for (String key : specificLinkNode.keySet()) {
                            String data = specificLinkNode.get(key).toString();
                            SpecificLinkParameter specificLinkParameter = new SpecificLinkParameter();
                            specificLinkParameter.setName(data);
                            specificLinkParameters.add(specificLinkParameter);
                        }
                        job.setSpecificLinkParameters(specificLinkParameters);
                    } else {
                        return new ResponseEntity("At least filter (Specific Link)", HttpStatus.BAD_REQUEST);
                    }
                }
            }
            float price = jobService.computeJobPrice(amount, emailHeaders, isSendingEmail, isDownloadPDF, isSpecificLink);
            BigDecimal bdPrice = new BigDecimal(Double.toString(price));
            bdPrice = bdPrice.setScale(2, RoundingMode.HALF_UP);
            job.setPrice(bdPrice);

            Job job1 = jobService.createJob(job, tokenProvider.getUserIdFromJWT(token.substring(7)));
            for (MailHeader m : mailHeaders) {
                m.setJob(job1);
                mailFieldService.insert(m);
            }
            for (SpecificLinkParameter s : specificLinkParameters) {
                s.setJob(job1);
                specificLinkParameterService.insert(s);
            }
            JobProcess jobProcess = new JobProcess();
            jobProcess.setId(job1.getId());
            jobProcess.setAccountID(job.getAccount().getId());
            jobProcess.setAmount(amount);
            jobProcess.setCounting(0);
            jobProcess.setStatus(CONST.ACTIVE_JOB);
            firebaseService.addData(jobProcess);
            return new ResponseEntity(HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("")
    public ResponseEntity updateJob(@RequestBody Map<String, String> payload) {
        try {
            int jobID = Integer.parseInt(payload.get("id"));
            Job job = jobService.findById(jobID);
            if(job != null) {
                String jobName = payload.get("jobName");
                String templateID = payload.get("templateID");
                String datasourceID = payload.get("datasourceID");
                String amountString = payload.get("amount");
                String outputOptions = payload.get("outputOptions");
                String specificDomain = payload.get("speacificDomain");
                String specificLink = payload.get("speacificLink");
                String mailNotification = payload.get("notifyEmail");
                String smtpString = payload.get("smtp");
                boolean isSetTime = Boolean.parseBoolean(payload.get("isSetTime"));
                SMTP smtp = null;
                if (!smtpString.isEmpty()) {
                    int smtpID = Integer.parseInt(smtpString);
                    smtp = smtpService.getByID(smtpID);
                }

                Datasource dataSource = dataSourceService.getByID(Integer.parseInt(datasourceID));
                DatabaseUtils databaseUtils = new DatabaseUtils(dataSource.getSqlserverType(), dataSource.getConnection());
                List<String> rows = databaseUtils.getDataFromTable(dataSource.getStatement());
                int amount = rows.size();

                job.setName(jobName);
                job.setDatasource(dataSource);
                job.setTemplate(templateService.findById(Integer.parseInt(templateID)));
                job.setMailNotification(mailNotification);
                job.setCreatedDate(new Date());
                job.setStatus(CONST.ACTIVE_JOB);
                job.setSmtp(smtp);
                if (isSetTime) {
                    String cronExpression = payload.get("cronExpression");
                    job.setCronExpression(cronExpression);
                }
                //get key from 1 node
                JSONObject outputOptionNode = new JSONObject(outputOptions);
                int emailHeaders = 0;
                List<MailHeader> mailHeaders = new ArrayList<>();
                List<SpecificLinkParameter> specificLinkParameters = new ArrayList<>();
                boolean isSendingEmail = false, isDownloadPDF = false, isSpecificLink = false;
                for (String optionKey : outputOptionNode.keySet()) {
                    String option = (String) outputOptionNode.get(optionKey);
                    if (option.equals("1")) {
                        List<MailHeader> mailHeadersPrevious = (List<MailHeader>) job.getMailHeaders();
                        //Delete old data
                        for(int i = 0; i < mailHeadersPrevious.size(); i++) {
                            mailFieldService.delete(mailHeadersPrevious.get(i));
                        }
                        //Add new data
                        String emailHeader = payload.get("emailHeader");
                        JSONObject emailHeaderNode = new JSONObject(emailHeader);
                        emailHeaders = emailHeaderNode.keySet().size();
                        for (String email : emailHeaderNode.keySet()) {
                            String data = emailHeaderNode.get(email).toString();
                            MailHeader mailHeader = new MailHeader();
                            mailHeader.setName(data);
                            mailHeaders.add(mailHeader);
                        }
                        job.setMailHeaders(mailHeaders);
                        isSendingEmail = true;
                    }
                    if (option.equals("2")) {
                        job.setDownloadPDF(true);
                        isDownloadPDF = true;
                    }
                    if (option.equals("3")) {
                        isSpecificLink = true;
                        String customURL = payload.get("customURL").replaceAll(" ", "%20");

                        if(job.getSpecificDomain().equals(specificDomain) && job.getRootParameter().equals(customURL)) {

                        } else {
                            if(jobService.isExistedCustomURL(specificDomain, customURL)) {
                                return new ResponseEntity("SpecificLink is existed!", HttpStatus.BAD_REQUEST);
                            }
                        }

                        job.setSpecificDomain(specificDomain);
                        job.setRootParameter(customURL);
                        List<SpecificLinkParameter> specificLinkParametersPrevious = (List<SpecificLinkParameter>) job.getSpecificLinkParameters();
                        //Delete old data
                        for(int i = 0; i < specificLinkParametersPrevious.size(); i++) {
                            specificLinkParameterService.delete(specificLinkParametersPrevious.get(i));
                        }
                        //Add new data
                        JSONObject specificLinkNode = new JSONObject(specificLink);
                        if (specificLinkNode.keySet().size() > 0) {
                            for (String key : specificLinkNode.keySet()) {
                                String data = specificLinkNode.get(key).toString();
                                SpecificLinkParameter specificLinkParameter = new SpecificLinkParameter();
                                specificLinkParameter.setName(data);
                                specificLinkParameters.add(specificLinkParameter);
                            }
                            job.setSpecificLinkParameters(specificLinkParameters);
                        } else {
                            return new ResponseEntity("At least filter (Specific Link)", HttpStatus.BAD_REQUEST);
                        }
                    }
                }
                float price = jobService.computeJobPrice(amount, emailHeaders, isSendingEmail, isDownloadPDF, isSpecificLink);
                BigDecimal bdPrice = new BigDecimal(Double.toString(price));
                bdPrice = bdPrice.setScale(2, RoundingMode.HALF_UP);
                job.setPrice(bdPrice);

                for (MailHeader m : mailHeaders) {
                    m.setJob(job);
                    mailFieldService.insert(m);
                }
                for (SpecificLinkParameter s : specificLinkParameters) {
                    s.setJob(job);
                    specificLinkParameterService.insert(s);
                }
                jobService.update(job);
                //Update amount
                firebaseService.updateAmount(job.getAccount().getId(), jobID, amount);
                firebaseService.updateDataCounting(jobID, job.getAccount().getId(), 0);
                return new ResponseEntity(HttpStatus.OK);
            } else {
                return new ResponseEntity("Job not found", HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("")
    public ResponseEntity getJob(@RequestHeader(name = "Authorization") String token) {
        try {
            int id = tokenProvider.getUserIdFromJWT(token.substring(7));
            List<Job> jobs = jobService.findByAcountID(id);
            return new ResponseEntity(jobs, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    private boolean isValidExecuteTime(String cron) {
        String newCron = cron.substring(0, cron.lastIndexOf(" ")).trim();
        String[] cronPath = cron.split(" ");
        int year = Integer.parseInt(cronPath[cronPath.length - 1]);
        CronSequenceGenerator generator = new CronSequenceGenerator(newCron);
        Date nextRunDate= generator.next(new Date());
        Calendar cal = Calendar.getInstance();
        cal.setTime(nextRunDate);
        int nextCronYear = cal.get(Calendar.YEAR);
        Instant instant = Instant.now();
        System.out.println(instant.toString());
        Instant next = nextRunDate.toInstant();
        next = next.plus(7, ChronoUnit.HOURS);
        if(year < nextCronYear) {
            next = next.minus(365L, ChronoUnit.DAYS);
        }
        boolean isValid = next.compareTo(instant) >= 0;
        return isValid;
    }

    @PostMapping("/{jobID}/execute")
    public ResponseEntity executeJob(@PathVariable int jobID) {
        try {
            System.out.println(jobID);
            Job job = jobService.findById(jobID);
            Template template = job.getTemplate();
            if(template == null) {
                throw new Exception("Template is missing.");
            }
            Datasource datasource = job.getDatasource();
            if(datasource == null) {
                throw new Exception("Datasource is missing");
            }
            if(job.getCronExpression() != null && !job.getCronExpression().isEmpty()) {
                if(!isValidExecuteTime(job.getCronExpression())) {
                    throw new Exception("Invalid Execute Time");
                }
            }
            DatabaseUtils databaseUtils = new DatabaseUtils(datasource.getSqlserverType(), datasource.getConnection());
            List<DatasourceDetail> datasourceDetails = (List<DatasourceDetail>) datasource.getDatasourceDetailById();
            HashMap<String, String> fieldWord = new HashMap<>();
            for (DatasourceDetail tmp : datasourceDetails) {
                fieldWord.put(tmp.getColumnName(), "");
            }
            PrepairTemplate prepairTemplate = documentService.doPrepareTemplate(template.getTemplateContent(), fieldWord);
            List<String> rows = databaseUtils.getDataFromTable(datasource.getStatement());

            //Pay Job
            paymentService.payForJob(job);
            JobModel jobModel = new JobModel();
            jobModel.setAccountID(job.getAccount().getId());
            jobModel.setId(job.getId());
            //Update Status
            job.setStatus(CONST.PENDING_JOB);
            jobService.update(job);
            firebaseService.updateJobStatus(job.getAccount().getId(), jobID, CONST.PENDING_JOB);
            //Send To Queue
            jobService.sendJob(jobModel);
            //Update Status
            job.setStatus(CONST.IN_QUEUE_JOB);
            jobService.update(job);
            firebaseService.updateJobStatus(job.getAccount().getId(), jobID, CONST.IN_QUEUE_JOB);
            return new ResponseEntity(HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/{jobID}/pause")
    public ResponseEntity pauseJob(@PathVariable int jobID) {
        try {
            System.out.println(jobID);
            Job job = jobService.findById(jobID);

            JobModel jobModel = new JobModel();
            jobModel.setId(job.getId());
            jobModel.setAccountID(job.getAccount().getId());
            jobService.pauseJob(jobModel);
            return new ResponseEntity(HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/{jobID}/resume")
    public ResponseEntity resumeJob(@PathVariable int jobID) {
        try {
            System.out.println(jobID);
            Job job = jobService.findById(jobID);

            JobModel jobModel = new JobModel();
            jobModel.setId(job.getId());
            jobModel.setAccountID(job.getAccount().getId());
            jobService.resumeJob(jobModel);
            return new ResponseEntity(HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping("/{jobID}")
    public ResponseEntity delete(@PathVariable int jobID) {
        try {
            Job job = jobService.findById(jobID);
            job.setStatus(CONST.ENABLE_JOB);
            jobService.update(job);
            return new ResponseEntity(HttpStatus.OK);
        }  catch (Exception e) {
            System.out.println(e.getMessage());
            return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/price")
    public ResponseEntity computePrice(@RequestBody Map<String, String> payload) {
        try {
            String datasourceID = payload.get("datasourceID");
            String amountString = payload.get("amount");
            int amount = 0;
            if (!amountString.isEmpty()) {
                amount = Integer.parseInt(amountString);
            }
            String outputOptions = payload.get("outputOptions");
            if (amount == 0) {
                Datasource dataSource = dataSourceService.getByID(Integer.parseInt(datasourceID));
                DatabaseUtils databaseUtils = new DatabaseUtils(dataSource.getSqlserverType(), dataSource.getConnection());
                List<String> rows = databaseUtils.getDataFromTable(dataSource.getStatement());
                amount = rows.size();
            }
            //get key from 1 node
            JSONObject outputOptionNode = new JSONObject(outputOptions);
            int emailHeaders = 0;
            boolean isSendingEmail = false, isDownloadPDF = false, isSpecificLink = false;
            for (String optionKey : outputOptionNode.keySet()) {
                String option = (String) outputOptionNode.get(optionKey);
                if (option.equals("1")) {
                    isSendingEmail = true;
                    String emailHeader = payload.get("emailHeader");
                    JSONObject emailHeaderNode = new JSONObject(emailHeader);
                    emailHeaders = emailHeaderNode.keySet().size();
                }
                if (option.equals("2")) {
                    isDownloadPDF = true;
                }
                if (option.equals("3")) {
                    isSpecificLink = true;
                }
            }
            float price = jobService.computeJobPrice(amount, emailHeaders, isSendingEmail, isDownloadPDF, isSpecificLink);
            return new ResponseEntity(price, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

}
