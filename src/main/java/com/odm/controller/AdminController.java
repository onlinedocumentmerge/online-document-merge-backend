/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.odm.controller;

import com.odm.entity.Account;
import com.odm.model.ArithmeticModel;
import com.odm.serviceImpl.AccountServiceImpl;
import com.odm.serviceImpl.StatisticServiceImpl;
import com.odm.utils.EmailSender;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.odm.utils.MathUtils;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 *
 * @author ADMIN
 */
@CrossOrigin(origins = "*",allowedHeaders = "*")
@RestController
public class AdminController {

    @Autowired
    EmailSender emailSender;
    @Autowired
    ModelMapper modelMapper;
    @Autowired
    AccountServiceImpl accountService;
    @Autowired
    StatisticServiceImpl statisticService;

    /* Start Account Block */
    @PutMapping("/api/accounts/activation")
    public ResponseEntity updateAccountActivation(@RequestParam int id, @RequestParam boolean isActive) {
        try {
            accountService.UpdateUserStatus(id, isActive);
            return new ResponseEntity(true, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/api/accounts/{id}")
    public ResponseEntity getAccountByID(@PathVariable int id) {
        //Role Admin
        List<Account> accounts = null;
        return new ResponseEntity(accounts, HttpStatus.OK);
    }

    @GetMapping("/api/accounts/")
    public ResponseEntity getAllAccounts() {
        //Role Admin
        try {
            List<Account> accounts = accountService.GetAllAccount();
            return new ResponseEntity(accounts, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }
    /* End Account Block */
    @GetMapping("/api/statistic")
    public ResponseEntity statistic(@RequestParam String startDate, @RequestParam String endDate) {
        try {
            Date from = new SimpleDateFormat("yyyy-MM-dd").parse(startDate);
            Date to = new SimpleDateFormat("yyyy-MM-dd").parse(endDate);
            List<List> result = statisticService.getStatistic(from, to);
            return new ResponseEntity(result, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/runJar")
    public String runJar() {
        MathUtils mathUtils = new MathUtils();
        return mathUtils.calculate("AVG(1,2,3)").toString();
    }

    @GetMapping("/runJarOK")
    public String runJarOK() {
        MathUtils mathUtils = new MathUtils();
        return mathUtils.calculate("SUM(1,2,3)").toString();
    }
}
