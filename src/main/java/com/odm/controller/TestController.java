package com.odm.controller;

import com.odm.entity.*;
import com.odm.model.*;
import com.odm.repository.DataSourceDetailRepository;
import com.odm.repository.MailHeaderRepository;
import com.odm.service.IAccountService;
import com.odm.service.IRoleService;
import com.odm.serviceImpl.*;
import com.odm.utils.*;

import java.io.*;
import java.time.Instant;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.lang.*;
import java.util.concurrent.ExecutionException;

import com.paypal.orders.OrdersGetRequest;
import lombok.extern.slf4j.Slf4j;
import odm.arithmetic.iarithmetic.IArithmeticService;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.logging.log4j.core.util.CronExpression;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.support.CronSequenceGenerator;
import org.springframework.web.bind.annotation.*;

import javax.mail.AuthenticationFailedException;

import com.odm.utils.PayPalClient;
import com.paypal.core.PayPalHttpClient;
import com.paypal.orders.Order;
import com.paypal.orders.OrdersGetRequest;
import org.json.JSONObject;

//import com.braintreepayments.http.HttpResponse;
import com.braintreepayments.http.serializer.Json;

@Slf4j
@CrossOrigin
@RestController("/test")
public class TestController {
    @Autowired
    EmailSender emailSender;
    @Autowired
    ModelMapper modelMapper;
    @Autowired
    IAccountService accountService;
    @Autowired
    IRoleService roleService;
    @Autowired
    RandomGenerator randomGenerator;
	@Autowired
	DocumentServiceImpl documentService;
	@Autowired
	DataSourceServiceImpl dataSourceService;
	@Autowired
	JobServiceImpl jobService;
	@Autowired
	FirebaseService firebaseService;
	@Autowired
	DataSourceDetailRepository dataSourceDetailRepository;
	@Autowired
	MailHeaderRepository mailHeaderRepository;
	@Autowired
	PayPalClient payPalClient;

	@GetMapping("/test/pdf")
	public void merge(int id) {
		Job job = jobService.findById(id);
		try {
//			job.setStatus(CONST.EXECUTE_JOB);
//			jobService.update(job);
//			firebaseService.updateJobStatus(job.getId(), job.getAccount().getId(), CONST.EXECUTE_JOB);
			PrepairTemplate prepairTemplate = getPrepairTemplate(job);
			List<String> rows = getRows(job, prepairTemplate);
			createDocumentOutput(job, prepairTemplate, rows);
		} catch (Exception e) {
			System.out.println("ERROR at Merging: " + e.getMessage());
		} finally {
//			job.setStatus(CONST.COMPLETED_JOB);
//			jobService.update(job);
//			try {
//				firebaseService.updateJobStatus( job.getId(), job.getAccount().getId(), CONST.COMPLETED_JOB);
//				if (job.getMailNotification() != null) {
//					sendMail(null, "Your Job " + job.getName() + " is done!", job.getMailNotification());
//				}
//			} catch (Exception e) {
//				System.out.println("ERROR at Update Status: " + e.getMessage());
//			}
		}
	}

	private PrepairTemplate getPrepairTemplate(Job job) throws  Exception{
		Datasource datasource = job.getDatasource();
		List<DatasourceDetail> datasourceDetails = dataSourceDetailRepository.getHeaderByDatasourceId(datasource.getId());
		HashMap<String, String> fieldWord = new HashMap<>();
		for (DatasourceDetail tmp : datasourceDetails) {
			fieldWord.put(tmp.getColumnName(), "");
		}
		Template template = job.getTemplate();
		PrepairTemplate prepairTemplate = documentService.doPrepareTemplate(template.getTemplateContent(), fieldWord);
		return prepairTemplate;
	}

	private List<Node> generateStatement(List<DatasourceDetail> datasourceDetails, String expression) {
		List<String> columnsList = new ArrayList<>();
		for(DatasourceDetail d : datasourceDetails) {
			columnsList.add(d.getColumnName());
		}
		String[] columns = columnsList.toArray(new String[0]);
		MathUtils2 mathUtils = new MathUtils2();
		mathUtils.loadAllArithmetic();
		IArithmeticService[] arithmetics = mathUtils.getArithmetics().toArray(new IArithmeticService[0]);
		TreeUtils treeUtils = new TreeUtils(expression, arithmetics, columns);
		treeUtils.initTree();
		treeUtils.getStatement(treeUtils.tree);
		return treeUtils.statements;
	}

	private boolean isDuplicateStatement(HashMap<String, String> original, String newValue) {
		Set<String> keySets = original.keySet();
		for(String key : keySets) {
			if(original.get(key).equals(newValue)) {
				return false;
			}
		}
		return true;
	}

	private List<String> getRows(Job job, PrepairTemplate prepairTemplate) throws Exception {
		List<Map<Integer, String>> mapPosition = new ArrayList<>();
		Datasource datasource = job.getDatasource();
		List<DatasourceDetail> datasourceDetails = dataSourceDetailRepository.getHeaderByDatasourceId(datasource.getId());
		HashMap<String, String> fieldWord = new HashMap<>();
		for (DatasourceDetail tmp : datasourceDetails) {
			fieldWord.put(tmp.getColumnName(), "");
		}

		LinkedHashMap<String, String> expressionQueryMap = new LinkedHashMap<>();
		LinkedHashMap<Integer, List<String>> expressionPosition = new LinkedHashMap<>();
		LinkedHashMap<Integer, Field> positionField = prepairTemplate.getPositionField();
		List<Integer> keys = new ArrayList<>(positionField.keySet());
		int expressionQueryCount = 1;
		for (Integer key : keys) {
			Field field = positionField.get(key);
			if (field.isExpression()) {
				LinkedHashMap<Integer, String> expressionParams = prepairTemplate.getPositionExpression().get(key);
				StringBuilder expression = new StringBuilder(field.getName());
				List<Integer> paramPosition = new ArrayList<>(expressionParams.keySet());
				Collections.reverse(paramPosition);
				for (Integer pos : paramPosition) {
					expression.insert(pos, expressionParams.get(pos));
				}
				String expressionNoSpace = expression.toString().replaceAll(" +", "");
				List<String> positionString = new ArrayList<>();
				List<Node> nodeStatement = generateStatement(datasourceDetails, expressionNoSpace);
				for (Node s : nodeStatement) {
					String result = s.getSqlStatement().replaceAll(" +", "");
					String signature = "expression_" + expressionQueryCount;
					expressionQueryMap.put(signature, result);
                    System.out.println(expressionNoSpace + " - " + expressionNoSpace.length());
					System.out.println(result + " - " + s.getOpenPosition() + " - " + s.getClosePosition());
					positionString.add(signature + "-" + s.getOpenPosition() + "-" + s.getClosePosition());
					expressionQueryCount++;
				}
				expressionPosition.put(key, positionString);
				//String expressionResult = (String) mathUtils.calculate(expression.toString().trim());
				//result.insert(key, expressionResult);
			} else {
				expressionQueryMap.put("[" + field.getName() + "]", "[" + field.getName() + "]");
			}
		}

		//Query
			//Expression: store key, select as key
		StringBuilder querySQL = new StringBuilder();
		querySQL.append("SELECT ");
		Set<String> keySet = expressionQueryMap.keySet();
		int index = 1;
		int keySetLength = keySet.size();
		for(String key : keySet) {
			querySQL.append(expressionQueryMap.get(key));
			querySQL.append(" AS ");
			querySQL.append(key);
			if(index < keySetLength) {
				querySQL.append(", ");
			}
			index++;
		}
		querySQL.append(" FROM ");
		querySQL.append(datasource.getTableName());

		System.out.println(querySQL);

		DatabaseUtils databaseUtils = new DatabaseUtils(datasource.getSqlserverType(), datasource.getConnection());
		List<String> rows = databaseUtils.getDataFromTable(querySQL.toString());

		System.out.println(rows.size());

		JSONObject jsonObject = new JSONObject(rows.get(0));
		for (String col: jsonObject.keySet()) {
			System.out.println(col + " - " + jsonObject.get(col));
		}

		Collections.reverse(keys);
		MathUtils mathUtils = new MathUtils();
		StringBuilder result = new StringBuilder(prepairTemplate.getContent());
		JSONObject node = new JSONObject(rows.get(0));
		for (Integer key : keys) {
			Field field = positionField.get(key);
			if (field.isExpression()) {
				LinkedHashMap<Integer, String> expressionParams = prepairTemplate.getPositionExpression().get(key);
				StringBuilder expression = new StringBuilder(field.getName());
				List<Integer> paramPosition = new ArrayList<>(expressionParams.keySet());
				Collections.reverse(paramPosition);
				for (Integer pos : paramPosition) {
					expression.insert(pos, expressionParams.get(pos));
				}
				String expressionNoSpace = expression.toString().replaceAll(" +", "");
				List<String> signatureStatement = expressionPosition.get(key);
				for (int i = signatureStatement.size() - 1; i >= 0; i--) {
					String sign = signatureStatement.get(i);
					String[] signPart = sign.split("-");
					String colName = signPart[0];
					int openPosition = Integer.parseInt(signPart[1]);
					int closePosition = Integer.parseInt(signPart[2]);
					String resultQuery = node.getString(colName);
					StringBuilder newExpression = new StringBuilder();
					System.out.println(expressionNoSpace.substring(0, openPosition));
					System.out.println(resultQuery);
					System.out.println(expressionNoSpace.substring(closePosition));
					newExpression.append(expressionNoSpace.substring(0, openPosition));
					newExpression.append(resultQuery);
					newExpression.append(expressionNoSpace.substring(closePosition));
					expressionNoSpace = newExpression.toString();
					System.out.println(expressionNoSpace);
				}
				String expressionResult = expressionNoSpace;
				if(!NumberUtils.isCreatable(expressionNoSpace)) {
					expressionResult = (String) mathUtils.calculate(expressionNoSpace.trim());
				}
				result.insert(key, expressionResult);
			} else {
				result.insert(key, node.get(field.getName()));
			}
		}
		System.out.println(result);



//			//If value != number -> compute arithmetic
//		//Insert Position

		return new ArrayList<>();
	}

	private String insertPosition(PrepairTemplate prepairTemplate, String row) {
		LinkedHashMap<Integer, Field> positionField = prepairTemplate.getPositionField();
		List<Integer> keys = new ArrayList<>(positionField.keySet());
		Collections.reverse(keys);
		MathUtils mathUtils = new MathUtils();
		StringBuilder result = new StringBuilder(prepairTemplate.getContent());
		JSONObject node = new JSONObject(row);
		for (Integer key : keys) {
			Field field = positionField.get(key);
			if (field.isExpression()) {
				LinkedHashMap<Integer, String> expressionParams = prepairTemplate.getPositionExpression().get(key);
				StringBuilder expression = new StringBuilder(field.getName());
				List<Integer> paramPosition = new ArrayList<>(expressionParams.keySet());
				Collections.reverse(paramPosition);
				for (Integer pos : paramPosition) {
					expression.insert(pos, node.get(expressionParams.get(pos)));
				}
				String expressionResult = (String) mathUtils.calculate(expression.toString().trim());
				result.insert(key, expressionResult);
			} else {
				result.insert(key, node.get(field.getName()));
			}
		}
		return result.toString();
	}

	private void createDocumentOutput(Job job, PrepairTemplate prepairTemplate, List<String> rows) throws ExecutionException, InterruptedException, IOException {
		int id = job.getId();
		int counting = 0;
		int i = 1;
		int end100 = 1;
		int startPDF = 1;
		int endPDF = 100;
		String jobFolderPath = "", pdfFolderPath = "";
		if (job.isDownloadPDF()) {
            jobFolderPath = getDirPath(id);
			if(end100 * 100 < rows.size()) {
                pdfFolderPath = jobFolderPath + i + "_100";
			} else {
                pdfFolderPath = jobFolderPath + i + "_" + rows.size();
				endPDF = rows.size();
			}
			new File(pdfFolderPath).mkdir();
		}
		ZipUtil zipUtil = new ZipUtil();
		long startTime = 0;
		long finishTime = 0;
		startTime = System.currentTimeMillis();
		List<String> errorList = new ArrayList<>();
		for (int j = 0; j < 1; j++) {
			List<String> errorPerRow = new ArrayList<>();
			String row = rows.get(j);
			try {
				JSONObject jsonObject = new JSONObject(row);
				String result = insertPosition(prepairTemplate, row);
				if (false) {
					//if (jobModel.isSendingEmail()) {
					try {
						List<MailHeader> mailFields = (List<MailHeader>) job.getMailHeaders();
						for (int k = 0; k < mailFields.size(); k++) {
							sendMail(job.getSmtp(), result, jsonObject.get(mailFields.get(k).getName()).toString());
						}
					} catch (AuthenticationFailedException e) {
						errorPerRow.add("Mail Authentication Exception");
					} catch (Exception e) {
						errorPerRow.add(e.getMessage());
					}
				}
				if (job.isDownloadPDF()) {
					try {
						if(i == end100 * 100 + 1) {
							startPDF = end100 * 100 +1;
							if(end100 * 100 < rows.size()) {
                                pdfFolderPath = jobFolderPath + i + "_" + end100 * 100;
								endPDF = end100 * 100;
							} else {
                                pdfFolderPath = jobFolderPath + i + "_" + rows.size();
								endPDF = rows.size();
							}
							new File(pdfFolderPath).mkdir();
						}
						if(i == end100 * 100) {
							end100++;
						}
						String pdfFilePath = "/ODM" + i;
						createPDF(jobFolderPath, pdfFolderPath, result, pdfFilePath);
						if(i == endPDF) {
							String zipFilePath = zipUtil.zipIt(pdfFolderPath);
							String downloadlink = firebaseService.uploadFilePath("documents_" + id + "_" + startPDF + "_" + endPDF + ".zip", zipFilePath);
							firebaseService.updateDownloadPDFLink(id, job.getAccount().getId(), downloadlink, "PDFLink_" + startPDF + "_" + endPDF);
						}
					} catch (Exception e) {
						errorPerRow.add(e.getMessage());
					}
				}
				counting++;
				i++;
			} catch (Exception e) {
				System.out.println("ERROR at Merging: " + e.getMessage());
			} finally {
				try {
					finishTime = System.currentTimeMillis();
					if (finishTime - startTime > 5000) {
						firebaseService.updateDataCounting(id, job.getAccount().getId(), counting);
						startTime = System.currentTimeMillis();
					}
					if(errorPerRow.size() > 0) {
						StringBuilder error = new StringBuilder();
						error.append("<tr><td>" + (j+1) + "</td><td>");
						for (int l = 0; l < errorPerRow.size(); l++) {
							error.append("<p>" + errorPerRow.get(l) + "</p>");
						}
						error.append("</td></tr>");
						errorList.add(error.toString());
					}
				} catch (Exception e) {
					System.out.println("ERROR at UpdateData(Firebase): " + e.getMessage());
				}
			}
		}
		if(errorList.size() > 0) {
			try {
				String error = createErrorHTML(errorList);
				String errorListFolderPath = jobFolderPath + "error_folder";
				new File(errorListFolderPath).mkdir();
				createPDF(jobFolderPath, errorListFolderPath, error, "\\error_file");
				String zipFilePath = zipUtil.zipIt(errorListFolderPath);
				String downloadlink = firebaseService.uploadFilePath("documents_" + id + "_error_list.zip", zipFilePath);
				firebaseService.updateDownloadPDFLink(id, job.getAccount().getId(), downloadlink, "ErrorFile");
			} catch (Exception e) {
                System.out.println("Error When Create Error File");
			}
		}
		FileUtils.deleteDirectory(new File(jobFolderPath));
		firebaseService.updateDataCounting(id, job.getAccount().getId(), counting);
	}

	private String createErrorHTML(List<String> errorList) {
		StringBuilder error = new StringBuilder();
		error.append("<!DOCTYPE html>\n" +
				"<html lang=\"en\">\n" +
				"<head>\n" +
				"    <meta charset=\"UTF-8\">\n" +
				"    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n" +
				"    <title>Online Document Merge </title>\n" +
				"    <style>\n" +
				"        #customers {\n" +
				"          font-family: \"Trebuchet MS\", Arial, Helvetica, sans-serif;\n" +
				"          border-collapse: collapse;\n" +
				"          width: 100%;\n" +
				"        }\n" +
				"        \n" +
				"        #customers td, #customers th {\n" +
				"          border: 1px solid #ddd;\n" +
				"          padding: 8px;\n" +
				"        }\n" +
				"        \n" +
				"        #customers tr:nth-child(even){background-color: #f2f2f2;}\n" +
				"        \n" +
				"        #customers tr:hover {background-color: #ddd;}\n" +
				"        \n" +
				"        #customers th {\n" +
				"          padding-top: 12px;\n" +
				"          padding-bottom: 12px;\n" +
				"          text-align: left;\n" +
				"          background-color: #4CAF50;\n" +
				"          color: white;\n" +
				"        }\n" +
				"        #customers td p {\n" +
				"            margin: 6px 0;\n" +
				"        }\n" +
				"        #customers tr td:first-child {\n" +
				"            width: 50px;\n" +
				"        }" +
				"        </style>\n" +
				"</head><body>\n" +
				"    <div class=\"container\">\n" +
				"        <h1 style=\"font-family:sans-serif; text-align: center;\">Merge Error List</h1>" +
				"<table id=\"customers\">\n" +
				"            <thead>\n" +
				"                <tr>\n" +
				"                    <th>No</th>\n" +
				"                    <th>Error</th>\n" +
				"                </tr>\n" +
				"            </thead>\n" +
				"            <tbody>");
		for(int j = 0; j < errorList.size(); j++) {
			error.append(errorList.get(j));
		}
		error.append("</tbody>\n" +
				"        </table>\n" +
				"    </div>" +
				"</body>\n" +
				"</html>");
		return error.toString();
	}

	private String getDirPath(int jobID) {
		File dir = new File(System.getProperty("user.dir"));
		dir = new File(dir, "src");
		dir = new File(dir, "main");
		dir = new File(dir, "resources");
		dir = new File(dir, "pdf");
		dir = new File(dir, "Job_" + jobID);
		if (dir.isDirectory() == false) {
			dir.mkdir();
		}
		return dir.getPath() + File.separator;
	}

	private void createPDF(String jobFolderPath, String pdfFolderPath, String html, String pdfFileName) throws Exception {
		File htmlFile = new File(jobFolderPath + pdfFileName + ".html");
		// if file does exists, then delete and create a new file
		if (htmlFile.exists()) {
			File newFileName = new File(jobFolderPath + pdfFileName + ".html");
            htmlFile.renameTo(newFileName);
            htmlFile.createNewFile();
		}
		OutputStream outputStream = new FileOutputStream(htmlFile.getAbsoluteFile());
		Writer writer = new OutputStreamWriter(outputStream);
		writer.write(html);
		writer.close();

		PDFUtil.tidyUp(htmlFile.getPath(), jobFolderPath);


		String pdfFilePath = pdfFolderPath + pdfFileName + ".pdf";
		File pdfFile = new File(pdfFilePath);
		if (pdfFile.createNewFile()) {
			System.out.println("File is created!");
		} else {
			System.out.println("File already exists.");
		}

		PDFUtil.createPdf(htmlFile.getPath(), pdfFile.getPath());
	}

	private void sendMail(SMTP smtp, String content, String mailTo) throws Exception {
		// SMTP server information
		String host = "smtp.gmail.com";
		String port = "587";
		String mailFrom = "ODM.automail@gmail.com";
		String password = "online2019";
		if (smtp != null) {
			host = smtp.getHost();
			port = smtp.getPort();
			mailFrom = smtp.getEmail();
			CryptoUtils cryptoUtils = new CryptoUtils();
			password = cryptoUtils.decrypt(smtp.getPassword());
		}

		// outgoing message information
		//String mailTo = "pdminh97@gmail.com";
		String subject = "Online Document Merge";

		// message contains HTML markups
		String message = content;

		EmailSender.sendHtmlEmail(host, port, mailFrom, password, mailTo, subject, message);
	}

	public PrepairTemplate doPrepareTemplate(String template, HashMap<String, String> fieldToken) {
		final String OPENTAG = "OPENTAG";
		final String CLOSETAG = "CLOSETAG";
		final String CONTENT = "CONTENT";

		List<String> words = new ArrayList<>();
		for (String key : fieldToken.keySet()) {
			words.add(key);
		}
		TrieUtil.Trie<Character> trie = new TrieUtil.Trie<>(words);
		char open = '«';
		char close = '»';
		String stage = CONTENT;
		StringBuilder result = new StringBuilder();
		StringBuilder expression = new StringBuilder();
		StringBuilder normal = new StringBuilder();
		boolean isOpen = false;
		boolean isExpressionOpen = false;
		int normalIndex = -1;
		int expressionIndex = -1;
		PrepairTemplate prepairTemplate = new PrepairTemplate();
		for (int i = 0; i < template.length(); i++) {
			char c = template.charAt(i);
			switch (stage) {
				case CONTENT:
					if (c == open) {
						stage = OPENTAG;
						isOpen = true;
						normalIndex = result.length();
					} else if (c == close) {
						if (isExpressionOpen) {
							Field field = prepairTemplate.getPositionField().get(expressionIndex);
							if (field != null) {
								field.setName(expression.toString());
								isExpressionOpen = false;
								expression.setLength(0);
								expressionIndex = -1;
							}
						}
					} else {
						if (isExpressionOpen) {
							expression.append(c);
						} else {
							result.append(c);
						}
					}
					break;
				case OPENTAG:
					if (c == open) {
						if (isOpen) {
							isExpressionOpen = true;
							expressionIndex = result.length();
							expression.append(normal);
							normal.setLength(0);
							normalIndex = -1;
						}
					} else if (c == close) {
						stage = CLOSETAG;
					} else {
						normal.append(c);
					}
					break;
				case CLOSETAG:
					if (trie.search(normal.toString())) {
						if (isExpressionOpen) {
							Field field = prepairTemplate.getPositionField().get(expressionIndex);
							if (field == null) {
								field = new Field();
								field.setExpression(true);
								prepairTemplate.getPositionField().put(expressionIndex, field);
							}

							LinkedHashMap<Integer, String> expressionField = prepairTemplate.getPositionExpression().get(expressionIndex);
							if (expressionField == null) {
								expressionField = new LinkedHashMap<>();
							}
							expressionField.put(expression.length(), normal.toString());
							normal.setLength(0);
							normalIndex = -1;
							prepairTemplate.getPositionExpression().put(expressionIndex, expressionField);

							isOpen = false;
							if (c == open) {
								stage = OPENTAG;
							} else if (c == close) {
								if (field != null) {
									field.setName(expression.toString());
									isExpressionOpen = false;
									expression.setLength(0);
									expressionIndex = -1;
								}
								stage = CONTENT;
							} else {
								stage = CONTENT;
								expression.append(c);
							}
						} else if (isOpen) {
							Field field = prepairTemplate.getPositionField().get(expressionIndex);
							if (field == null) {
								field = new Field();
								field.setName(normal.toString());
								field.setExpression(false);
							}
							prepairTemplate.getPositionField().put(normalIndex, field);
							normal.setLength(0);
							normalIndex = -1;
							if (c == open) {
								stage = OPENTAG;
							} else {
								stage = CONTENT;
								result.append(c);
							}
						}
					}
					break;
			}
		}
		prepairTemplate.setContent(result);
		return prepairTemplate;
	}

	public List<Node> getStatements(String algorithm, List<String> columnsList) {
		String[] columns = columnsList.toArray(new String[0]);
		MathUtils2 mathUtils = new MathUtils2();
		mathUtils.loadAllArithmetic();
		IArithmeticService[] arithmetics = mathUtils.getArithmetics().toArray(new IArithmeticService[0]);
		TreeUtils treeUtils = new TreeUtils(algorithm, arithmetics, columns);
		treeUtils.initTree();
		treeUtils.getStatement(treeUtils.tree);
		List<Node> statements = treeUtils.statements;
		return statements;
	}

	@GetMapping("/math2")
	public void math(@RequestParam String algorithm) {
    	List<String> columnsList = new ArrayList<>();
		columnsList.add("Math");
		columnsList.add("Chemistry");
		columnsList.add("Physical");
		columnsList.add("More");
    	String[] columns = columnsList.toArray(new String[0]);
		MathUtils2 mathUtils = new MathUtils2();
		mathUtils.loadAllArithmetic();
		IArithmeticService[] arithmetics = mathUtils.getArithmetics().toArray(new IArithmeticService[0]);
		TreeUtils treeUtils = new TreeUtils(algorithm, arithmetics, columns);
		treeUtils.initTree();
		treeUtils.getStatement(treeUtils.tree);
		List<Node> statements = treeUtils.statements;
		for (int i = 0; i < statements.size(); i++) {
			System.out.println(statements.get(i).getSqlStatement() + " - " + statements.get(i).getOpenPosition());
		}
	}

	@GetMapping("/template")
	public void testTemplate() {
		String input = "<p>Math: «Math»</p> <p>AVG: «AVG(«Math», «Physical»)»</p> <p>AVG: «AVG(«Math», «Physical», «AVG(«Literature», «Chemistry»)», «AVG(«Math», «Chemistry», «Physical»)»)»</p>";
		HashMap<String, String> fieldWord = new HashMap<>();
		List<String> headers = new ArrayList<>();
		headers.add("Math");
		headers.add("Physical");
		headers.add("Literature");
		headers.add("Chemistry");
		for (String tmp : headers) {
			fieldWord.put(tmp, "");
		}
		PrepairTemplate prepairTemplate = documentService.doPrepareTemplate(input, fieldWord);
		System.out.println("resadas");
	}

	@GetMapping("api/data/company")
	public ResponseEntity getJSONData() {
    	try {
			Employee employee1 = new Employee();
			employee1.setName("Le Tuan");
			employee1.setAge(20);
			employee1.setEmail("letuan@gmail.com");
			employee1.setSalary(200);
			Employee employee2 = new Employee();
			employee2.setName("Pham Minh");
			employee2.setAge(20);
			employee2.setEmail("phamminh@gmail.com");
			employee2.setSalary(190);
			Employee employee3 = new Employee();
			employee3.setName("Huynh Thai");
			employee3.setAge(20);
			employee3.setEmail("huynhthai@gmail.com");
			employee3.setSalary(220);
			Employee employee4 = new Employee();
			employee4.setName("Tran Hoang");
			employee4.setAge(18);
			employee4.setEmail("tranhoang@gmail.com");
			employee4.setSalary(180);

			Product product1 = new Product();
			product1.setName("Coca");
			product1.setPrice(10);
			product1.setSku(1);
			product1.setStock(50);
			Product product2 = new Product();
			product2.setName("Pepsi");
			product2.setPrice(10);
			product2.setSku(2);
			product2.setStock(40);
			Product product3 = new Product();
			product3.setName("Sprite");
			product3.setPrice(9);
			product3.setSku(3);
			product3.setStock(60);
			Product product4 = new Product();
			product4.setName("Fanta");
			product4.setPrice(9);
			product4.setSku(4);
			product4.setStock(20);

			JSONData jsonData = new JSONData();
			jsonData.setName("ODMTable");
			List<Employee> employees = new ArrayList<>();
			employees.add(employee1);
			employees.add(employee2);
			employees.add(employee3);
			employees.add(employee4);
			List<Product> products = new ArrayList<>();
			products.add(product1);
			products.add(product2);
			products.add(product3);
			products.add(product4);
			jsonData.setEmployees(employees);
			jsonData.setProducts(products);
			return new ResponseEntity(jsonData, HttpStatus.OK);
		} catch (Exception e) {
    		e.printStackTrace();
			return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}

	@GetMapping("/jobTime")
	public void testJobTime(@RequestParam String cron) {
		try {
			//String cron = "0 10 16 12 05 ? 2020";
			String newCron = cron.substring(0, cron.lastIndexOf(" ")).trim();
			String[] cronPath = cron.split(" ");
			int year = Integer.parseInt(cronPath[cronPath.length - 1]);
			CronSequenceGenerator generator = new CronSequenceGenerator(newCron);
			Date nextRunDate= generator.next(new Date());
			Calendar cal = Calendar.getInstance();
			cal.setTime(nextRunDate);
			int nextCronYear = cal.get(Calendar.YEAR);
			Instant instant = Instant.now();
			System.out.println(instant.toString());
			Instant next = nextRunDate.toInstant();
			next = next.plus(7, ChronoUnit.HOURS);
			if(year < nextCronYear) {
				next = next.minus(365L, ChronoUnit.DAYS);
			}
			boolean isValid = next.compareTo(instant) >= 0;
			System.out.println(isValid);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
}
