package com.odm.controller;

import com.odm.entity.SMTP;
import com.odm.model.SMTPModel;
import com.odm.service.IAccountService;
import com.odm.service.ISMTPService;
import com.odm.serviceImpl.AccountServiceImpl;
import com.odm.serviceImpl.SMTPServiceImpl;
import com.odm.utils.CryptoUtils;
import com.odm.utils.EmailSender;
import com.odm.utils.JwtTokenProvider;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.mail.AuthenticationFailedException;
import java.util.List;

@RestController
@Slf4j
@CrossOrigin
@RequestMapping("api/smtps")
public class SMTPController {
    @Autowired
    ModelMapper modelMapper;
    @Autowired
    ISMTPService smtpService;
    @Autowired
    JwtTokenProvider tokenProvider;
    @Autowired
    IAccountService accountService;
    @PostMapping("")
    public ResponseEntity create(@RequestBody SMTPModel smtpModel, @RequestHeader(name = "Authorization") String token) {
        try {
            int id = tokenProvider.getUserIdFromJWT(token.substring(7));
            SMTP smtp = modelMapper.map(smtpModel, SMTP.class);
            CryptoUtils cryptoUtils = new CryptoUtils();
            smtp.setPassword(cryptoUtils.encrypt(smtpModel.getPassword()));
            smtp.setAccount(accountService.getAccountById(id));
            smtpService.insert(smtp);
            return new ResponseEntity(HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/sendTestEmail")
    public ResponseEntity sendTestEmail(@RequestBody SMTPModel smtpModel) {
        try {
            EmailSender.sendHtmlEmail(smtpModel.getHost(), smtpModel.getPort(), smtpModel.getEmail(), smtpModel.getPassword(), smtpModel.getToEmail(), "Online Document Merge - Test Email", "Sending test email (SMTP) successfully!");
            return new ResponseEntity(HttpStatus.OK);
        } catch (AuthenticationFailedException e) {
            return new ResponseEntity("Authentication Failed. May be you do not turn on less secure option.", HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("")
    public ResponseEntity getAll(@RequestHeader(name = "Authorization") String token) {
        try {
            int id = tokenProvider.getUserIdFromJWT(token.substring(7));
            List<SMTP> smtps = (List<SMTP>) accountService.getAccountById(id).getSmtps();
            CryptoUtils cryptoUtils = new CryptoUtils();
            for (SMTP smtp : smtps) {
                smtp.setPassword(cryptoUtils.decrypt(smtp.getPassword()));
            }
            return new ResponseEntity(smtps, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity get(@PathVariable int id) {
        try {
            SMTP smtp = smtpService.getByID(id);
            CryptoUtils cryptoUtils = new CryptoUtils();
            smtp.setPassword(cryptoUtils.decrypt(smtp.getPassword()));
            return new ResponseEntity(smtp, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("")
    public ResponseEntity update(@RequestBody SMTPModel smtpModel) {
        try {
            SMTP smtp = smtpService.getByID(smtpModel.getId());
            smtp.setName(smtpModel.getName());
            smtp.setEmail(smtpModel.getEmail());
            smtp.setHost(smtpModel.getHost());
            smtp.setPort(smtpModel.getPort());
            CryptoUtils cryptoUtils = new CryptoUtils();
            smtp.setPassword(cryptoUtils.encrypt(smtpModel.getPassword()));
            System.out.println(cryptoUtils.decrypt(cryptoUtils.encrypt(smtpModel.getPassword())));
            smtpService.update(smtp);
            return new ResponseEntity(smtp, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity delete(@PathVariable int id) {
        try {
            smtpService.delete(id);
            return new ResponseEntity(HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }
}
