package com.odm.controller;

import com.odm.entity.Tariff;
import com.odm.service.ITariffService;
import com.odm.serviceImpl.TariffServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.apache.tools.ant.taskdefs.Tar;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@Slf4j
@RestController
@CrossOrigin
@RequestMapping(value = "/api/tariffs")
public class TariffController {

    @Autowired
    ITariffService tariffService;
    @Autowired
    ModelMapper modelMapper;

    @GetMapping("")
    public ResponseEntity getAll() {
        try {
            List<Tariff> tariffs = tariffService.getAll();
            return ResponseEntity.ok(tariffs);
        } catch (Exception e) {

            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    @PostMapping("/add/")
    public ResponseEntity addNew(@RequestBody Tariff tariff) {
        try {
            tariffService.addNew(tariff);
            return ResponseEntity.ok("ok");
        } catch (Exception e) {

            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }
    @PutMapping("")
    public ResponseEntity updatePlan(@RequestBody Map<String, String> payload) {
        try {
            int id = Integer.parseInt(payload.get("id"));
            Tariff tariff = tariffService.getById(id);
            tariff.setPrice(Float.parseFloat(payload.get("price")));
            tariff.setQuantity(Integer.parseInt(payload.get("quantity")));
            tariffService.update(tariff);
            return new ResponseEntity(HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }
}
