package com.odm.controller;

import com.odm.entity.Account;
import com.odm.model.AccountRequest;
import com.odm.model.AccountResponse;
import com.odm.model.CustomUserDetails;
import com.odm.service.IAccountService;
import com.odm.service.IFirebaseService;
import com.odm.service.IRoleService;
import com.odm.utils.JwtTokenProvider;
import com.odm.utils.RandomGenerator;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;

@Slf4j
@RestController
@CrossOrigin
@RequestMapping("/api/accounts/")
public class AccountController {
    @Autowired
    ModelMapper modelMapper;
    @Autowired
    PasswordEncoder passwordEncoder;
    @Autowired
    RandomGenerator randomGenerator;
    @Autowired
    AuthenticationManager authenticationManager;
    @Autowired
    JwtTokenProvider tokenProvider;
    @Autowired
    IRoleService roleService;
    @Autowired
    IAccountService accountService;
    @Autowired
    IFirebaseService firebaseService;

    @PostMapping("/register")
    public ResponseEntity createAccount(@Valid @RequestBody AccountRequest accountReq) {
        if (accountReq == null) {
            return new ResponseEntity("Cannot create Empty Account", HttpStatus.BAD_REQUEST);
        }
        try {
            Account account = modelMapper.map(accountReq, Account.class);
            account.setRole(roleService.getRole(1));
            account.setPassword(passwordEncoder.encode(account.getPassword()));
            account.setActive(true);
            account.setConfirm(false);
            account.setBalance(new BigDecimal(0));
            account.setCreatedDate(new Date());
            account.setActivationCode(randomGenerator.getAlphaNumericString(15, true, true, true));
            accountService.insert(account);
            return new ResponseEntity(true, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/login")
    public ResponseEntity login(@Valid @RequestBody AccountRequest accountReq) {
        try {
            // Xác thực thông tin người dùng Request lên
            Authentication authentication = authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(
                            accountReq.getEmail(),
                            accountReq.getPassword()
                    ));
            //Nếu không xảy ra exception tức là thông tin hợp lệ
            // Set thông tin authentication vào Security Context
            SecurityContextHolder.getContext().setAuthentication(authentication);
            // Trả về jwt cho người dùng.
            String jwt = tokenProvider.generateToken((CustomUserDetails) authentication.getPrincipal());
            int id = tokenProvider.getUserIdFromJWT(jwt);
            Account account = accountService.getAccountById(id);
            if(account.getActive()) {
                AccountResponse accountResponse = modelMapper.map(account, AccountResponse.class);
                if(account.getConfirm()) {
                    accountResponse.setToken(jwt);
                    accountResponse.setFirebaseToken(firebaseService.getFirebaseToken(account.getId()));
                } else {
                    accountResponse.setConfirm(false);
                }
                return new ResponseEntity(accountResponse, HttpStatus.OK);
            } else {
                return new ResponseEntity("Account Is Disable", HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            return new ResponseEntity("Wrong email or password", HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/profile")
    public ResponseEntity getProfile(@RequestHeader(name = "Authorization") String token) {
        try {
            int id = tokenProvider.getUserIdFromJWT(token.substring(7));
            Account account = accountService.getAccountById(id);
            return new ResponseEntity(account, HttpStatus.OK);
        } catch (StringIndexOutOfBoundsException e) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/activation")
    public ResponseEntity sendActivationCode(@RequestParam String email) {
        try {
            Account account = accountService.getAccountByEmail(email);
            if(account != null) {
                if(account.getConfirm()) {
                    return new ResponseEntity("Account is already active", HttpStatus.BAD_REQUEST);
                } else {
                    accountService.sendActiveCode(account.getEmail(), account.getFullname(), account.getActivationCode());
                }
                return new ResponseEntity(true, HttpStatus.OK);
            } else {
                return new ResponseEntity("Wrong Email", HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/activation")
    public ResponseEntity ActivateAccount(@RequestBody String activationCode) {
        try {
            accountService.ActivateAccount(activationCode);
            return new ResponseEntity(true, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("")
    public ResponseEntity UpdateAccount(@RequestBody AccountRequest accountReq, @RequestHeader(name = "Authorization") String token) {
        try {
            Account account = modelMapper.map(accountReq, Account.class);
            int id = tokenProvider.getUserIdFromJWT(token.substring(7));
            accountService.UpdateAccount(id, account);
            return new ResponseEntity(true, HttpStatus.OK);
        } catch (StringIndexOutOfBoundsException e) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/forgot")
    public ResponseEntity forgotPassword(@RequestParam String email) {
        try {
            Account account = accountService.getAccountByEmail(email);
            if(account != null) {
                accountService.forgotPassword(account, email);
                return new ResponseEntity(true, HttpStatus.OK);
            } else {
                return new ResponseEntity("Wrong Email", HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("/password")
    public ResponseEntity changePassword(@RequestHeader(name = "Authorization") String token, @RequestBody Map<String, Object> payload) {
        try {
            int accountId = tokenProvider.getUserIdFromJWT(token.substring(7));
            accountService.changePassword(accountId,payload.get("oldPassword").toString(),payload.get("newPassword").toString());
            //accountService.changePassword(accountId, passwordEncoder.encode(payload.get("oldPassword").toString()),passwordEncoder.encode(payload.get("newPassword").toString()));
            return new ResponseEntity(true, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }
}
