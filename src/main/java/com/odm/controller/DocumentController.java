package com.odm.controller;

import com.odm.entity.DatasourceDetail;
import com.odm.model.PrepairTemplate;
import com.odm.service.IDocumentService;
import com.odm.serviceImpl.DocumentServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

@RestController
@CrossOrigin
@Slf4j
@RequestMapping("/api/documents")
public class DocumentController {
    @Autowired
    IDocumentService documentService;

    @PostMapping("/preview")
    public ResponseEntity preview(@RequestBody Map<String, Object> payload) {
        try {
            String template = payload.get("template").toString();
            String data = payload.get("data").toString();

            JSONObject node = new JSONObject(data);
            Iterator<String> keys = node.keys();
            HashMap<String, String> fieldWord = new HashMap<>();
            for (Iterator<String> it = keys; it.hasNext(); ) {
                String key = it.next();
                fieldWord.put(key, "");
            }
            PrepairTemplate prepairTemplate = documentService.doPrepareTemplate(template, fieldWord);
            String resultHTML = documentService.insertPosition(prepairTemplate, data);
            return new ResponseEntity(resultHTML, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }
}
