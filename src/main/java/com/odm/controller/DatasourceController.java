package com.odm.controller;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.odm.entity.*;
import com.odm.model.DBMSTableModel;
import com.odm.service.*;
import com.odm.serviceImpl.MailFieldServiceImpl;
import com.odm.utils.CONST;
import com.odm.utils.DatabaseUtils;
import com.odm.utils.JwtTokenProvider;
import com.odm.utils.ODMUtils;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.sql.SQLException;
import java.util.*;

@RestController
@CrossOrigin
@Slf4j
@RequestMapping("api/datasources")
public class DatasourceController {
    @Autowired
    private JwtTokenProvider tokenProvider;
    @Autowired
    IDocumentService documentService;
    @Autowired
    IJobService jobService;
    @Autowired
    ITemplateService templateService;
    @Autowired
    IDataSourceService dataSourceService;
    @Autowired
    IDataSourceDetailsService dataSourceDetailService;
    @Autowired
    IFirebaseService firebaseService;
    @Autowired
    IPaymentService paymentService;
    @Autowired
    MailFieldServiceImpl mailFieldService;
    @Autowired
    ISpecificLinkParameterService specificLinkParameterService;

    private ODMUtils utils = new ODMUtils();
    private DatabaseUtils databaseUtils = new DatabaseUtils(CONST.TYPE_CONNECT_SQLSERVER,
            CONST.CONNECTION_STRING);

    @PostMapping("")
    public ResponseEntity saveDataSource(@RequestBody Map<String, Object> payload, @RequestHeader(name = "Authorization") String token) {
        try {
            int id = tokenProvider.getUserIdFromJWT(token.substring(7));
            //String data = "[{\"Employee\":\"\",\"Day Work\":\"\",\"Salary\":\"\"},{\"Employee\":\"Nguyen Van A\",\"Day Work\":20,\"Salary\":500000},{\"Employee\":\"Nguyen Van B\",\"Day Work\":18,\"Salary\":400000},{\"Employee\":\"Nguyen Van C\",\"Day Work\":22,\"Salary\":300000},{\"Employee\":\"Nguyen Van D\",\"Day Work\":24,\"Salary\":450000},{\"Employee\":\"Nguyen Van E\",\"Day Work\":23,\"Salary\":400000},{\"Employee\":\"Nguyen Van F\",\"Day Work\":19,\"Salary\":300000}]";
            String connectionString = payload.get("connectionString").toString();
            if (connectionString.equals("")) {
                connectionString = "jdbc:sqlserver://localhost;databaseName=ODM;integratedSecurity=false;user=sa;password=123;";
            }
            int sqlServerType = 2;
            String sqlserverTypeString = payload.get("sqlserverType").toString();
            if (!sqlserverTypeString.equals("")) {
                sqlServerType = Integer.parseInt(sqlserverTypeString);
            }
            Datasource dataSource = dataSourceService.create(connectionString, id, "", sqlServerType);
            boolean isDBMS = Boolean.parseBoolean(payload.get("isDBMS").toString());
            String tableName = "";
            if(!isDBMS) {
                tableName = "Datasource.dbo.TABLE_" + id + "_" + dataSource.getId();
            }
            String statement = payload.get("statement").toString();
            if (statement.equals("")) {
                statement = "SELECT * FROM " + tableName;
            }
            dataSource.setName(payload.get("name").toString());
            dataSource.setStatement(statement);
            dataSource.setTableName(tableName);
            dataSource.setDBMS(isDBMS);
            dataSourceService.update(dataSource);

            Datasource dataSourceResponse = new Datasource();
            dataSourceResponse.setId(dataSource.getId());
            dataSourceResponse.setName(dataSource.getName());

            utils = new ODMUtils();
            String data = payload.get("data").toString();
            utils.confirmTypeOfDataSource(data);
            dataSourceDetailService.create(utils.getMapDataTypeClient(), utils.getColumns(), dataSource);

            //Create Table
            if(!isDBMS) {
                databaseUtils.createTableDataSource(id, dataSource.getId(),
                        utils.getMap(), utils.getColumns(), utils.getValues());
            }
            return new ResponseEntity(dataSourceResponse, HttpStatus.OK);
        } catch (SQLException e) {
            System.out.println("ERROR: " + e.getMessage());
            return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
        } catch (ClassNotFoundException e) {
            System.out.println("ERROR: " + e.getMessage());
            return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            System.out.println("ERROR: " + e.getMessage());
            return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("")
    public ResponseEntity getDataSource(@RequestHeader(name = "Authorization") String token) {
        try {
            int id = tokenProvider.getUserIdFromJWT(token.substring(7));
            List<Datasource> dataSources = dataSourceService.getByAccountID(id);
            return new ResponseEntity(dataSources, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity getDataSourceByID(@PathVariable int id) {
        try {
            //Account account = accountRepository.findById(id).get();
            Datasource dataSources = dataSourceService.getByID(id);
            DatabaseUtils databaseUtils = new DatabaseUtils(dataSources.getSqlserverType(), dataSources.getConnection());
            List<String> data = databaseUtils.getDataFromTable(dataSources.getStatement());
            List<DatasourceDetail> datatype = (List<DatasourceDetail>) dataSources.getDatasourceDetailById();
            List<List> a = new ArrayList<>();
            a.add(data);
            a.add(datatype);
            return new ResponseEntity(a, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/{id}/headers")
    public ResponseEntity getDataSourceHeaderByID(@PathVariable int id) {
        try {
            Datasource dataSources = dataSourceService.getByID(id);
            List<DatasourceDetail> datatype;
//            if(isDBMS) {
//                datatype = dataSourceDetailService.getDBMSHeaders(dataSources);
//            } else {
//                datatype  = (List<DatasourceDetail>) dataSources.getDatasourceDetailById();
//            }
            datatype  = (List<DatasourceDetail>) dataSources.getDatasourceDetailById();
            return new ResponseEntity(datatype, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("")
    public ResponseEntity updateDataSource(@RequestBody Map<String, Object> payload) {
        try {
            int id = Integer.parseInt(payload.get("id").toString());
            //get datasource
            Datasource datasource = dataSourceService.getByID(id);
            if(datasource.isDBMS()) {
                String connectionString = payload.get("connectionString").toString();
                if (connectionString.equals("")) {
                    connectionString = "jdbc:sqlserver://localhost;databaseName=ODM;integratedSecurity=false;user=sa;password=123;";
                }
                int sqlServerType = 2;
                String sqlserverTypeString = payload.get("sqlserverType").toString();
                if (!sqlserverTypeString.equals("")) {
                    sqlServerType = Integer.parseInt(sqlserverTypeString);
                }
                String statement = payload.get("statement").toString();

                datasource.setName(payload.get("name").toString());
                datasource.setStatement(statement);

                String headers = payload.get("headers").toString();
                JSONArray jsonArray = new JSONArray(headers);
                List<DatasourceDetail> datasourceDetailsPrevious = (List<DatasourceDetail>) datasource.getDatasourceDetailById();
                //Delete old data
                for (int i = 0; i < datasourceDetailsPrevious.size(); i++) {
                    dataSourceDetailService.deleteById(datasourceDetailsPrevious.get(i).getDatasourceDetailId());
                }
                //Add new data
                List<DatasourceDetail> datasourceDetails = new ArrayList<>();
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject header = jsonArray.getJSONObject(i);
                    DatasourceDetail datasourceDetail = new DatasourceDetail();
                    datasourceDetail.setColumnName(header.getString("name"));
                    datasourceDetail.setDataType(header.getString("datatype"));
                    datasourceDetail.setDatasourceByDatasourceId(datasource);
                    dataSourceDetailService.updateNameById(datasourceDetail);
                    datasourceDetails.add(datasourceDetail);
                }
                datasource.setDatasourceDetailById(datasourceDetails);
                dataSourceService.update(datasource);
            } else {
                String createOrEditRows = payload.get("createOrEdit").toString();
                String deleteRows = payload.get("delete").toString();

                //Connect to datasource database
                int typeConnect = datasource.getSqlserverType();
                DatabaseUtils DBUtils = new DatabaseUtils(typeConnect, datasource.getConnection());

                //confirm type data of input Data Source
                ODMUtils ODMUtils = new ODMUtils();
                ODMUtils.confirmTypeOfDataSource(createOrEditRows);

                //update metadata details
                dataSourceDetailService.update((List<DatasourceDetail>) datasource.getDatasourceDetailById(), ODMUtils.getColumns(), DBUtils, ODMUtils.getMapDataTypeClient(), datasource);


                //UPDATE DATASOURCE DATABASE
                //+ update type of columns in database
                List<DatasourceDetail> columns = new ArrayList<>();
                columns = (List<DatasourceDetail>) datasource.getDatasourceDetailById();
                for (DatasourceDetail column : columns) {
                    DBUtils.changeDataTypeColumn(typeConnect, datasource.getTableName(), column.getDataType(), column.getColumnName());
                }

                //+ update row database
                ODMUtils.confirmTypeOfStatus(datasource.getTableName());
                String sqlUpdates = ODMUtils.getSqlUpdate();
                if (sqlUpdates.length() > 0) {
                    String[] sqlUpdate = sqlUpdates.split(";");
                    for (String string : sqlUpdate) {
                        DBUtils.queryRowDataSource(string);
                    }
                }

                //+ delete row database
                boolean check = true;
                ObjectMapper mapper = new ObjectMapper();
                JsonNode jsonNode = mapper.readTree(deleteRows);
                ArrayList<String> rowIds = new ArrayList<>();
                for (Iterator<JsonNode> it = jsonNode.iterator(); it.hasNext(); ) {
                    String rowId = it.next().asText();
                    if (ODMUtils.getTypeOfVariable(rowId) != CONST.TYPE_DATA_INTEGER) {
                        check = false;
                        break;
                    }
                    rowIds.add(rowId);
                }
                if (check) {
                    DBUtils.deleteRow(rowIds.toArray(new String[rowIds.size()]), datasource.getTableName());
                } else {
                    System.out.println("DELETE FAIL");
                }
                //+ insert row database
                String sqlInsert = ODMUtils.getSqlInsert();
                if (sqlInsert.length() > 0) {
                    DBUtils.queryRowDataSource(sqlInsert);
                }
            }
            return new ResponseEntity(HttpStatus.OK);
        } catch (IOException e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
        } catch (SQLException e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
        } catch (ClassNotFoundException e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteDataSource(@PathVariable int id) {
        try {
            //get datasource
            Datasource datasource = dataSourceService.getByID(id);
            int typeConnect = datasource.getSqlserverType();
            DatabaseUtils DBUtils = new DatabaseUtils(typeConnect, datasource.getConnection());

            if(datasource.getConnection().equals("jdbc:sqlserver://localhost;databaseName=ODM;integratedSecurity=false;user=sa;password=123;")) {
                //drop table in datasource database
                DBUtils.dropTable(datasource.getTableName());
                //remove metadata Details
                List<DatasourceDetail> details = (List<DatasourceDetail>) datasource.getDatasourceDetailById();
                for (DatasourceDetail detail : details) {
                    dataSourceDetailService.deleteById(detail.getDatasourceDetailId());
                }
                //remove metadata
                List<Job> jobs = (List<Job>) datasource.getJobsById();
                for (Job job : jobs) {
                    job.setDatasource(null);
                    jobService.update(job);
                }
            }
            dataSourceService.deleteById(id);
            return new ResponseEntity(HttpStatus.OK);
        } catch (SQLException e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
        } catch (ClassNotFoundException e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("/headers")
    public ResponseEntity updateHeader(@RequestParam int datasourceID, @RequestParam String oldheader, @RequestParam String newheader) {
        try {
                //Datasource
                Datasource datasource = dataSourceService.getByID(datasourceID);
                if(datasource != null) {
                    DatabaseUtils DBUtils = new DatabaseUtils(datasource.getSqlserverType(), datasource.getConnection());
                    DBUtils.changeColumnName(datasource.getTableName(), oldheader, newheader);

                    List<DatasourceDetail> datasourceDetails = (List<DatasourceDetail>) datasource.getDatasourceDetailById();
                    for(int i = 0; i < datasourceDetails.size(); i++) {
                        DatasourceDetail datasourceDetail = datasourceDetails.get(i);
                        if(datasourceDetail.getColumnName().equals(oldheader)) {
                            datasourceDetail.setColumnName(newheader);
                            dataSourceDetailService.updateNameById(datasourceDetail);
                        }
                    }
                }

                List<Job> jobs = (List<Job>) datasource.getJobsById();
                for(int i = 0; i < jobs.size(); i++) {
                    Job job = jobs.get(i);
                    List<MailHeader> mailHeaders = (List<MailHeader>) job.getMailHeaders();
                    //Mail Field
                    for(int j = 0; j < mailHeaders.size(); j++) {
                        MailHeader mailHeader = mailHeaders.get(i);
                        if(mailHeader.getName().equals(oldheader)) {
                            mailHeader.setName(newheader);
                            mailFieldService.update(mailHeader);
                        }
                    }
                    //Link
                    List<SpecificLinkParameter> specificLinkParameters = (List<SpecificLinkParameter>) job.getSpecificLinkParameters();
                    for(int j = 0; j < specificLinkParameters.size(); j++) {
                        SpecificLinkParameter specificLinkParameter = specificLinkParameters.get(i);
                        if(specificLinkParameter.getName().equals(oldheader)) {
                            specificLinkParameter.setName(newheader);
                            specificLinkParameterService.update(specificLinkParameter);
                        }
                    }
                }


            return new ResponseEntity(HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping("/headers")
    public ResponseEntity deleteHeader(@RequestParam int datasourceID, @RequestParam String header) {
        try {
            if(!header.isEmpty()) {
                //Datasource
                Datasource datasource = dataSourceService.getByID(datasourceID);

                DatabaseUtils DBUtils = new DatabaseUtils(datasource.getSqlserverType(), datasource.getConnection());
                DBUtils.deleteColumn(datasource.getTableName(), header);

                if(datasource != null) {
                    List<DatasourceDetail> datasourceDetails = (List<DatasourceDetail>) datasource.getDatasourceDetailById();
                    for(int i = 0; i < datasourceDetails.size(); i++) {
                        if(datasourceDetails.get(i).getColumnName().equals(header)) {
                            dataSourceDetailService.deleteById(datasourceDetails.get(i).getDatasourceDetailId());
                        }
                    }
                }

                List<Job> jobs = (List<Job>) datasource.getJobsById();
                for(int i = 0; i < jobs.size(); i++) {
                    Job job = jobs.get(i);
                    List<MailHeader> mailHeaders = (List<MailHeader>) job.getMailHeaders();
                    //Mail Field
                    for(int j = 0; j < mailHeaders.size(); j++) {
                        MailHeader mailHeader = mailHeaders.get(i);
                        if(mailHeader.getName().equals(header)) {
                            mailFieldService.delete(mailHeader);
                        }
                    }
                    //Link
                    List<SpecificLinkParameter> specificLinkParameters = (List<SpecificLinkParameter>) job.getSpecificLinkParameters();
                    for(int j = 0; j < specificLinkParameters.size(); j++) {
                        SpecificLinkParameter specificLinkParameter = specificLinkParameters.get(i);
                        if(specificLinkParameter.getName().equals(header)) {
                            specificLinkParameterService.delete(specificLinkParameter);
                        }
                    }
                }

            }
            return new ResponseEntity(HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/json")
    public ResponseEntity getJSONData(@RequestBody String urlString) {
        try {
            System.out.println(urlString);
            URL url = new URL(urlString);
            URLConnection conn = url.openConnection();
            conn.addRequestProperty("User-Agent", "Mozilla/4.76");
            BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            StringBuffer result = new StringBuffer();
            String inputLine;
            while ((inputLine = in.readLine()) != null)
                result.append(inputLine);
            in.close();
            new JSONObject(result);

            try {
                new JSONObject(result);
            } catch (JSONException ex) {
                try {
                    new JSONArray(result);
                } catch (JSONException ex1) {
                    throw new Exception("Data is not under JSON format");
                }
            }

//            JSONSubArray subArray1 = new JSONSubArray();
//            subArray1.setNum(1);
//            subArray1.setSubArrayName("Num1");
//            JSONSubArray subArray2 = new JSONSubArray();
//            subArray2.setNum(2);
//            subArray2.setSubArrayName("Num2");
//            JSONSubArray subArray3 = new JSONSubArray();
//            subArray3.setNum(3);
//            subArray3.setSubArrayName("Num3");
//            List<JSONSubArray> jsonSubArrays = new ArrayList<>();
//            jsonSubArrays.add(subArray1);
//            jsonSubArrays.add(subArray2);
//            jsonSubArrays.add(subArray3);
//            Employee jsonArray1 = new Employee();
//            jsonArray1.setArrayName("Array1");
//            jsonArray1.setSubArray(jsonSubArrays);
//            Employee jsonArray2 = new Employee();
//            jsonArray2.setArrayName("Array2");
//            jsonArray2.setSubArray(jsonSubArrays);
//            Employee jsonArray3 = new Employee();
//            jsonArray3.setArrayName("Array3");
//            jsonArray3.setSubArray(jsonSubArrays);
//            List<Employee> jsonArrays = new ArrayList<>();
//            jsonArrays.add(jsonArray1);
//            jsonArrays.add(jsonArray2);
//            jsonArrays.add(jsonArray3);
//            JSONData jsonData = new JSONData();
//            jsonData.setName("Name");
//            jsonData.setArray(jsonArrays);


            return new ResponseEntity(result, HttpStatus.OK);
//        } catch (IOException e) {
//            log.error("ERROR at Datasource - getJSONData: " + e.getMessage());
//            return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    /* Start DBMS Block */
    @GetMapping("/dbms/checkconnect")
    public ResponseEntity checkConnect(@RequestParam("connectionString") String dbURL,
                                       @RequestParam("typeConnect") int typeConnect) {
        try {
            //typeConnect = CONST.TYPE_CONNECT_SQLSERVER;
            //dbURL = "jdbc:sqlserver://cloudprc391.database.windows.net:1433;databaseName=DataSource;encrypt=true;trustServerCertificate=false;hostNameInCertificate=*.database.windows.net;loginTimeout=30";
            DatabaseUtils DBUtils = new DatabaseUtils(typeConnect, dbURL);
            boolean result = !(DBUtils.getConnection() == null);
            return new ResponseEntity(result, HttpStatus.OK);
        } catch (SQLException e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
        } catch (ClassNotFoundException e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/dbms/checkstatement")
    public ResponseEntity checkStatement(@RequestParam("connectionString") String dbURL,
                                         @RequestParam("typeConnect") int typeConnect,
                                         @RequestParam("statement") String statement) {
        try {
            //typeConnect = CONST.TYPE_CONNECT_SQLSERVER;
            //dbURL = "jdbc:sqlserver://cloudprc391.database.windows.net:1433;databaseName=DataSource;encrypt=true;trustServerCertificate=false;hostNameInCertificate=*.database.windows.net;loginTimeout=30";
            DatabaseUtils DBUtils = new DatabaseUtils(typeConnect, dbURL);
            List<String> data = DBUtils.checkStatement(statement);
            return new ResponseEntity(data, HttpStatus.OK);
        } catch (SQLException e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
        } catch (ClassNotFoundException e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/dbms")
    public ResponseEntity getTable(@RequestParam("connectionString") String dbURL,
                                   @RequestParam("typeConnect") int typeConnect) {
        try {
            //typeConnect = CONST.TYPE_CONNECT_SQLSERVER;
            //dbURL = "jdbc:sqlserver://cloudprc391.database.windows.net:1433;databaseName=DataSource;encrypt=true;trustServerCertificate=false;hostNameInCertificate=*.database.windows.net;loginTimeout=30";
            DatabaseUtils DBUtils = new DatabaseUtils(typeConnect, dbURL);
            List<String> tables = DBUtils.getTablesInDatabase();
            List<DBMSTableModel> data = new ArrayList<>();
            for (String table : tables) {
                DBMSTableModel tableModel = new DBMSTableModel();
                tableModel.setName(table);
                tableModel.setColumns(DBUtils.getColumnFromTable(table));
                data.add(tableModel);
            }
            return new ResponseEntity(data, HttpStatus.OK);
        } catch (SQLException e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
        } catch (ClassNotFoundException e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/dbms/table")
    public ResponseEntity getDataFromTable(@RequestParam("connectionString") String dbURL,
                                           @RequestParam("typeConnect") int typeConnect,
                                           @RequestParam("tableName") String tableName,
                                           @RequestParam("limit") int limit,
                                           @RequestParam("pageNumber") int pageNumber,
                                           @RequestParam("statement") String statement) {
        try {
            //typeConnect = CONST.TYPE_CONNECT_SQLSERVER;
            //dbURL = "jdbc:sqlserver://cloudprc391.database.windows.net:1433;databaseName=DataSource;encrypt=true;trustServerCertificate=false;hostNameInCertificate=*.database.windows.net;loginTimeout=30";
            DatabaseUtils DBUtils = new DatabaseUtils(typeConnect, dbURL);
            int offset = (pageNumber - 1) * limit;
            //String queryStatement = "SELECT * FROM " + tableName + " LIMIT " + limit + " OFFSET " + offset ;
            if(statement.isEmpty()) {
                statement = "SELECT * FROM " + tableName;
            }
            System.out.println(statement);
            List<String> data = DBUtils.getDataFromTable(statement);
            return new ResponseEntity(data, HttpStatus.OK);
        } catch (SQLException e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
        } catch (ClassNotFoundException e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }
    /* End DBMS Block */
}
