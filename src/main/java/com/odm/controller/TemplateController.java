package com.odm.controller;

import com.odm.entity.Job;
import com.odm.entity.Template;
import com.odm.service.*;
import com.odm.serviceImpl.*;
import com.odm.utils.JwtTokenProvider;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.xml.bind.DatatypeConverter;
import java.util.Base64;
import java.util.List;
import java.util.Map;

@RestController
@CrossOrigin
@Slf4j
@RequestMapping("api/templates")
public class TemplateController {
    @Autowired
    private JwtTokenProvider tokenProvider;
    @Autowired
    IDocumentService documentService;
    @Autowired
    IJobService jobService;
    @Autowired
    ITemplateService templateService;
    @Autowired
    IDataSourceService dataSourceService;
    @Autowired
    IDataSourceDetailsService dataSourceDetailService;
    @Autowired
    IFirebaseService firebaseService;
    @Autowired
    IPaymentService paymentService;
    @PostMapping("")
    public ResponseEntity createTemplate(@RequestBody Map<String, String> payload, @RequestHeader(name = "Authorization") String token) {
        try {
            //String template = "<p><strong>ALIBABA</strong></p><p><strong>Salary Slip</strong></p><p>Employee Name: <ODMNode>Employee</ODMNode></p><p>Month: 6 Year: 2019</p><figure class=\"table\"><table><tbody><tr><td>No</td><td>Content</td><td>Workday</td><td>Amount</td></tr><tr><td>1</td><td>Lương tháng 6/2019</td><td><ODMNode>Day Work</ODMNode></td><td><ODMNode>Salary</ODMNode></td></tr><tr><td>2</td><td>Lương làm them * 100%</td><td>&nbsp;</td><td>&nbsp;</td></tr><tr><td>3</td><td>Lương làm them * 130%</td><td>&nbsp;</td><td>&nbsp;</td></tr><tr><td>4</td><td>Lương làm them * 150%</td><td>&nbsp;</td><td>&nbsp;</td></tr><tr><td>5</td><td>Tiền Phạt</td><td>&nbsp;</td><td>&nbsp;</td></tr><tr><td>6</td><td>Tiền Tạm Ứng</td><td>&nbsp;</td><td>&nbsp;</td></tr><tr><td>7</td><td>Nộp BHXH/BHYT</td><td>&nbsp;</td><td>&nbsp;</td></tr><tr><td>8</td><td>Thuế</td><td>&nbsp;</td><td>&nbsp;</td></tr><tr><td>&nbsp;</td><td>Tổng Cộng:</td><td>&nbsp;</td><td>&nbsp;</td></tr></tbody></table></figure><p>Accountant</p><p>MinXL</p>";
            int id = tokenProvider.getUserIdFromJWT(token.substring(7));
            Template template = templateService.createTemplate(payload.get("name"), "", id);
            Document doc = Jsoup.parse(payload.get("template"));
            Elements imgs = doc.select("img");
            for (int i = 0; i < imgs.size(); i++) {
                Element img = imgs.get(i);
                String base64Image = img.attr("src").split(",")[1];
                byte[] imageBytes = DatatypeConverter.parseBase64Binary(base64Image);
                String fileName = "template_" + template.getId() + "_" + i + ".png";
                String downloadLink = firebaseService.uploadImage(fileName, imageBytes);
                img.attr("src", downloadLink); // or whatever
            }
            String templateContent = doc.outerHtml();
            System.out.println(templateContent);
            template.setTemplateContent(templateContent);
            templateService.update(template);
            return new ResponseEntity(template, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }
    @PutMapping("")
    public ResponseEntity updateTemplate(@RequestBody Map<String, String> payload, @RequestHeader(name = "Authorization") String token) {
        try {
            int id = Integer.parseInt(payload.get("id"));
            String name = payload.get("name");
            Template template = templateService.findById(id);
            Document doc = Jsoup.parse(payload.get("template"));
            Elements imgs = doc.select("img");
            for (int i = 0; i < imgs.size(); i++) {
                Element img = imgs.get(i);
                String base64Image = img.attr("src").split(",")[1];
                byte[] imageBytes = DatatypeConverter.parseBase64Binary(base64Image);
                String fileName = "template_" + template.getId() + "_" + i + ".png";
                String downloadLink = firebaseService.uploadImage(fileName, imageBytes);
                img.attr("src", downloadLink); // or whatever
            }
            String templateContent = doc.outerHtml();
            System.out.println(templateContent);
            template.setTemplateContent(templateContent);
            template.setName(name);
            templateService.update(template);
            return new ResponseEntity(template, HttpStatus.OK);
        } catch (NumberFormatException e) {
            return new ResponseEntity("Template ID Not Found", HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }
    @GetMapping("")
    public ResponseEntity getTemplate(@RequestHeader(name = "Authorization") String token) {
        try {
            int id = tokenProvider.getUserIdFromJWT(token.substring(7));
            //Account account = accountRepository.findById(id).get();
            List<Template> templates = templateService.getByAccountID(id);
            return new ResponseEntity(templates, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity getTemplateByID(@PathVariable int id) {
        try {
            Template template = templateService.findById(id);
            return new ResponseEntity(template, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteTemplateByID(@PathVariable int id) {
        try {
            Template template = templateService.findById(id);
            List<Job> jobs = (List<Job>) template.getJobsById();
            for (Job job : jobs) {
                job.setTemplate(null);
                jobService.update(job);
            }
           templateService.deleteById(id);
            return new ResponseEntity(true, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }
}
