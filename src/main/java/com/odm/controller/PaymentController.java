package com.odm.controller;

import com.odm.entity.PaymentODM;
import com.odm.service.IPaymentService;
import com.odm.serviceImpl.AccountServiceImpl;
import com.odm.serviceImpl.PaymentServiceImpl;
import com.odm.utils.GetOrder;
import com.odm.utils.JwtTokenProvider;
import com.odm.utils.PayPalClient;
import io.swagger.annotations.ResponseHeader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

@RestController
@CrossOrigin
public class PaymentController {
    @Autowired
    private JwtTokenProvider tokenProvider;
    @Autowired
    private IPaymentService paymentService;

    @PostMapping("/api/payments/deposit")
    public ResponseEntity deposit(@RequestHeader(name = "Authorization") String token, @RequestBody Map<String, Object> payload) {
        try {
            int id = tokenProvider.getUserIdFromJWT(token.substring(7));
//            String orderID = payload.get("orderID").toString();
//            paymentService.getOrder(orderID);
            String amountString = payload.get("amount").toString();
            BigDecimal amount = new BigDecimal(amountString);
            paymentService.deposit(id, amount);
            return new ResponseEntity(HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("api/payments/history")
    public ResponseEntity getPaymentHistory(@RequestHeader(name = "Authorization") String token) {
        try {
            int id = tokenProvider.getUserIdFromJWT(token.substring(7));
            List<PaymentODM> history = paymentService.getPaymentHistory(id);
            return new ResponseEntity(history, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }
}
