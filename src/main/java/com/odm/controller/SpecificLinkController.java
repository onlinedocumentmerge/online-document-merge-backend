package com.odm.controller;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.odm.entity.*;
import com.odm.model.PrepairTemplate;
import com.odm.service.IDocumentService;
import com.odm.service.IJobService;
import com.odm.serviceImpl.DocumentServiceImpl;
import com.odm.serviceImpl.JobServiceImpl;
import com.odm.utils.DatabaseUtils;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Slf4j
@Controller
public class SpecificLinkController {
    @Autowired
    IJobService jobService;
    @Autowired
    IDocumentService documentService;

    @GetMapping("/documents/**")
    public String viewSpecificLink(HttpServletRequest request) {
        String result = "error";
        try {
//            URL aURL = new URL("http://example.com:80/docs/books/tutorial"
//                    + "/index.html?name=networking#DOWNLOADING");
//
//            System.out.println("protocol = " + aURL.getProtocol()); //http
//            System.out.println("authority = " + aURL.getAuthority()); //example.com:80
//            System.out.println("host = " + aURL.getHost()); //example.com
//            System.out.println("port = " + aURL.getPort()); //80
//            System.out.println("path = " + aURL.getPath()); //  /docs/books/tutorial/index.html
//            System.out.println("query = " + aURL.getQuery()); //name=networking
//            System.out.println("filename = " + aURL.getFile()); ///docs/books/tutorial/index.html?name=networking
//            System.out.println("ref = " + aURL.getRef()); //DOWNLOADING


            //String uri = request.getRequestURI();
//            Pattern pattern = Pattern.compile("/[^api]/[a-zA-Z0-9\\t\\n ./<>?;:\"'`!@#$%^&*()\\[\\]{}_+=|\\\\-]+");
//            Matcher matcher = pattern.matcher(uri);
//            if(matcher.matches()) {
            URL url = new URL(request.getRequestURL().toString());
            String domain = url.getHost();
            if(!String.valueOf(url.getPort()).isEmpty()) {
                if(url.getPort() > -1) {
                    domain += ":" + url.getPort();
                }
            }
            String params = url.getPath();
            String[] uriArray = params.split("/");
            String customURL = uriArray[2];
            Job job = jobService.findBySpecificLink(domain, customURL);
            if(job != null) {
                List<String> filterParam = new ArrayList<>();
                for (int i = 3; i < uriArray.length; i++) {
                    filterParam.add(uriArray[i].replaceAll("%20", " "));
                }
                Datasource datasource = job.getDatasource();
                DatabaseUtils databaseUtils = new DatabaseUtils(datasource.getSqlserverType(), datasource.getConnection());

                List<SpecificLinkParameter> specificLinkParameters = (List<SpecificLinkParameter>) job.getSpecificLinkParameters();


                //String[] conditionArray = conditionStatement.split(" AND ");
                if(specificLinkParameters.size() == filterParam.size()) {




                    //String whereStatement = datasource.getStatement() + " WHERE ";
//                    if (conditionArray.length > 0) {
//                        for (int i = 0; i < conditionArray.length; i++) {
//                            whereStatement += conditionArray[i] + filterParam.get(i) + " AND ";
//                        }
//                        whereStatement = whereStatement.substring(0, whereStatement.lastIndexOf(" AND "));
//                    } else {
//                        whereStatement += conditionStatement + filterParam.get(0);
//                    }

                    //List<String> listRow = databaseUtils.getRowByLink(whereStatement);
                    //List<String> listRow = databaseUtils.getRowByLinkA(whereStatement, specificLinkParameters, filterParam);

                    List<String> listRow = databaseUtils.getDataFromTable(datasource.getStatement());
                    if (listRow.size() > 0) {
                        int index = -1;
                        for (int i = 0; i < listRow.size(); i++) {
                            if(index > -1) {
                                break;
                            }
                            JSONObject node = new JSONObject(listRow.get(i));
                            for(int j = 0; j < specificLinkParameters.size(); j++) {
                                if(node.get(String.valueOf(specificLinkParameters.get(j).getName())).equals(filterParam.get(j))) {
                                    index = i;
                                } else {
                                    index = -1;
                                    break;
                                }
                            }
                        }

                        if(index > -1) {
                            Template template = job.getTemplate();
                            String fieldString = listRow.get(index);
                            List<DatasourceDetail> datasourceDetails = (List<DatasourceDetail>) datasource.getDatasourceDetailById();
                            HashMap<String, String> fieldWord = new HashMap<>();
                            if (datasourceDetails.size() > 0) {
                                for (DatasourceDetail tmp : datasourceDetails) {
                                    fieldWord.put(tmp.getColumnName(), "");
                                }
                            } else {
                                String lastNode = listRow.get(0);
                                //get key from 1 node
                                JSONObject node1 = new JSONObject(lastNode);
                                Set<String> keys = node1.keySet();
                                for (String key : keys) {
                                    fieldWord.put(key, "");
                                }
                            }
                            PrepairTemplate prepairTemplate = documentService.doPrepareTemplate(template.getTemplateContent(), fieldWord);
                            String resultHTML = documentService.insertPosition(prepairTemplate, fieldString);
                            request.setAttribute("RESULT", resultHTML);
                            result = "index";
                        } else {
                            System.out.println("ERROR at link: " + "Not Found Data");
                            request.setAttribute("ERROR", "Not Found Data" );
                        }
                    } else {
                        System.out.println("ERROR at link: " + "Not Found Data");
                        request.setAttribute("ERROR", "Not Found Data" );
                    }
                } else {
                    System.out.println("ERROR at link: " + "Wrong Number Of Param");
                    request.setAttribute("ERROR", "Wrong Path");
                }
            } else {
                System.out.println("ERROR at link: " + "Job Not Found (Domain or Link Wrong)");
                request.setAttribute("ERROR", "Wrong Path");
            }
            //}
            //request.setAttribute("ERROR", "Invalid URL");
        } catch (Exception e) {
            request.setAttribute("ERROR", e.getMessage());
        } finally {
            return result;
        }
    }
}
