/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.odm.repository;

import com.odm.entity.DatasourceDetail;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 *
 * @author 84918
 */
public interface DataSourceDetailRepository extends CrudRepository<DatasourceDetail, Integer> {
    @Query(value = "select d from DatasourceDetail d where d.datasourceByDatasourceId.id = :id")
    public List<DatasourceDetail> getHeaderByDatasourceId(int id);
}
