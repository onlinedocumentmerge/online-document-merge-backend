package com.odm.repository;

import com.odm.entity.MailHeader;
import org.springframework.data.repository.CrudRepository;

public interface MailFieldRepository extends CrudRepository<MailHeader, Integer> {
}
