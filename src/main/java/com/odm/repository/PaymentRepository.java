/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.odm.repository;

import com.odm.entity.Account;
import com.odm.entity.PaymentODM;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;

/**
 *
 * @author ADMIN
 */
public interface PaymentRepository extends CrudRepository<PaymentODM, String>{
    @Query("SELECT p FROM PaymentODM p WHERE account.id = :id order by createdDay desc ")
    List<PaymentODM> getPaymentHistory (int id);
    @Query("select p from PaymentODM p where p.createdDay >= :from and p.createdDay <= :to")
    public List<PaymentODM> statistic(@Param("from") Date date, @Param("to") Date Date);
}
