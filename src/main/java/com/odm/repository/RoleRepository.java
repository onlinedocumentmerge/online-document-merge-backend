/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.odm.repository;

import com.odm.entity.Account;
import com.odm.entity.Role;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author ADMIN
 */
public interface RoleRepository extends CrudRepository<Role, Integer>{
    
}
