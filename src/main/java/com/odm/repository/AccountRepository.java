package com.odm.repository;

import com.odm.entity.Account;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface AccountRepository extends CrudRepository<Account, Integer> {

    @Query(value = "SELECT * FROM Account a WHERE a.activation_code = ?1",
             nativeQuery = true)
    public Account findByActivationCode(String activationCode);
    
    @Query(value = "SELECT * FROM Account a WHERE a.email = ?1",
             nativeQuery = true)
    public Account findByEmail(String email);

    @Query("select a from Account a where a.email = :email and a.password = :password and a.isActive = 1")
    public Account login(@Param("email") String email, @Param("password") String password);

    @Query("select a from Account a where a.createdDate >= :from and a.createdDate<= :to")
    public List<Account> statistic(@Param("from") Date date, @Param("to") Date Date);
}
