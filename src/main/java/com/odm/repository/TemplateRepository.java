package com.odm.repository;

import com.odm.entity.Template;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface TemplateRepository extends CrudRepository<Template,Integer> {
    @Query("SELECT t FROM Template t WHERE t.account.id = :accountID")
    List<Template> getByAccountID(@Param("accountID") int accountID);
}
