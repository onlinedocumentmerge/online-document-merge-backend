package com.odm.repository;

import com.odm.entity.MailHeader;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface MailHeaderRepository extends CrudRepository<MailHeader, Integer> {
    @Query(value = "select m from MailHeader m where m.job.id = :id")
    List<MailHeader> getMailFieldsByJobId(int id);
}
