package com.odm.repository;

import com.odm.entity.SMTP;
import org.springframework.data.repository.CrudRepository;

public interface SMTPRepository extends CrudRepository<SMTP, Integer> {
}
