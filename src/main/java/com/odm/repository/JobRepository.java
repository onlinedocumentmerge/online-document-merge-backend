/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.odm.repository;

import com.odm.entity.Job;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;

/**
 *
 * @author ADMIN
 */
public interface JobRepository extends CrudRepository<Job, Integer> {
    @Query("SELECT j FROM Job j WHERE j.account.id = :accountID and j.status <> -1")
    List<Job> findByAccountID(@Param("accountID") int accountID);

    @Query("SELECT oj FROM Job oj WHERE oj.specificDomain = :domain and oj.rootParameter = :rootParameter and oj.status <> -1")
    Job findBySpecificLink(@Param("domain") String domain, @Param("rootParameter") String rootParameter);

    @Query("select j from Job j where j.createdDate >= :from and j.createdDate<= :to")
    List<Job> statistic(@Param("from") Date date, @Param("to") Date Date);
}
