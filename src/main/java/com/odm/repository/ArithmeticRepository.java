package com.odm.repository;

import com.odm.entity.Arithmetic;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface ArithmeticRepository extends CrudRepository<Arithmetic, Integer> {
    @Query(value = "select a from Arithmetic a")
    List<Arithmetic> getAll();
    @Query(value = "select a from Arithmetic a where a.isActive = true")
    List<Arithmetic> getByActive();
    @Query(value = "select a from Arithmetic a where a.name = :name")
    Arithmetic getByName(@Param("name") String name);
}
