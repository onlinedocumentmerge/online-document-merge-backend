package com.odm.repository;

import com.odm.entity.Tariff;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TariffRepository extends CrudRepository<Tariff, Integer> {
    @Query("SELECT t FROM Tariff t WHERE t.name = :name")
    Tariff findByName(@Param("name") String name);

    @Query("SELECT t FROM Tariff t")
    List<Tariff> getAll();
}
