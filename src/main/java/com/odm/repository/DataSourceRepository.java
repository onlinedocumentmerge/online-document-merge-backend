/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.odm.repository;

import com.odm.entity.Datasource;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 *
 * @author 84918
 */
@Repository
public interface DataSourceRepository extends CrudRepository<Datasource, Integer>{
    @Query("SELECT d FROM Datasource d WHERE d.account.id = :accountID")
    List<Datasource> getByAccountID(@Param("accountID") int accountID);
}
