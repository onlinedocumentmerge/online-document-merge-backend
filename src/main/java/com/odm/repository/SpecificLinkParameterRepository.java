package com.odm.repository;

import com.odm.entity.SpecificLinkParameter;
import org.springframework.data.repository.CrudRepository;

public interface SpecificLinkParameterRepository extends CrudRepository<SpecificLinkParameter, Integer> {
}
