package com.odm;

import com.odm.utils.EmailSender;
import com.odm.utils.RandomGenerator;
import org.modelmapper.ModelMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class OnlineDocumentMergeApplication {

	 
	
    public static void main(String[] args) {
        SpringApplication.run(OnlineDocumentMergeApplication.class, args);
    }


    
    @Bean
    public ModelMapper modelMapper() {
        return new ModelMapper();
    }
    @Bean
    public EmailSender emailSender() {
        return new EmailSender();
    }
    @Bean
    public RandomGenerator randomGenerator(){
        return new RandomGenerator();
    }

}
