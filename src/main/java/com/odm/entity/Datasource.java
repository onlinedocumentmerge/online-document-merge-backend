package com.odm.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Collection;
import java.util.Date;

@Entity
@Table(name = "Datasource")
public class Datasource {
    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Basic
    @Column(name = "connection", nullable = true, length = 255)
    private String connection;
    @Basic
    @Lob
    @Column(name = "statement", nullable = true)
    private String statement;
    @Basic
    @Column(name = "table_name", nullable = true, length = 255)
    private String tableName;
    @Basic
    @Column(name = "name", nullable = true, length = 255)
    private String name;
    @Basic
    @Column(name = "SQLServerType")
    private int sqlserverType;
    @Basic
    @Column(name = "create_date", nullable = true, length = 255)
    private Date createDate;
    @ManyToOne
    @JoinColumn(name = "created_by", referencedColumnName = "id")
    @JsonIgnore
    private Account account;
    @OneToMany(mappedBy = "datasourceByDatasourceId")
    @JsonIgnore
    private Collection<DatasourceDetail> datasourceDetailById;
    @OneToMany(mappedBy = "datasource")
    @JsonIgnore
    private Collection<Job> jobsById;
    @Basic
    @Column(name = "is_DBMS")
    private boolean isDBMS;

    public boolean isDBMS() {
        return isDBMS;
    }

    public void setDBMS(boolean DBMS) {
        isDBMS = DBMS;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getConnection() {
        return connection;
    }

    public void setConnection(String connection) {
        this.connection = connection;
    }

    public String getStatement() {
        return statement;
    }

    public void setStatement(String statement) {
        this.statement = statement;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public int getSqlserverType() {
        return sqlserverType;
    }

    public void setSqlserverType(int sqlserverType) {
        this.sqlserverType = sqlserverType;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Collection<DatasourceDetail> getDatasourceDetailById() {
        return datasourceDetailById;
    }

    public void setDatasourceDetailById(Collection<DatasourceDetail> datasourceDetailById) {
        this.datasourceDetailById = datasourceDetailById;
    }

    public Collection<Job> getJobsById() {
        return jobsById;
    }

    public void setJobsById(Collection<Job> jobsById) {
        this.jobsById = jobsById;
    }

    public Account getAccountByCreatedBy() {
        return account;
    }

    public void setAccountByCreatedBy(Account accountByCreatedBy) {
        this.account = accountByCreatedBy;
    }
}