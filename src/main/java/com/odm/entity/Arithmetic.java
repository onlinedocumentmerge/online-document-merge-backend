package com.odm.entity;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "arithmetic")
public class Arithmetic implements Serializable {
    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Basic
    @Column(name = "name")
    private String name;
    @Basic
    @Column(name = "is_active")
    private boolean isActive;
    @Basic
    @Column(name = "min_parameter")
    private int minParameter;
    @Basic
    @Column(name = "max_parameter")
    private int maxParameter;
    @Basic
    @Column(name = "description")
    private String description;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public int getMinParameter() {
        return minParameter;
    }

    public void setMinParameter(int minParameter) {
        this.minParameter = minParameter;
    }

    public int getMaxParameter() {
        return maxParameter;
    }

    public void setMaxParameter(int maxParameter) {
        this.maxParameter = maxParameter;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
