package com.odm.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

@Entity
@Table(name = "DatasourceDetail")
public class DatasourceDetail {
    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int datasourceDetailId;
    @ManyToOne
    @JoinColumn(name = "datasource_id", referencedColumnName = "id")
    @JsonIgnore
    private Datasource datasourceByDatasourceId;
    @Basic
    @Column(name = "column_name", nullable = true, length = 255)
    private String columnName;
    @Basic
    @Column(name = "data_type", nullable = true, length = 255)
    private String dataType;

    public int getDatasourceDetailId() {
        return datasourceDetailId;
    }

    public void setDatasourceDetailId(int datasourceDetailId) {
        this.datasourceDetailId = datasourceDetailId;
    }

    public Datasource getDatasourceByDatasourceId() {
        return datasourceByDatasourceId;
    }

    public void setDatasourceByDatasourceId(Datasource datasourceByDatasourceId) {
        this.datasourceByDatasourceId = datasourceByDatasourceId;
    }

    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public String getDataType() {
        return dataType;
    }

    public void setDataType(String dataType) {
        this.dataType = dataType;
    }
}
