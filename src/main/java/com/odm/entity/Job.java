/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.odm.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.Digits;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;

/**
 *
 * @author ADMIN
 */
@Entity
@Table(name = "Job")
public class Job {
    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Basic
    @Column(name = "name")
    private String name;
    @ManyToOne
    @JoinColumn(name = "datasource_id", referencedColumnName = "id")
    private Datasource datasource;
    @ManyToOne
    @JoinColumn(name = "template_id", referencedColumnName = "id")
    private Template template;
    @ManyToOne
    @JoinColumn(name = "created_by", referencedColumnName = "id")
    @JsonIgnore
    private Account account;
    @Column(name = "created_date")
    @Basic
    private Date createdDate;
    @Column(name = "price", scale = 2)
    @Digits(integer=16, fraction=2)
    private BigDecimal price;
    @Column(name = "mail_notification")
    @Basic
    private String mailNotification;
    @ManyToOne
    @JoinColumn(name = "smtp", referencedColumnName = "id")
    private SMTP smtp;
    @OneToMany(mappedBy = "job")
    private Collection<MailHeader> mailHeaders;
    @Column(name = "is_download_PDF")
    @Basic
    private boolean isDownloadPDF;
    @OneToMany(mappedBy = "job")
    private Collection<PDFStorageURL> pdfStorageURLs;
    @Column(name = "specific_domain")
    @Basic
    private String specificDomain;
    @Column(name = "root_parameter")
    @Basic
    private String rootParameter;
    @OneToMany(mappedBy = "job")
    private Collection<SpecificLinkParameter> specificLinkParameters;
    @Column(name = "status")
    private int status;
    @Column(name = "executeSchedule")
    private String cronExpression;
    @Column(name = "errorFileStorageURL")
    private String errorFileStorageURL;

    public String getRootParameter() {
        return rootParameter;
    }

    public void setRootParameter(String rootParameter) {
        this.rootParameter = rootParameter;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Datasource getDatasource() {
        return datasource;
    }

    public void setDatasource(Datasource datasource) {
        this.datasource = datasource;
    }

    public Template getTemplate() {
        return template;
    }

    public void setTemplate(Template template) {
        this.template = template;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getMailNotification() {
        return mailNotification;
    }

    public void setMailNotification(String mailNotification) {
        this.mailNotification = mailNotification;
    }

    public SMTP getSmtp() {
        return smtp;
    }

    public void setSmtp(SMTP smtp) {
        this.smtp = smtp;
    }

    public Collection<MailHeader> getMailHeaders() {
        return mailHeaders;
    }

    public void setMailHeaders(Collection<MailHeader> mailHeaders) {
        this.mailHeaders = mailHeaders;
    }

    public Collection<PDFStorageURL> getPdfStorageURLs() {
        return pdfStorageURLs;
    }

    public void setPdfStorageURLs(Collection<PDFStorageURL> pdfStorageURLs) {
        this.pdfStorageURLs = pdfStorageURLs;
    }

    public String getSpecificDomain() {
        return specificDomain;
    }

    public void setSpecificDomain(String specificDomain) {
        this.specificDomain = specificDomain;
    }

    public Collection<SpecificLinkParameter> getSpecificLinkParameters() {
        return specificLinkParameters;
    }

    public void setSpecificLinkParameters(Collection<SpecificLinkParameter> specificLinkParameters) {
        this.specificLinkParameters = specificLinkParameters;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getCronExpression() {
        return cronExpression;
    }

    public void setCronExpression(String cronExpression) {
        this.cronExpression = cronExpression;
    }

    public String getErrorFileStorageURL() {
        return errorFileStorageURL;
    }

    public void setErrorFileStorageURL(String errorFileStorageURL) {
        this.errorFileStorageURL = errorFileStorageURL;
    }

    public boolean isDownloadPDF() {
        return isDownloadPDF;
    }

    public void setDownloadPDF(boolean downloadPDF) {
        isDownloadPDF = downloadPDF;
    }
}
