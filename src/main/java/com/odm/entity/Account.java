package com.odm.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.apache.http.cookie.SM;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.*;
@Entity
@Table(name = "Account")
public class Account {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private int id;
    @Basic
    @Column(name = "email", nullable = false, length = 255)
    private String email;
    @Basic
    @Column(name = "password", nullable = false, length = 255)
    private String password;
    @Basic
    @Column(name = "fullname", length = 255)
    private String fullname;
    @Basic
    @Column(name = "phone", nullable = true, length = 10)
    private String phone;
    @Basic
    @Column(name = "isActive", nullable = false)
    private Boolean isActive;
    @Basic
    @Column(name = "isConfirm", nullable = false)
    private Boolean isConfirm;
    @Basic
    @Column(name = "activationCode", nullable = true, length = 255)
    private String activationCode;
    @ManyToOne
    @JoinColumn(name = "role_id", referencedColumnName = "id")
    private Role role;
    @Column(name = "created_date")
    @Basic
    private Date createdDate;
    @OneToMany(mappedBy = "account")
    @JsonIgnore
    private Collection<Job> jobs;
    @OneToMany(mappedBy = "account")
    @JsonIgnore
    private Collection<Datasource> datasources;
    @OneToMany(mappedBy = "account")
    @JsonIgnore
    private Collection<Template> templates;
    @OneToMany(mappedBy = "account")
    @JsonIgnore
    private Collection<SMTP> smtps;
    @Basic
    @Column(name = "balance", scale = 2)
    private BigDecimal balance;
    @OneToMany(mappedBy = "account")
    @JsonIgnore
    private Collection<PaymentODM> payments;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    public Boolean getConfirm() {
        return isConfirm;
    }

    public void setConfirm(Boolean confirm) {
        isConfirm = confirm;
    }

    public String getActivationCode() {
        return activationCode;
    }

    public void setActivationCode(String activationCode) {
        this.activationCode = activationCode;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Collection<Job> getJobs() {
        return jobs;
    }

    public void setJobs(Collection<Job> jobs) {
        this.jobs = jobs;
    }

    public Collection<Datasource> getDatasources() {
        return datasources;
    }

    public void setDatasources(Collection<Datasource> datasources) {
        this.datasources = datasources;
    }

    public Collection<Template> getTemplates() {
        return templates;
    }

    public void setTemplates(Collection<Template> templates) {
        this.templates = templates;
    }

    public Collection<SMTP> getSmtps() {
        return smtps;
    }

    public void setSmtps(Collection<SMTP> smtps) {
        this.smtps = smtps;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public Collection<PaymentODM> getPayments() {
        return payments;
    }

    public void setPayments(Collection<PaymentODM> payments) {
        this.payments = payments;
    }
}
